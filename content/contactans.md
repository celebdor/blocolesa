+++
date = "2017-07-29"
title = "Contacta'ns"
[menu.main]
name = "Contacta'ns"
weight = +90
+++

Per posar-vos en contacte amb nosaltres o informar-nos de qualsevol cosa,
envieu-nos un correu electrònic a [blocolesa2019@gmail.com](mailto:blocolesa2019@gmail.com)
