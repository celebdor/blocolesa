+++
date = "2014-04-09"
title = "El Partit"
[menu.main]
name = "Qui som?"
weight = -100
+++

Els trets bàsics que defineixen la proposta del **BLOC OLESÀ** per la nova
Olesa que necessitem responen a unes línies d'actuació que, malgrat la seva
senzillesa, ha estat impossible posar en marxa en l'Olesa del passat, que tots,
nosaltres i vosaltres, volem deixar enrera. Conceptes clau com **participació,
bon urbanisme, serveis universals, cultura popular, ensenyament de qualitat o
joventut compromesa i amb plens drets ciutadans** formen part no només dels
nostres discursos, sinó del nostre bagatge en les institucions i entitats en
què els membres del **BLOC OLESÀ** han col·laborat en els darrers anys.

**El BLOC OLESÀ** s’ha constituït a partir de dos grups ben definits. Per una
banda, una formació de llarga trajectòria política: **Esquerra Unida d'Olesa**
, el referent local d'Esquerra Unida i Alternativa. Per l'altra, **Olesans i
Olesanes per la Participació**, un grup de recent constitució que aplega una
colla de veïns d'aquest poble amb una sòlida i dilatada experiència en les
entitats cíviques. En el poc temps que ha transcorregut des de la constitució a
aquest nucli inicial s'han anat sumant una bona colla de ciutadans que
s'autoqualifiquen com a **independents** i que han acabat de completar **el
ventall ampli i plural que és el BLOC OLESÀ**.

Aquesta pluralitat de persones i sensibilitats, aquesta amplitud de posicions
ideològiques no ha d'amagar, però, que ens hem unit perquè tots coincidim en el
model de poble en què ens agradaria que es convertís Olesa. El que ens ha unit,
diguem-ho clar, és l'ambició. **L'ambició de fer entre tots un poble millor, la
convicció que això és possible i la seguretat que els olesans ens ho
mereixem**.

Ja sabem que alguns no acaben d'entendre la unió entre els independents
d’Olesans i Olesanes i Esquerra Unida d'Olesa. Però aquesta era una **suma
natural**. I diem una suma perquè aquesta operació aritmètica és la que millor
ens defineix. Som una suma que abans de consolidar-se va convidar a alguns
altres que van preferir seguir en solitari fent ús, i cal dir-ho sense acritud,
del seu legítim dret a prendre les decisions que considerin més adequades.

A alguns de nosaltres se'ns acusa d’haver fet fàstics fins ara a la política i
als polítics, com si consideréssim aquesta activitat indigna. Els que parlen
així tenen una idea absolutament esbiaixada del terme política. Perquè tots
estem fent política contínuament: quan critiquem el mal estat de les voreres,
quan demanem la dignificació de l'escola pública, quan ens indignem perquè a
uns veïns els volen arremangar el pati per fer un carrer que no porta enlloc o
quan ens queixem de la insuficiència de places geriàtriques.

Tots practiquem i estimem la política. Però tots plegats coincidim en
**menysprear la politiqueria**, és a dir, la voluntat de determinats
representants públics d'utilitzar la seva presència en les institucions per
assegurar la seva continuïtat o la del seu partit en futures legislatures. És a
dir, el no fotre res pel poble i molt pel partit i la colla que governa.

Ben al contrari del tarannà que defineix el **BLOC OLESÀ**: un grup de persones
que han fet seva la màxima **una altra Olesa és possible, una altra Olesa és
necessària. Perquè tots plegats ens mereixem una altra Olesa**.
