+++
title = "Crònica d\"una moció (encara) no presentada"
tags = ["mocions"]
type = "blog"
date = "2015-11-27"
author =  "Jordi Martínez"
menu = "mocions"
+++
Al Ple municipal celebrat el dimecres 25 de novembre, en el torn de precs i
preguntes, una representant de la Plataforma d’Afectats per les Hipoteques
(PAH), va preguntar per què una moció que van entrar per registre el 23 de
setembre de 2015, en concret la “moción de apoyo para exigir una nueva ley
reguladora del derecho a la vivienda que cubra las medidas de mínimos para
hacer frente a la emergencia habitacional”, encara no s’havia dut al ple per al
seu debat.

La resposta que es va donar al mateix Ple, ampliada, és la que tot seguit podeu llegir.

**23.09.2015** La PAH entra pel registre (nº 10239) la seva moció i demana que
entri al proper Ple, que se celebra l’endemà mateix, 24 de setembre.

Cal dir que al Ple només poden presentar mocions l’alcaldessa, la Junta de
Govern, la Junta de Portaveus, els portaveus en nom del seu grup municipal o un
mínim de tres regidors (Art. 15.2 Reglament Orgànic Municipal). 

Vol dir que, per entrar aquesta moció al Ple, cal que una d’aquestes instàncies
la faci seva. A més, si, com en aquest cas (a un dia del ple) es presentés
urgent, caldria justificar-ne la urgència.

**25.09.2015** Des de secretaria d’alcaldia es truca a la persona que signa la instància de la moció per demanar que s’enviï la instància en format digital per reenviar-la als grups municipals.

**1.10.2015** Arriba a alcaldia la moció en format digital

**2.10.2015** Des d’alcaldia s’envia un correu a la PAH demanant, a instancia
del secretari, la moció traduïda al català.

El mateix dia la PAH contesta que es tracta d’una moció estatal i que no la té
traduïda.

**6.10.2015** Des d’alcaldia s’envia un correu on es demana a la PAH si li
sembla correcte que la moció es passi al consorci per a la seva traducció.

La PAH respon que perfecte.

**7.10.2015** El Bloc Olesà estima pertinent recolzar la moció, si es tenen en
compte unes petites modificacions que considera que cal introduir. Ho comunica
als portaveus municipals.

**7.10.2015** Els portaveus diuen que no han rebut la moció de la PAH. El
mateix dia 7 se’ls envia còpia.

**8.10.2015** Des d’alcaldia s’envia un correu a la PAH demanant que es posi en
contacte de manera urgent amb el regidor d’habitatge.

No hi ha resposta ni trucada.

**10.10.2015** El regidor d’habitatge es posa en contacte amb la persona de la
PAH que signa la instància per explicar-li els punts de la moció que, si volen
que el Bloc la presenti, caldria modificar.

**6.11.2015** La regidora de Movem envia un correu al regidor d’habitatge
preguntant si la moció de la PAH va al Ple de novembre.

**9.11.2015** Es respon a la regidora que el Bloc no la presentarà, doncs
encara està esperant que la PAH els respongui sobre la proposta de modificació.

**10.11.2015** La regidora de MOVEM envia al grup de portaveus un correu on diu
que el seu grup ha entrat la moció de la PAH, tot dient que creuen
imprescindible que entri al Ple de novembre i que, en nom de la PAH, demana que
ens hi sumem.

La moció que presenta no contempla la modificació proposada pel BLOC.

**10.11.2015** La PAH envia un correu al regidor d’habitatge on diu que la PAH
no accepta cap tipus de modificació.

**18.11.2015** La Junta de Portaveus reunida abans del Ple acorda limitar el
nombre de mocions que hi entraran a tres (ja que se’n presentaven 10). La de la
PAH no és una de les seleccionades per la Junta de portaveus, ja que veu altres
com a més urgents.

No es contempla com a urgent la moció de la PAH, ja que el que demanen es que
el govern de l’Estat que sorgeixi de les eleccions del 20 de desembre aprovi
una Llei Reguladora del Dret a la Vivenda, d’acord amb les mesures que presenta
la moció i també, perquè el PSC i el Bloc són partidaris d’introduir alguns
matisos a la moció.

 

Les modificacions que vol introduir el Bloc Olesà són les següents:

Del punt 3.3, “Les persones i unitats familiars en situació de vulnerabilitat que no poden afrontar el pagament de lloguer d’habitatges obtindran ajudes que garanteixin evitar el desnonament”. Proposem, perquè és així, afegir que l’administració que s’ha de fer càrrec de donar aquestes ajudes és l’Estat.

Del punt 3.4, “En cap cas es podrà realitzar un desallotjament o desnonament de persones en situació de vulnerabilitat, ja sigui per impagament del lloguer o l’ocupació precària motivada per la manca d’habitatge, sense que l’administració competent garanteixi un reallotjament adequat”, proposem aclarir que l’administració competent és l’Estat.

També vam proposar afegir un darrer punt per tal que de l’acord de la moció se’n doni compte al Govern de l’Estat, al Congrés de Diputats, al Parlament de Catalunya, a la ACM, la FCM i a la FEMP.

 

No creiem que les modificacions que proposem des del Bloc Olesà desvirtuïn el contingut de la moció. Tan sols volem deixar constància de quina ha de ser l’administració responsable de garantir tant els reallotjaments com les ajudes.

I si el Bloc Olesà vol que la moció contempli aquestes aportacions és perquè, hores d’ara, ja estem destinant molts esforços a aquests ajuts:

El 2015, i fins al 17 de novembre, s’han destinat 5.640€ a l’ajut de lloguers, 11.670€ a l’ajut pel subministraments de l’aigua, 10.439€ al subministrament del gas, i 40.034 al subministrament elèctric.
En total 67.603€ que han beneficiat a un total de 801 unitats familiars.

Més enllà dels esforços econòmics, també fem esforços per poder ampliar el nombre d’habitatges gestionats per la nostra Oficina Local d’Habitatge (OLH).
En aquest sentit el passat 5 de novembre es va presentar l’estudi sobre el Parc d’habitatge buits a Olesa, tant de particulars com d’entitats financeres, per tal d’estimular positivament la seva mobilitat a través de l’OLH.
També es fan les gestions administratives per tal de sancionar i establir recàrrecs en l’ impost de l’IBI a les persones jurídiques propietàries de pisos buits. Aquest, però, és un tema molt complex que hem explicat moltes vegades. Volem afegir, en tot cas, la darrera de les vicissituds: el recàrrec del 50% en l’ impost de l’IBI sembla difícil d’aplicar perquè la llei no va acompanyada del seu reglament. Per tant, l’Organisme recaptador de la Diputació, ara per ara, no ens permet aplicar aquest recàrrec.
