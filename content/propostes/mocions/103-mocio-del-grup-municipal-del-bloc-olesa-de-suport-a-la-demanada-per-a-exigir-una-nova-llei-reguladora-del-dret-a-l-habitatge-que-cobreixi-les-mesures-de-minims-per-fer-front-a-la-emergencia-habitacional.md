+++
title = "MOCIO DEL GRUP MUNICIPAL DEL BLOC OLESA DE SUPORT A LA DEMANADA PER A EXIGIR UNA NOVA LLEI REGULADORA DEL DRET A L\"HABITATGE QUE COBREIXI LES MESURES DE MÍNIMS PER FER FRONT A LA EMERGÈNCIA HABITACIONAL."
tags = ["mocions"]
type = "blog"
date = "2015-12-11"
author =  "Jordi Martínez"
menu = "mocions"
+++
### EXPOSICIÓ DE MOTIUS

L'actual crisi econòmica i l'augment de l'atur ha impactat de manera dramàtica
en la vida de milers de persones que, a causa de les dificultats econòmiques
sobrevingudes, no poden cobrir les seves necessitats més bàsiques. Aquesta
situació ha portat a què moltes famílies no puguin fer front a les quotes
hipotecàries o de lloguer del seu habitatge habitual.

Això s'ha traduït en milers de desnonaments a tot l'estat espanyol, i en què
centenars de persones han vist vulnerat el seu dret a un habitatge digne,
havent d'afrontar situacions de greu vulnerabilitat, precarietat extrema,
pobresa i exclusió social, econòmica i residencial.

Segons dades del Consell General del Poder Judicial des de 2007 fins al primer
trimestre del 2015, s'han produït a l'Estat Espanyol 624.690 execucions
hipotecàries, 8.178 en el primer trimestre del 2015. A aquestes alarmants
xifres s'han de sumar l'augment de les dificultats per afrontar el pagament del
lloguer, que cada vegada afecta més persones. El CGPJ ha comptabilitzat la
preocupant xifra de 397.954 desnonaments des de l'inici de la crisi el 2007
fins al primer trimestre del 2015, només en el primer trimestre de 2015 s'han
executat 9.917 desnonaments.

Estem davant d'una situació d'emergència i vulnerabilitat habitacional que
s'incrementa per l'existència d'un mercat de lloguer escàs, car i
preocupadament especulatiu, i per la falta d'un parc públic d'habitatge social:
menys d'un 2% de l'habitatge construït. Tot això constitueix una autèntica
anomalia en el context europeu. A més, com denuncia l'informe "Emergència
Habitacional a l'estat espanyol", elaborat per l'Observatori DESC i la
Plataforma d'Afectats per la Hipoteca, aquesta situació empitjora encara més
pel fet que Espanya és el país d'Europa amb més habitatge buit, 13,7%, del parc
total (3 milions i mig de pisos buits segons el cens estatal d'habitatge de
2011).

El context descrit de vulnerabilitat i d’emergència en què es troba gran part
de la població s'està traduint també en un significatiu augment de les
ocupacions d'habitatge. Una forma de fer efectiu el dret a l'habitatge que
augmenta el grau de vulnerabilitat social de qui s'ha vist empès a aquesta
forma d'accés a un habitatge.

També resulta alarmant el creixent nombre de persones afectades per la pobresa
energètica, entesa com la dificultat per poder pagar les factures dels
subministraments bàsics d'electricitat, aigua i llum. Els preus d'accés i
consum d'aquests subministraments, que han crescut de forma exponencial, s'han
tornat inassequibles per gran part de la ciutadania.

Aquesta situació d'emergència social que pateixen les persones en situació de
vulnerabilitat, contrasta de forma aclaparadora amb els ingents beneficis
obtinguts per les entitats financeres i les empreses subministradores.

La Declaració Universal de Drets Humans (article 25) i el Pacte Internacional
dels Drets Econòmics, Socials i Culturals, en article 11, reconeix "el dret de
tota persona a un nivell de vida adequat per a si mateix i la seva família, a
l’alimentació, vestit i habitatge adequats, i a una millora contínua de les
condicions d'existència. Els estats membres han de prendre mesures apropiades
per assegurar l'efectivitat d'aquest dret..”

En el marc jurídic nacional, l'article 47 CE proclama del dret a un habitatge
digne i adequat, així com el deure dels poders públics de promoure les
condicions necessàries i les normes pertinents per fer efectiu aquest dret, i
l'article 33 declara la funció social de l'habitatge.

L'article 267 Tractat de la Unió Europea declara la primacia del Dret
Comunitari (STJUE 1978.03.09, Assumpte 106/77 cas Simmenthal) que desplaça el
Dret nacional (art. 93 CE, cessió competències en relació a l'art. 96 CE, els
tractats internacionals celebrats formaran part de l'ordenament intern).

En relació a l'anteriorment exposat i concretant en l'àmbit que ens ocupa, la
regulació del procediment d'execució hipotecària en la vigent Llei
d'Enjudiciament Civil infringeix la normativa comunitària. Aquesta legislació
és, per tant, il•legal en ser d'obligat compliment pel jutge nacional, patint
d'un vici radical que determina la seva nul•litat de ple dret. En aquest sentit
s'han manifestat reiteradament diferents sentències del Tribunal de Justícia de
la Unió Europea (Cas Aziz, cas Sanchez Morcillo i cas Monika Kusionova).

A Catalunya es va presentar, al mes de juliol del 2014, una Iniciativa
Legislativa Popular (ILP) promoguda per la Plataforma d'Afectats per la
Hipoteca, l'Aliança contra la Pobresa Energètica i l'Observatori DESC,
recollint així un clam de la ciutadania preocupada per la alarmant situació
d'emergència habitacional.

Aquesta ILP és avui una realitat. El passat 29 de juliol de 2015, el Parlament
de Catalunya va aprovar la Llei 24/2015 de mesures urgents per afrontar
l'emergència en l'àmbit de l'habitatge i la pobresa energètica. Aquesta
victòria en l'àmbit autonòmic, ens demostra que fer efectiu el dret a
l'habitatge és una qüestió merament de voluntat política.

Amb ocasió de les properes eleccions generals, la Plataforma d'Afectats per la
Hipoteca ha fet pública una sèrie de mesures de mínims, que considera necessari
i imprescindible incloure en una futura Llei reguladora del Dret a l'Habitatge.
Aquestes mesures impliquen reformes profundes i valentes però alhora factibles,
ja que majoritàriament estan recollides en l'abans esmentada Llei 24/2015.

Per tot això exposat, el grup municipal del Bloc Olesà proposa al Ple l’adopció
dels següents acords:

### ACORDS

Únic. Donar suport a les propostes presentades per la Plataforma d’afectats per
la Hipoteca i incloure les mesures següents en una llei reguladora del Dret a
l’Habitatge.

Primer: Mesures de segona oportunitat

* Dació en pagament retroactiva i condonació del deute (modificació de la Llei
Hipotecària i la Llei d'enjudiciament civil).
*  Eliminació automàtica, per part de les entitats bancàries i sense prèvia
   petició del titular de les clàusules declarades abusives per les sentències
   del Tribunal Superior de Justícia de la Unió Europea.
* No es podrà executar la primera i única vivenda tant dels titulars com dels
  avaladors per exigir la seva responsabilitat, amb vista a considerar
  l'habitatge habitual com un bé inembargable.
* Eliminació de tots els requisits restrictius per accedir a la moratòria de
  desnonaments i al Codi de Guindos, excepte habitatge habitual, deutor/a de
  bona fe i manca de recursos

Segon: Lloguer digne

* La regulació del lloguer a favor de la part més feble dels contractes
  d'arrendament, introduint mecanismes de seguretat en la tinença, estabilitat
  en la renda i allargant el termini mínim de durada del lloguer, com a mínim
  fins als 5 anys. Quan l'arrendatari pertanyi a un col•lectiu especialment
  vulnerable es produirà una pròrroga automàtica del contracte de lloguer, si
  així ho manifesta, que serà obligada quan l'arrendador sigui un banc o gran
  propietari d'habitatges.

Tercer: Habitatge garantit

* Les entitats bancàries han de garantir un lloguer social per a les persones
  deutores de bona fe, i les seves unitats familiars, que havent cedit el seu
  habitatge únic i habitual en dació en pagament no disposin d'alternativa
  habitacional.
* Els grans tenidors d'habitatge, especialment les entitats financeres i
  filials immobiliàries, fons voltor, entitats de gestió d'actius (inclosos els
  procedents de la reestructuració bancàries i entitats immobiliàries), han de
  garantir un lloguer social per a les persones i unitats familiars en situació
  de vulnerabilitat que no puguin fer front al pagament del seu habitatge i no
  disposin d'alternativa habitacional.
* Les persones i unitats familiars en situació de vulnerabilitat que no puguin
  fer front al pagament del lloguer d'habitatges obtindran ajudes que els
  garanteixin evitar el desnonament.
* En cap cas es podrà realitzar el desallotjament o desnonament de persones en
  situació de vulnerabilitat, ja sigui per impagament de lloguer o ocupació en
  precari motivada per la manca d'habitatge, sense que l'administració
  competent garanteixi un reallotjament adequat.
* En el cas que es dugui a terme el lloguer social en un habitatge diferent de
  la que resideix la família o persona en situació de vulnerabilitat, aquest
  reallotjament es produirà a la zona on aquestes tinguin les seves xarxes
  vitals i socials.
* Creació d'un parc públic d'habitatges a través de la mobilització de pisos
  buits en mans d'entitats financeres i filials immobiliàries, fons voltor,
  entitats de gestió d'actius (inclosos els procedents de la reestructuració
  bancàries i entitats immobiliàries). L'administració ha de regular mitjançant
  llei els mecanismes que possibilitin aquesta mobilització.
* En totes aquestes mesures el preu a pagar en concepte de lloguer social no
  superarà el 30% dels ingressos de la unitat familiar, incloses despeses de
  subministraments, d'acord amb els estàndards de Nacions Unides, sempre que
  els ingressos familiars superin el salari mínim professional 648,60 €; en cas
  contrari el preu a pagar en concepte de lloguer serà del 10% dels ingressos i
  els subministraments aniran a càrrec de les empreses subministradores (punt
  següent).

Quart: Subministraments bàsics

* Impedir els talls de subministrament bàsics d'aigua, llum i gas de les
  persones i unitats familiars en situació de vulnerabilitat.
* El pagament dels subministraments bàsics per a les famílies en aquesta
  situació es farà d'acord amb la capacitat adquisitiva de la unitat familiar,
  sempre respectant els estàndards de Nacions Unides.
* Els costos associats a garantir aquest dret i el deute acumulat que no puguin
  ser coberts per les persones vulnerables seran assumits per les empreses
  subministradores.

Cinquè: Creació d'un observatori de l'habitatge

Aquest observatori estaria integrat per representants de les institucions i de
la societat civil. Aquest observatori serà l'encarregat d'investigar i
analitzar la situació de l'habitatge a Espanya. Entre les seves funcions
estarien fer censos periòdics d'habitatges buits, fer seguiment de les
polítiques públiques, elaborar d'informes; comptaria amb capacitats no només
consultives sinó també control, seguiment, denúncia, executives i de proposta
legislativa.

Sisè:

L’administració competent que ha de garantir les ajudes de què parla el punt
tercer serà l’estatal i, cas d’estar transferida la competència, serà
l’autonòmica.

Setè:

Donar trasllat de l’acord al Govern de la Nació, al Congrés de Diputats, al
Parlament de Catalunya, a la ACM, la FMC i a la FEMP.
