+++
title = "MOCIÓ DEL GRUP MUNICIPAL BLOC OLESÀ DE L’AJUNTAMENT D’OLESA DE MONTSERRAT SOL•LICITANT A LA GENERALITAT DE CATALUNYA QUE GARANTEIXI BEQUES-MENJADOR A TOTS ELS INFANTS EN SITUACIÓ DE VULNERABILITAT."
tags = ["mocions"]
type = "blog"
date = "2015-11-18"
author =  "Jordi Martínez"
menu = "mocions"
+++
### EXPOSICIÓ DE MOTIUS
Ateses les creixents dificultats de moltes famílies de la nostra ciutat per
oferir una alimentació adequada, diversificada i de qualitat als seus fills i
filles per problemes econòmics, derivats tant de l’atur creixent com de les
retallades en els diferents programes de garantia de rendes (RMI, prestació
contributiva d’atur, Prepara...)

Atesa l’existència d’una situació significativa de malnutrició infantil
reconeguda tant per entitats socials (com Creu Roja o Càritas) com per diverses
instàncies de la Generalitat: segons l’IDESCAT (estadística oficial) 50.000
catalans menors de 16 anys no poden menjar carn o peix cada 2 dies per
privacions materials; esment a la problemàtica en diversos pactes recentment
signats com el pacte contra la pobresa o el pacte per la infància; diverses
crides per part de la presidència de la Consell Assessor de la Generalitat per
Polítiques Socials i Familiars; reiterats informes elaborats per la Sindicatura
de Greuges...

Atès que la funcionalitat dels menjadors escolars ha anat evolucionant. Fa uns
anys era bàsicament un recurs per millorar les oportunitats educatives i per
facilitar la conciliació laboral-familiar. Ara també es planteja com una eina
per garantir la suficiència alimentària dels infants vulnerables en un entorn
socialment normalitzat.

Atesa la responsabilitat competencial de la Generalitat de Catalunya en aquesta
matèria: art.166 del EAC de Serveis Socials, Voluntariat, Menors i Promoció de
les Famílies i Decret 160/1996 que regula el servei escolar de menjador als
centres docents públics.

Atès que, malgrat l’agreujament de la situació social, la partida
pressupostària que dedica la Generalitat a les beques menjador porta 4 anys
congelada.

Atès que aquesta combinació de necessitats creixents i recursos congelats ha
implicat un enduriment de la situació exigida als usuaris per accedir al
sistema de beques.

Atès que aquest canvi de criteris implica una important reducció del nombre de
beques i suposa també deixar d’atendre infants que sí s’atenien els cursos
anteriors. Alhora cal esmentar que els canvis introduïts per la Conselleria han
implicat una pèrdua de cobertura en tant que es deixa fora del sistema a
gairebé la meitat de famílies que realitzen la sol•licitud de beca.

Atès que aquest curs escolar el Consell Comarcal del Baix Llobregat ha rebut
més d’11.000 sol•licituds d’ajuts de menjador, la més alta registrada en els
darrers cursos escolars.

Atès que a data 2 d’octubre, amb el curs ja començat, s’han atorgat un total de
5.147 beques garantides, xifra que no arriba al 50% de les sol•licituds
presentades.

Atès que, en la mateixa data, la Generalitat va comunicar al Consell Comarcal
del Baix Llobregat que la quantitat destinada a la dotació flexible, és a dir,
aquella quantitat econòmica destinada a cada territori per a cobrir la resta de
beques no garantides patirà una retallada de 250 mil euros respecte el curs
anterior, el que suposa pràcticament la meitat del pressupostat anteriorment. 

Més de 2.000 nens i nenes es troben a la comarca en aquesta situació. Aquestes
famílies reuneixen les condicions d’accés als ajuts de menjador, ja que no
superen els límits econòmics fixats per la Generalitat per tot Catalunya. Tot i
no tenir accés a una beca garantida, aquestes famílies tenen importants
dificultats econòmiques i socials; cal remarcar que un 55% de les mateixes
estan en un pla d’intervenció dels serveis socials municipals.

Atès que l’Ajuntament d’Olesa de Montserrat, malgrat no tenir responsabilitats
competencials al respecte, ha anat complementant de forma important l’ajut a
les famílies per poder complementar l’alimentació dels infants .

Atès que la societat civil olesana a través d’entitats també ha fomentat una
campanya de finançament.

Atès l’acord del Parlament de Catalunya pres per unanimitat del 4 de juliol de
2013 en el sentit d’una major dotació pressupostària per garantir que cap nen o
nena del nostre país que ho necessiti es quedi sense beca de menjador per
limitacions pressupostàries.. 

És per tot això que el Grup Municipal Bloc Olesà proposa al Ple de l’Ajuntament
l’adopció dels següents:

### ACORDS

Primer.- Sol•licitar a la Generalitat de Catalunya que habiliti les partides
pressupostàries adients per tal que es puguin atorgar pel curs 2015-2016 les
beques-menjador necessàries per atendre les sol•licituds avaluades pels serveis
socials, per tal de garantir un àpat equilibrat al dia a tots els infants en
situació de vulnerabilitat.

Segon.- Traslladar l’anterior acord al Govern de la Generalitat, a la Mesa del
Parlament de Catalunya, als grups polítics del Parlament de Catalunya, al
Consell Comarcal del Baix Llobregat, a la Federació de Municipis de Catalunya i
a l’Associació Catalana de Municipis.
