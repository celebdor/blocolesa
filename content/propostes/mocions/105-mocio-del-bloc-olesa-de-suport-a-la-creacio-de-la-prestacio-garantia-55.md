+++
title = "MOCIO DEL BLOC OLESÀ DE SUPORT A LA CREACIÓ DE LA PRESTACIÓ “GARANTIA +55”."
tags = ["mocions"]
type = "blog"
date = "2015-12-15"
author =  "Jordi Martínez"
menu = "mocions"
+++
Atès el context de massiva desocupació en que ens trobem, on el 19,6% de la
població activa major de 55 anys es troba a l’atur amb escasses perspectives de
recuperació d’una activitat laboral amb garanties després de perdre el lloc de
treball, amb menys capacitat per a modificar les seves trajectòries laborals i
empentats a un retir avançat del mercat de treball, és essencial que hi hagi
una major vinculació entre les polítiques d’ocupació i les de protecció per
desocupació.

Donat que el col•lectiu de persones desocupades de més de 55 anys és més
vulnerable a caure a l’atur de molt llarga durada -composen el 54,2% del total
d’atur de molt llarga durada-, és evident que en poc temps gran part d’aquest
col•lectiu passarà d’un nivell de protecció contributiu a un d’assistencial, en
el millor dels casos, i deixaran de cotitzar a efectes d’una futura possible
pensió de jubilació, excepte els que tinguin dret a rebre el subsidi de majors
de 55 anys que cotitzaran tan sols per la base mínima de cotització.

Ja que les darreres reformes normatives relatives a la percepció de subsidis
per desocupació, a partir dels Reials Decret-Llei 20/20012 i 5/2013, s’han
dirigit a reduir la intensitat de la protecció elevant l’edat de les persones
beneficiàries del subsidi de majors de 52 anys a 55, passant a computar els
ingressos de tota la unitat familiar per accedir-hi, limitant el temps durant
pel qual es pot cobrar i reduint la cotització per jubilació del subsidi de
majors de 55 anys, què és l’únic subsidi que cotitza, del 125% de la base
mínima de cotització al 100%, podem suposar una altra ràpida reducció de la
cobertura de la protecció per desocupació en els majors de 55 anys.

També és una realitat que les darreres reformes en la protecció per desocupació
han prioritzat la suficiència econòmica del sistema de prestacions per
desocupació. Entenem doncs que s’ha deixat sense protegir adequadament a les
persones majors de 55, i fins i tot de més de 45 anys, oblidant-se de tot un
col•lectiu que pot quedar fins i tot fora del sistema de protecció o deixar-lo
al marge dels sistemes assistencials i empènyer-los a futures pensions de
jubilació per sota de mínims, fins i tot després de llargues carreres de
cotització.

Constatem que aquestes reformes han trencat el trànsit cap a la jubilació
d’aquelles persones que són expulsades del mercat de treball en edats pròximes
a la jubilació, limitant les estratègies d’accés a una pensió de jubilació més
avantatjosa en funció de les trajectòries laborals personals i possibilitats
individuals.

Constatem que aquests fets afecten directament a les rendes actuals d’aquest
col•lectiu de persones que en moltes ocasions tenen encara càrregues familiars,
acabant la seva vida activa cobrant, si compleix requisits, un subsidi de 426
euros mensuals, un fet que també afecta als seus futurs drets de jubilació i a
la quantia d’aquesta, doncs els anys en desocupació en un context on s’ha
augmentat l’edat ordinària de jubilació els penalitzarà durament, ja sigui
perquè augmentaran proporcionalment els anys de cotització mínima per al càlcul
de la base reguladora de pensió, o perquè hi hauran més llacunes de cotització
que s’ompliran fins i tot amb la meitat de les bases mínimes de cotització, o
be perquè se’ls aplicarà coeficients reductors de pensió perquè hauran
d’anticipar les seves jubilacions.

Gairebé 8 de cada 10 persones a l’atur de més de 55 anys o bé cobren un subsidi
de més de 426 euros mensuals, o bé reben la renda mínima d’inserció o algun
altre ajut econòmic, o bé no cobren res. Entenem que aquest col•lectiu és molt
més vulnerable a patir la pobresa i l’exclusió social

Entenem que per aquest col•lectiu que voreja l’edat de jubilació és
especialment important que es prioritzin actuacions que reforcin la seva
contribució al sistema de la Seguretat Social per tal que se’ls asseguri unes
pensions dignes.

Per tot això, el grup municipal Bloc Olesà proposa al Ple l’adopció dels acords
següents:

Primer.- Donar suport a la proposta impulsada per la UGT de Catalunya,
anomenada “Garantia +55”, que significa el següent:

1. Crear la prestació "Garantia +55" per a les persones de més de 55 anys que
   es trobin en situació de desocupació, tinguin cotitzats un mínim de 15 anys
   i hagin exhaurit la prestació per desocupació contributiva. Aquesta
   prestació serà una proposta integral i que contempla:

   1. Una prestació econòmica igual al SMI vigent.

   2. el manteniment d’entre un 90 i el 100% del còmput de les bases de
      cotització en la mateixa quantia que la darrera prestació contributiva
      per desocupació durant la percepció d’aquesta prestació, a efectes del
      càlcul de les prestacions de Seguretat Social per IP, mort i
      supervivència i jubilació.

2. Apostar per un SMI de 1.000 euros que ens apropi als estàndards dictats per
   la Carta Social Europea.

3. Afrontar la gran complexitat i fragmentació dels diferents sistemes de
   protecció. Cal simplificar i racionalitzar l’actual sistema de prestacions,
   i cal fer-ho amb la col•laboració de l’administració central. En
   conseqüència, abordar i millorar les iniquitats de les prestacions, i
   assegurar la compatibilitat de les prestacions econòmiques amb altres rendes
   (salarials o no).

4. Finalitzar amb l’estigmatització de les persones desocupades en general i,
   concretament, de les majors de 55 anys. Només d’aquesta manera podrem
   millorar l’ocupabilitat d’aquestes persones.

5. Analitzar l’eficiència dels programes d’inserció i apostar per accions
   d’inserció i deformació amb continguts reals.

Segon.- Notificar el present acord a la Secretaria Comarcal de la UGT del Baix Llobregat.
