+++
title = "MOCIÓ DEL GRUP MUNICIPAL BLOC OLESA DE L’AJUNTAMENT D’ OLESA DE MONTSERRAT CONTRA LA LEY 4/2015, de 30 de Març DE PROTECCIÓ DE LA SEGURETAT CIUTADANA."
description = "Moció contra la llei de la seguretat ciutadana"
tags = ["mocions"]
type = "blog"
date = "2015-11-18"
author =  "Jordi Martínez"
menu = "mocions"
+++
### EXPOSICIÓ DE MOTIUS

Més de vint anys després de l'aprovació de la polèmica Llei de Seguretat
ciutadana 1/92, també denominada “Llei Corcuera”, que va tombar en gran part
del seu articulat el Tribunal Constitucional, el Govern del PP ha sancionat, en
la seva tònica del “reformazo regressiu”, una nova Llei, que ve a substituir la
ja criticable llei del 92, que va costar el càrrec al llavors Ministre de
l'Interior socialista.

Un text redactat en paral•lel a la reforma del Codi Penal, també de marcat
caràcter restrictiu en drets, i que ve a compensar l'eliminació de la majoria
de les infraccions penals tipificades com a faltes, que amb la nova Llei de
Seguretat Ciutadana passarien a considerar-se infraccions administratives de
caràcter molt greu, greu o lleu.

La discrecionalitat que atorga a l'administració a l'hora d'establir sancions,
l'elevada quantia de les mateixes, que en el cas de les infraccions molt greus
podrien arribar als 600.000€ i la fixació en la regulació de conductes
habituals en les protestes ciutadanes, defineixen a aquesta Llei com la “Llei
de la por” o la “Llei Mordassa”. S'habilita, per tant, un procediment
administratiu que legalitza la criminalització i persecució de les
mobilitzacions i crea un instrument governamental, per impossibilitar drets
democràtics bàsics com la llibertat d'expressió i de manifestació que recull la
nostra carta magna com a drets fonamentals.

La norma tipifica noves figures sancionables, considerant tals els escarnis (a
diferència de l'opinió del president del Suprem, Gonzalo Moliner, que va
declarar que "no és possible dir a priori si un escarni és o no legal"), les
concentracions davant el Congrés i el Senat o l'escalada en façanes oficials
(com va ocórrer amb el Palau de les Corts). Sanciona l'ús de caputxes en
manifestacions, faculta a la policia a establir “zones de seguretat” en
perímetres urbans, i habilita a les forces i cossos de seguretat a retirar de
forma expeditiva vehicles que taponen una via pública, en cas que els
concentrats desobeeixin l'ordre dels antiavalots de buidar la zona. No se citen
expressament, però tot indica que apunta a impedir tractorades, marxes de
taxistes o de camioners.

Aquesta breu descripció d'algunes conductes que seran regulades per aquesta
llei, ens indica que aquesta “Llei Mordassa”, torna a situar la seguretat
ciutadana en l'òrbita del vell concepte d'ordre públic, molt allunyat de la
concepció democràtica i constitucional de la seguretat.

El propi Consell d'Europa, institució encarregada de vetllar pel respecte als
drets humans en el continent, ja va qualificar el projecte de llei d’ "altament
problemàtic". Nils Muiznieks, el seu responsable de drets humans, dubte que
"aquestes restriccions siguin necessàries en una societat democràtica" i creu
que s'hauria de vetllar per la seguretat "sense interferir massa en la
llibertat de reunió" i de manifestació.

Una altra llei més, per tant, que suposa una clara reculada social, un
anacronisme constitucional, un concepte d'ordre públic passat de moda i un clar
atemptat contra drets i llibertats democràtics, que són encotillats en
procediments sancionadors que pretenen impedir la protesta, la crítica i la
demostració cívica de desacord polític.

La democràcia és expressió i la seguretat ciutadana protecció de llibertats
públiques, no retallades de les mateixes. El delicte es combat de forma
integral: amb educació, amb la promoció d’una vida digna, amb la eliminació de
les diferències socials, amb bones polítiques de reinserció i amb unes
institucions que treballin en la prevenció, no en el càstig.

En conclusió, aquesta Llei, reiterativa a regular conductes que ja tipificava
el codi penal, i creadora d'un nou Estat policial, controlador i limitador de
llibertats públiques, és inadmissible en un entorn europeu democràtic.

### ACORDS

Primer.- Exigir al Govern de la Nació, la retirada immediata de la Llei
Orgànica 4/2015, de 30 de març, de Protecció de la Seguretat Ciutadana.

Segon.- Sol•licitem un debat consensuat entre totes les forces polítiques, per
a l'elaboració d'un text que reguli la matèria de seguretat ciutadana,
incloent, al debat així mateix, la Reforma de la Llei 2/1986, de 13 de març, de
Forces i Cossos de Seguretat de l'Estat, superada i obsoleta en molts dels seus
aspectes, així com la Llei Orgànica general penitenciària. Tot això, amb
l'objectiu, de reformar amb caràcter integral, tots aquells aspectes
relacionats amb la seguretat, que haurà de conformar un ampli debat social,
enfocat no únicament des dels tradicionals aspectes reactius sinó i
fonamentalment, preventius, enfocant l'anàlisi del delicte i la seva contenció,
com un problema social que abasta variables socioeconòmiques i que requereix un
ampli treball de caràcter transversal.

Tercer.- Exigim la dimissió del Ministre de l'Interior, impulsor de la present
Llei, els postulats de la qual són clarament preconstitucionals, i són
inconcebibles en una democràcia, així com, per la seva reiterada disposició de
conculcar els drets humans, a tenor de la disposició de legalització de les
“devolucions en calent” a la frontera de Ceuta i Melilla, contraris a la
normativa internacional i que tracten de manera immoral i inhumana a nombrosos
éssers humans a les nostres fronteres, que solament desitgen una vida millor.

Quart.- Donar trasllat de l'acord al Govern de la Nació i a tots els Grups
Parlamentaris del Parlament de Catalunya i del Congrés de Diputats.
