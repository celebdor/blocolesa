+++
title = "Participar vol dir decidir entre tots el nostre futur col·lectiu"
description = "La importància de la participació ciutadana"
tags = ["participacio"]
type = "blog"
date = "2014-04-09"
author =  "celebdor"
menu = "participacio"
+++
Alguns grups polítics que ens han governat aquests darrers anys no han cregut
mai en el **paper actiu dels ciutadans ni de les entitats cíviques**. Ben al
contrari, no només han mostrat escassa sensibilitat davant les nostres
opinions, sinó que també han posat en dubte la **legitimitat** de les entitats
per participar en el debat públic. Així, doncs, ciutadans i entitats hem quedat
relegats a un **paper subordinat** que ha pretès convertir-nos en comparses i
no en protagonistes d'aquelles decisions que afecten la nostra vila, és a dir,
la casa de tots.

L'Ajuntament d'Olesa va aprovar fa tres anys un nou **reglament de participació
ciutadana** que ha patit dos problemes bàsics en el seu desenvolupament:

1. S'han creat uns consells de participació que han consagrat en la seva
   composició l'**hegemonia dels partits polítics**.
2. No s'ha arribat a aplicar perquè força partits polítics de l'actual
   consistori han considerat la participació ciutadana com una nosa
   **innecessària**.

El resultat és que **la participació ciutadana a Olesa no existeix**.

Vetllar i treballar pel bon funcionament d'Olesa és un **deure** i un **dret**
que ens afecta a tots. Cal que a partir d'ara construïm un projecte comú en què
tots i totes siguem **corresponsables del futur col·lectiu**. Cal dotar la
ciutadania d'instruments suficients per tal de garantir aquesta participació,
un nou marc de presa de decisions que doni més legitimitat a les institucions.
Per això hem obert un **debat** on tots els ciutadans poden participar, per tal
d'elaborar un **nou Reglament de Participació Ciutadana** que defineixi un
**marc efectiu** de deliberació i decisió col·lectives, inclosos els
pressupostos municipals.

Cal adaptar l'estructura i el funcionament de l'Ajuntament d'Olesa al projecte
participatiu i posar en marxa **consells sectorials de participació i
decisió**, formats i presidits bàsicament per veïns i representants d'entitats.
Els consells que proposem crear són: **Entitats Solidàries, Voluntariat,
Joventut, Cultura, Festes, Esports, Barris, Ensenyament, Tercera Edat, Econòmic
i de Comerç**.

També volem posar en marxa el **Consell Municipal de Participació**, format per
representants dels consells sectorials i amb capacitat de decisió sobre
assumptes com ara els **pressupostos municipals**.

L’Ajuntament ha de garantir la **informació** i la **transparència** dels
acords que prengui mitjançant:

1. La comunicació directa amb les entitats, els ciutadans i els consells
   sectorials.
2. Els mitjans de comunicació municipal, potenciant el seu paper com a serveis
   públics.
3. Aprofitant el potencial de les noves tecnologies per a difondre i rebre
   informació.

En definitiva, cal posar a disposició de les **entitats** i els veïns tots els
**mitjans humans i materials** per afavorir la participació en el debat sobre
el model de ciutat que volem.
