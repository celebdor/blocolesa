+++
title = "Consulta"
description = "Consulta sobre l'ARE"
tags = ["ARE", "Consulta", "Participacio"]
type = "blog"
date = "2014-11-05"
author =  "celebdor"
menu = "participació"
+++
Per tal de fomentar la participació ciutadana en assumptes locals d’especial
rellevància per als interessos dels veïns, la Constitució, l’Estatut
d’Autonomia de Catalunya i la llei de consultes populars preveuen la
possibilitat de realitzar referèndums.

El creixement urbanístic d’ Olesa de Montserrat és, sens dubte, un assumpte
d’especial rellevància, ja que marca el futur i el disseny d’una població. Una
bona prova d’això és que quan es va aprovar la zona ARE d’ Olesa van ser moltes
les al·legacions presentades en contra, acompanyades de la signatura de més de
mil cinc-cents olesans, i, probablement, moltes més les que haurien volgut
dir-hi la seva.

Per tant, emparats per l’esperit i la lletra de les lleis, i pel ferm
convenciment de que cal introduir eines de participació directa en la política
local, l’Equip de govern del Bloc Olesà ja fa temps que vol preguntar a tots
els veïns tres qüestions cabdals relacionades amb el creixement urbanístic de
la nostra vila.

Aquestes tres preguntes són:

1. En el mandat consistorial de 2007-2011, la Generalitat de Catalunya va
   aprovar la creació d'una zona ARE ( Àrea Residencial Estratègica) a Olesa de
   Montserrat.

    **Vol que l'ajuntament d' Olesa de Montserrat demani a la Generalitat de
    Catalunya la supressió d'aquesta zona ARE?**

    SI/NO

2. Els terrenys del camp de futbol d' Olesa de Montserrat tenen la qualificació
   urbanística que permet construir-hi habitatge..

    **Vol que es preservin aquests terrenys per a ús d'equipaments?**

    SI/NO

3. L'actual Pla General d'Ordenació Urbana (PGOU) preserva el pla de Can
   Llimona, zona delimitada per la riera de Can Carreras a la dreta, i per les
   Planes a l'esquerra, com a zona agrícola.

    Atès que la Generalitat de Catalunya encara no ha aprovat definitivament
    l'ampliació del Parc Natural de Montserrat, que preservaria definitivament
    el pla de Can Llimona per aquest ús agrícola.

    **Vol que en el futur es mantingui la qualificació agrícola d'aquesta zona
    nord (pla de Can Llimona)?**

    SI/NO

Per poder realitzar aquets referèndum, calia que el Ple municipal ho
autoritzés. Això va succeir el 30 de gener de 2014, quan amb el vot favorable
de tots el grups, tret de l’abstenció del PP, el Ple hi va votar favorablement.

Un cop vam tenir el vist i plau del Ple, la proposta havia de passar pel
Departament de Governació de la Generalitat de Catalunya, qui, després de
temps, va d’autoritzar la consulta.

La Generalitat tenia 30 dies per autoritzar la consulta i enviar la proposta al
Govern de Madrid. Però la seva resposta ens va arribar el 16 d’abril, fora, per
tant, del termini previst i, a més, ens demanava que es reformulessin dues de
les tres preguntes.

A la primera pregunta, on deia “demanar la supressió de la zona ARE”,
proposaren “deixar sense efecte el desenvolupament”. Deixen igual la segona
pregunta i, en la tercera, tragueren l’ atès. Tota la resta quedà igual.

Aquestes noves preguntes van obligar a refer l’expedient per modificar-ne el
text, i obligaren a una nova votació al Ple, que per majoria absoluta ho havia
d’autoritzar. Abans però de dur els canvis al Ple, vam concretar amb el
Departament de Governació de la Generalitat el nou redactat. El vist i plau ens
arriba el dijous 24 d’abril, el mateix dia del Ple municipal d’abril.

Per tal de poder presentar el nou redactat al Ple d’abril, una hora abans del
Ple es convocà una Junta de Portaveus extraordinària. A la Junta només hi foren
presents els grups de CiU i del PSC, que no volien definir el seu vot
argumentant que ho havien de dur a les seves assemblees. Davant d’aquesta
negativa, la modificació de les dues preguntes no va anar al Ple d’abril, per
bé que tota la documentació es va preparar de manera urgent perquè hi pogués
anar.

Al llarg de tot el mes de maig vam demanar a tots els grups saber quin seria el
sentit del seu vot, ja que cal majoria absoluta per aprovar-ho. No va ser fins
els mateix dia del Ple de maig, el dijous 29, que en vam tenir resposta. Per
aquest motiu la proposta va entrar d’urgència el mateix dia del Ple. Cal
recordar també que a la Junta de Portaveus del mes de maig cap grup, tret del
Bloc Olesà, no hi va assistir.

Un cop reformulades, les noves preguntes que el Ple de maig va aprovar són
aquestes:

1. En el mandat consitorial 2007-2011, la Generalitat de Catalunya va aprovar
   la creació d’una zona ARE (Àrea Residencial Estratègica) a Olesa de
   Montserrat.

    Vol que l’Ajuntament d’Olesa de Montserrat demani a la Generalitat deixar
    sense efecte el desenvolupament de la zona ARE?

2. Els terrenys del camp de futbol d' Olesa de Montserrat tenen la qualificació urbanística que permet construir-hi habitatge..

    **Vol que es preservin aquests terrenys per a ús d'equipaments?**

3. El Pla General d’Ordenació d’Olesa de Montserrat vigent preserva el Pla de
   Can Llimona, zona delimitada per al riera de Can Carreres a la dreta i per
   Les Planes a l’esquerra, amb la qualificació d’agrícola.

    **Vol que el futur el planejament urbanístic municipal mantingui aquesta
    qualificació agrícola del Pla de Can Llimona?**

El resultat de la votació al Ple de maig va ser aquesta: Aprovat per quinze
vots a favor del Bloc Olesà, CiU, ERC i Entesa, cap vot en contra, i les
abstencions del PSC, PP i PxC.

Un cop aquestes preguntes van ser aprovades, es van enviar de nou al
Departament de Governació de la Generalitat, el qual pocs dies després les va
trametre al Ministeri d’Hisenda i Administracions Públiques, que és qui ha de
cursar l’ordre per tal que el Consell de Ministres ho aprovi definitivament.

A dia d’avui, 3 de novembre de 2014, un cop consultada la Delegació del Govern
i en contacte permanent amb el Departament de Governació de la Generalitat, ens
consta que la nostra petició de referèndum està en mans de l’advocacia de
l’estat. Quan arribi, si és el cas, al Consell de Ministres, i si aquests
aprova la Consulta, ho farà saber a la Generalitat, que serà qui ens ho
notificarà.

A dia d’avui, per tant, fa 10 mesos que el Ple de l’Ajuntament d’Olesa de
Montserrat va votar per primer cop i per majoria absoluta l’autorització per a
la realització del referèndum, i seguim esperant que el govern central ho
aprovi.

Volem recordar, també, que malgrat el Ple de gener va aprovar realitzar la
consulta, al Ple de febrer el PSC va entrar una moció per tal de modificar del
tot allò que s’havia aprovat. En la seva moció excloïa la possibilitat que els
olesans i olesanes poguéssim votar en una consulta sobre la zona ARE. Una moció
que va comptar amb el vots a favor de CiU i del PP, però que no va prosperar
gràcies els vots contraris del Bloc, l’Entesa i PxC.

És, per tant, voluntat de l’Equip de govern del Bloc Olesà, introduir formes de
participació directa en les decisions politiques, consultar els veïns pel
creixement urbanístic futur, i assumir com a vinculants els resultats dels
referèndums.
