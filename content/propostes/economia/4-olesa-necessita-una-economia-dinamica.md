+++
title = "Olesa necessita una economia dinàmica"
description = "Economia dinàmica per a Olesa"
tags = ["economia"]
proposts = ["economia"]
type = "blog"
date = "2014-04-09"
author =  "Salvador Prat"
menu = "economia"
+++
<img style="float: left;" src="/images/salva.jpeg"
alt="Fotografia Salvador Prat" width="180" height="225">
Fins fa uns anys, els nostres governants van apostar per allò que definien com
a ciutat residencial: un lloc per viure però no per treballar. Això s'ha
traduït en una desestructuració completa del teixit social, en un desarrelament
dels ciutadans respecte del municipi i en la falta de vitalitat econòmica del
municipi.

Dues dècades després de la crisi del tèxtil, un cop perdudes gairebé totes les
oportunitats, l'Ajuntament no ha fet gairebé res. Només ha iniciat una
inadequada política d'alliberament de sòl industrial, que no ha estat suficient
per atreure noves activitats econòmiques.

Si Olesa vol afrontar el futur amb un cert grau d'optimisme ha de repensar
també el seu desenvolupament econòmic i l'Ajuntament ha d'actuar com a agent
dinamitzador, ja sigui tornant a definir l'ús del seu territori perifèric, ja
sigui dotant a les zones industrials d'un valor afegit en forma de nous serveis
d'alt valor tecnològic, ja sigui a través de polítiques eficients d'ajut a la
implantació de noves activitats o amb d'altres que puguin aportar-se en un
estudi o debat obert en què tot el poble ha de participar.

Olesa només serà dinàmica si som capaços de fidelitzar els olesans al nostre
poble. Caldrà doncs assegurar-los bones ofertes relacionades amb l'oci, la
cultura, el lleure, la joventut, l'esport, el comerç, etc. I per això és
imprescindible que tots fem més vida al poble.

És necessària la creació d'un Consell econòmic i social amb la participació
d'empresaris, botiguers, sindicats, cambra de comerç i ciutadans per realitzar
polítiques actives de desenvolupament industrial i comercial.
