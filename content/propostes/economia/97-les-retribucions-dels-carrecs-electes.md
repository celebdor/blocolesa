+++
title = "Les retribucions dels càrrecs electes"
description = "Proposta sobre els càrrecs electes"
tags = ["economia"]
proposts = ["economia"]
type = "blog"
date = "2015-10-21"
author =  "Jordi Martínez"
menu = "economia"
+++
El passat 29 de juliol es va celebrar un Ple extraordinari on, entre d’altres
punts, es van debatre les dedicacions dels càrrecs electes i les seves
retribucions, i les assignacions per les assistències als òrgans col·legiats.

Parlar de dedicacions equival a parlar de sous que cotitzen a la seguretat
social. Aquestes dedicacions poden ser exclusives (100% de la jornada) o
parcials.

Les assignacions per assistència son les retribucions que reben els regidors
que no tenen dedicació. Que no tinguin dedicació no vol dir que no treballin,
vol dir que la seva retribució, que no cotitza a la seguretat social, la
perceben per les assistències efectives als òrgans col·legiats. Aquests son el
Ple, les Juntes de Govern i les Comissions informatives. Hi ha ajuntaments, com
veureu més endavant, que també retribueixen la Junta de Portaveus, però no és
el nostre cas.

La retribució dels càrrecs electes és un tema sempre delicat que demana alguna
reflexió i el màxim de transparència.

Des del Bloc pensem en primer lloc que la dedicació a la política ha de ser
retribuïda per varis motius, entre els quals en volem destacar un: la
retribució permet l’accés al treball en política de totes les persones al marge
de la seva condició social, laboral i del seu patrimoni.

Si donem per bo aquest argument, cal preguntar-se tot seguit quin ha de ser l’
import d’aquesta retribució. Aquesta es una qüestió que no ha estat mai
resolta, tot i que la Llei 27/2013 de racionalització i sostenibilitat de
l’administració local ha establert dos conceptes nous, al nostre entendre un
més afortunat que l’altre.

### Què diu la Llei?

D’una banda, l’art. 75 bis estableix el límit màxim total que poden rebre els
membres de les corporacions locals, i el compara amb les retribucions d’un
secretari d’estat. Així, per un ajuntament entre els 20 mil i els 50 mil
habitants, que és el cas d’Olesa de Montserrat, aquesta retribució màxima és de
55 mil € bruts anuals, i inclou tots els conceptes retributius i
d’assistències. Per una altra banda, l’art. 75 ter parla de la limitació de
càrrecs públics amb dedicació exclusiva. En aquest sentit diu que un municipi
entre el 20 mil i els 35 mil habitants, el nombre de dedicacions exclusives no
excedirà de 10.

Així doncs, i pel que fa al que diu la llei, l’ajuntament d’Olesa ha fixat el
sou màxim de l’alcaldessa en 45mil € anuals (el mateix que va percebre
l’anterior alcalde en el període 2011-2015), 10 mil € menys del que està permès
per llei.

Pel que fa al nombre de dedicacions exclusives dels càrrecs electes, tot i que
la llei en permet 10, l’ajuntament d’Olesa només en te 1.

### Quina és la mitjana Catalunya?

Una altre aproximació al tema de les retribucions la podem fer a partir del que
fan altres ajuntaments a casa nostra.

Segon les dades que aporta la Fundació Carles Pi i Sunyer d’estudis autonòmics
i locals, la mitjana retributiva dels alcaldes en els municipis entre 20 mil i
50 mil habitants és de 55.829€. En aquest cas el sou de la nostra alcaldessa es
de gairebé un 20% menys que la mitjana.

Pel que fa a la retribució dels regidors amb dedicació exclusiva, la mitjana a
Catalunya, també als municipis entre 20mil i 50 mil habitants, és de 47.447€. A
Olesa de Montserrat la dedicació exclusiva es de 36 mil€, gairebé un 25% menys.

També podem agafar una referència més propera, com ara els pobles del nostre
entorn i d’una mida similar.

### Quines són les retribucions als pobles similars del nostre entorn?

(les informacions les hem extret dels respectius webs municipals)

A **Esparreguera** (21.685 habitants) amb govern del PSC i ICV, l’alcalde amb
dedicació exclusiva percep la retribució màxima que permet la llei, 55mil€, i
els regidors amb dedicació exclusiva, que són 3, reben 44.500€.

A **Martorell** (27.895 habitants) amb govern de CiU i ERC, no hi ha dedicacions
exclusives, nomes hi ha dedicacions parcials repartides de la següent manera:
hi ha dues dedicacions retribuïdes en 21.700€ que cobren l’alcalde (CiU) i el
primer tinent d’alcalde (ERC), i nou dedicacions parcial de 14.500€. Aquestes
dedicacions demanen un mínim de 16 hores setmanals.

A **Sant Andreu de la Barca** (27.268 habitants) governat pel PSC, la
retribució de l’alcalde no la trobem enlloc.  Pel que fa a les dedicacions
exclusives de regidors, n’hi ha dues que estan retribuïdes en 51.520€ anuals.
Altres dedicacions parcials de 25 h/set. estan retribuïdes en 1500€ mensuals.

A **Sant Feliu de Llobregat** (43.715 habitants) amb govern de ICV, l’alcalde
amb dedicació exclusiva percep 55.172€ anuals, el màxim que permet la llei. Els
regidors amb dedicació exclusiva (6 en total) perceben entre 45 mil i 49 mil€.
Les dedicacions parcials de 2/3 parts de jornada estan retribuïdes en 30mil€
anuals, i les de 2/5 parts en 18 mil€ anuals.

A **Sant Vicenç dels Horts** (28.103 habitants) amb govern d’ERC i SVHSP
(podem), ni l’alcalde ni la primera tinent d’alcalde tenen sou assignat. Cal
dir que l’alcalde rep el seu sou com a diputat del Parlament de Catalunya i per
tant, diu al web, estalvia un sou de 54mil€ anuals (hem de pensar, per tant,
que aquesta seria la seva retribució municipal). Pel que fa als regidors amb
dedicació exclusiva perceben una retribució de 42mil€ anuals. Les dedicacions
parcials del 90% 37.800€, del 85% 35.700€, i successivament fins al 57% de
dedicació amb una retribució de 24mil€.

A **Olesa de Montserrat** (23.543 habitants) amb govern del Bloc Olesà,
l’alcaldessa 45mil€ anuals, un regidor amb dedicació exclusiva 36mil€ anuals, i
tres regidors amb dedicacions parcials del 50% i del 25% es reparteixen 26mil€
anuals.

### Resum

Amb aquests exemples veiem com ajuntaments veïns amb poblacions similars (tret
de Sant Feliu que te 43 mil habitants), governats per partits diversos (PSC,
ICV, CiU i ERC), en tots els casos, tots, les seves retribucions tant pel que
fa a les dedicacions exclusives com les parcials, son més altes que no pas a
Olesa.

En resum, tant pel que fa a la llei, a la mitjana de Catalunya o als pobles del
nostre entorn, les retribucions dels càrrecs electes amb dedicació a
l’Ajuntament d’Olesa són entre un 20 i un 25% més baixes.

### Les assignacions per assistència

Les assignacions per assistència són les indemnitzacions que reben els regidors
que no tenen dedicació, per assistir als òrgans de govern col·legiats (cal
assistir-hi realment). Els òrgans de govern són, essencialment, el Ple, les
Juntes de Govern, les Comissions Informatives i la Junta de Portaveus.

Aquestes són les indemnitzacions:

Poble          | Ple      | Jta Govern | Comissió inf | Jta. Portaveus
---------------|----------|------------|--------------|---------------
Esparreguera   | 285/190    | 162          | 95 | 0
Martorell | 492 | 120 | 172/77 | 200
St.Andreu | 141 | 705/329 | 235/150 | 173
St.Feliu Llob. | 242 | 184 | 116 | 0
St.Vicenç | 150 |   | 100 | 0
Olesa Mont. | 175 | 155 | 60 | 0
**Mitj.Catalunya** | 464 | 285 | 215 | 261

En aquelles caselles on hi surten dos imports, el primer és el que percep el
president. Per exemple, a St.Andreu de la Barca, el president de la junta de
govern, que és l’alcalde, rep 705€ per cada junta de govern. Els membres
restants, sempre de l’equip de govern, reben 329. També podeu veure que a la
darrera columna, la Junta de Portaveus, només està remunerada a Martorell i a
St. Andreu.

Un altre dada a tenir en compte la trobem a l’ajuntament de St.Vicenç dels
Horts. La Junta de Govern no està retribuïda perquè tots els regidors de
l’equip de govern tenen dedicació i, com hem dit, les dedicacions, ja siguin
parcials o totals, són incompatibles amb les indemnitzacions per assistència.
Això vol dir que a St.Vicenç només l’oposició rep indemnitzacions per
assistència tant al plens com a les comissions.

Per fer una comparativa entre ambdós ajuntaments, el de St.Vicenç dels Horts i
el d’Olesa de Montserrat, amb sistemes retributius diferents, trobem el següent
resultat:

Els 11 regidors de l’equip de govern de St.Vicenç tenen un cost de 285mil€
anuals (la suma de les retribucions de les dedicacions de 9 regidors, ja que
l’alcalde i la primera tinent d’alcalde no en perceben cap de l’ajuntament).

Els 12 regidors de l’equip de govern d’Olesa tenen un cost de 182mil€ (la suma
de les dedicacions de 5 regidors més les sumes de totes les juntes, els plens i
les comissions dels regidors que formen l’equip de govern).

Una diferencia de més de 100mil anuals.

Aquesta comparativa, a més d’avaluar dos sistemes retributius diferents, un
basat sobre tot en les dedicacions (Sant Vicenç) i l’altre més basat amb les
assignacions per assistència (Olesa), també demana una reflexió de tipus
polític.

Al Ple del passat 29 de juliol on es va debatre i votar aquest model
retributiu, tots els grups municipals es van abstenir tret del Bloc que,
òbviament, va votar a favor i ERC, que hi va votar en contra. No deixa de ser
curiós aquest vot en contra quan un ajuntament com el de Sant Vicenç,
encapçalat pel líder d’Esquerra, te un sistema retributiu més onerós per a les
arques municipals que no pas el nostre.

### Conclusió

Com queda explicat, l’Ajuntament d’Olesa de Montserrat, tant pel que fa a les
dedicacions com a les assignacions per assistència, és el que menys recursos
públics destina a les retribucions dels càrrecs electes entre els pobles del
nostre entorn i de mida similar en població, i també pel que fa a la mitjana de
Catalunya.
