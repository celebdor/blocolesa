+++
title = "Els serveis públics han de funcionar a satisfacció d’uns ciutadans exigents"
tags = ["serveis"]
type = "blog"
date = "2014-04-09"
author =  "celebdor"
menu = "serveis"
+++
![Foto Nico Segado](/images/nico.jpeg)
La suma de l'irracional creixement demogràfic d'Olesa i la inoperància dels
nostres governants, traduïda en una mala planificació, ha donat com a resultat
l'actual dèficit de serveis públics.

L'Ajuntament d'Olesa:

1. Incompleix les seves obligacions legals: encara no tenim un pla de mobilitat
   ni un pla d'emergència exterior operatiu.

2. Manté alguns dels serveis essencials sota mínims: sanitat, educació,
   recollida d'escombraries, neteja viària, seguretat ciutadana, trànsit,
   pavimentació, senyalització...

Si els polítics olesans no han estat capaços de complir amb les funcions
assignades fins ara, qui es pot refiar que sabran afrontar el nou repte de
futur.
Amb la vostra participació, els del Bloc Olesà:

* Redimensionarem els serveis municipals i els repensarem en funció del que
  demanen les lleis i el creixement demogràfic.

* Reclamarem a les administracions competents que compleixin amb les seves
  obligacions, especialment en els sectors educatiu, sanitari i atenció a la
  tercera edat.

* Serem escrupulosos amb les funcions que han de tenir les prestacions
  públiques. Els serveis municipals s'han d'a- justar als principis de justícia
  social, de redistribució de rendes i de benestar universal.

* Donarem suport i potenciarem la Plataforma d'entitats solidàries (PESO) per
  tal de: 1- col·laborar a integrar la població immigrant, 2- aplicar l'ajut
  del 0,7 % del pressupost municipal a ajuts per a tasques locals i
  internacionals i 3- potenciar i ajudar les entitats de solidaritat local
  (Càrites, ...).

* Eliminarem les barreres arquitectòniques d'Olesa.

* Fomentarem la col·laboració i l'ajut a les entitats culturals. Crearem uns
  consells de participació que gestionin i planifiquin els equipaments, les
  infrastructures i els serveis de caràcter públic.

* Defensarem els interessos de la comunitat, no transigint davant les empreses
  que no han respectat els contractes signats amb l'Ajuntament (és el cas,
  entre d'altres, de la recollida d'escombraries, adjudicada a Fomento de
  Construcciones y Contratas) i que han contribuït de manera decisiva al
  desànim dels olesans davant uns serveis que no funcionen.
