+++
title = "Hisenda"
description = "Proposta d'hisenda pel mandat 2015"
tags = ["hisenda"]
type = "blog"
date = "2015-01-13"
author =  "celebdor"
+++
![Foto Nico Segado](/images/nico.jpeg)
El pressupost públic d’un ajuntament, conjuntament amb les ordenances fiscals
expressen les prioritats de qualsevol Equip de Govern.

L’equip de Govern del Bloc Olesà, donada la crisi econòmica que malauradament
de moment no s’allunya, proposa continuar una política econòmica marcada
necessariament  per l’austeritat, en el sentit clàssic de no malgastar; cosa
que no vol dir aplicar una política de retallades, ja que es contempla  un
increment de les partides socials, d’educació, de serveis a les persones i  de
manteniment i millora d’espais públics.

Durant el mandat 2011-2015 hem sanejat l’economia de l’ajuntament, hem complert
tots els ratis econòmics  que l’administració demana i hem estat estrictes amb
la regla de la despesa. Aquests factors ens han permès ser el poble del Baix
Llobregat que paga els seus proveïdors en el marge més curt de temps: 31,3
dies. Si tenim en compte que molts dels nostres proveïdors són olesans, el
guany que això representa és encara més important.

Per a la propera legislatura proposem les següents 12 mesures econòmiques:

1. Els impostos en cap cas s’incrementaran per sobre de l’IPC. En el període
   2011-2015 han estat, de promig, gairebé congelats.
2. Es revisaran els preus de les taxes per fer-los tan progressius com sigui
   possible.
3. El sistema de subvencions a les entitats seguirà fent-se en funció de les
   seves activitats i amb un sistema de baremació objectiu i transparent.
4. Mantindrem i augmentarem una línea de bonificacions tributàries en totes les
   activitats productives que contribueixin a la creació de llocs de treball o
   al desenvolupament econòmic d’Olesa de Montserrat.
5. Augmentarem les bonificacions i les subvencions de caire social o
   mediambiental.
6. Els sous i les dedicacions dels càrrecs públics seran moderats en funció de
   la grandària del municipi i la situació econòmica.

    L’alcalde cobrarà un sou raonable i treballarà a jornada completa a
    l’ajuntament. No compaginarà la seva activitat amb altres càrrecs de
    representació política que el distreguin del seu objectiu primordial: el
    poble d’Olesa.

    Pel que fa a les dedicacions parcials, seguirem el criteri del mandat
    2011-2015, eliminant les dedicacions innecessàries. Els regidors cobraran
    per assistències efectives als òrgans col·legiats, el que permetrà un
    estalvi en les quotes de la seguretat social.
7. Seguirem respectant els drets consolidats dels treballadors municipals, així
   com la seva estructura organitzativa interna i dotarem els departaments dels
   mitjans necessaris per aconseguir millorar-ne l’eficiència. L’administració
   electrònica i la formació seran els pilars bàsics.
8. Mantindrem i millorarem els  mecanismes de control del compliment dels
   contractes dels serveis de recollida d’escombreries, neteja, zones blaves,
   transport i serveis funeraris. L’ajuntament ha de controlar els contractes
   que signa i que paga a empreses privades.
9. Posarem les condicions, fins allà on la llei ho permeti, perquè siguin
   empreses d’Olesa les encarregades de donar el serveis als olesans i les
   olesanes.
10. En matèria de contractació laboral, el Bloc Olesà haurà d’acomplir les
    prescripcions legals. Però, a més, posarà el màxim èmfasi en la difusió
    activa de totes les convocatòries per tal que tots els olesans i les
    olesanes tinguin la possibilitat d’aspirar, en condiciones d’igualtat, a
    treballar par a l’administració local.
11. Seguirem amb les inversions participatives. Us volem recordar que hem
    seguit més de 50 propostes participatives realitzades en el mandat
    anterior.
12. Ens mantindrem fidels al que vam ja aconseguir en l’anterior mandat. fer
    que el capítol pressupostari destinat a sous i salaris sigui inferior al
    capítol dedicat als serveis que l’ajuntament dóna
