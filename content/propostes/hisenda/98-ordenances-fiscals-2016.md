+++
title = "Ordenances fiscals 2016"
description = "Propsta ordenances fiscals 2016"
tags = ["hisenda"]
type = "blog"
date = "2015-11-03"
author =  "Jordi Martínez"
+++
Parlar d’ordenances fiscals equival a parlar d’impostos, de taxes i de preus
públics. Hi ha cinc impostos: l’**IBI**, que és l’impost sobre béns immobles
(la contribució);l’**IAE**, l’impost sobre les activitats econòmiques;
l’**IVTM**, l’impost de vehicles de tracció mecànica; l’**IITVNU**,l’impost
sobre l’increment del valor dels béns (impost sobre les plusvàlues); i
l’**ICIO**, l’impost sobre les construccions que demanin llicència urbanística.

A més d’aquests cinc impostos, a Olesa tenim **23 tributs** més, repartits
entre **taxes i preus públics**. La diferència fonamental entre una taxa i un
preu públic radica en si hi ha competència del sector privat. Les llars
d’infants, per exemple, es regulen amb preus públics perquè també hi ha llars
privades. Per contra, el tribut que paguen per la recollida de les escombraries
és una taxa perquè el servei el dona únicament l’ajuntament.

Per a aquests 28 tributs totals, entre impostos, taxes i preus, la proposta que
fa l’equip de govern del Bloc Olesà per al 2016 és la congelació del preu de 23
tributs, i l’augment de l’**IPC** previst (**1,3%**) en 5: l’IBI, la taxa
escombraries, la taxa per la retirada de vehicles abandonats o mal estacionats,
la taxa per l’ús de les instal·lacions municipals i la taxa de guals.

Aquest augment de l’1,3% de l’IPC aplicat a la taxa d’escombraries significa
1,38€ anuals. En el cas de l’IBI, la mitjana significa 5,2€ anuals.

Cal dir, pel que fa a la taxa d’escombraries, que està previst proposar un
canvi en la seva tarificació que permeti introduir factors de progressivitat.

La ordenança fiscal nº23 és la taxa per la retirada de vehicles abandonats o
mal estacionats, i també està previst que augmenti un 1,3. De tota manera
aquesta taxa ha comportat un debat que cal tenir en compte: el servei de
retirada de vehicles, la grua, és deficitari. La taxa no cobreix ni de bon tros
el seu cost. Per tant, al tractar-se d’un servei deficitari que grava un
comportament incívic, entenem que seria lícit plantejar un augment superior a
l’1,3.

També hi ha una proposta de modificació en l’ordenança nº4, l’ impost sobre l’
increment de valor dels terrenys de naturalesa urbana, el que coneixement com a
plusvàlues. En aquest cas és planteja una rebaixa del 10% per als increments de
valor generats en un període de temps superior als 10 anys.

En tot cas, les ordenances que es van presentar al Ple ordinari d’octubre,
mantenen el compromís del Bloc Olesà de no augmentar els impostos, en el seu
conjunt, per sobre de l’IPC.

Aquesta proposta inicial d’ordenances va se aprovada amb el vot favorable del
Bloc, les abstencions d’ERC, del PSC, de Movem i del PP, i el vot en contra de
CiU.
