+++
title = " Obres i actuacions fetes i previstes a Olesa"
tags = ["urbanisme"]
date = "2015-01-13"
author =  "celebdor"
menu = "urbanisme"
+++

A partir del compromís assumit en el nostre programa electoral hem treballat
intensament per  millorar els carrers, l’accessibilitat, la il·luminació, els
parcs... i, en general, els espais públics d’Olesa, sense oblidar de
contribuir, en la mesura del possible, i tenint en compte la situació de forta
crisi que estem vivint, els espais privats i el comerç. Per això el mateix dia
que vam arribar al govern d’Olesa ens vam posar a treballar àrduament en
aquesta tasca i aquí us enumerem les principals actuacions que hem anat fent,
any rere any, amb els mitjans de què disposàvem.

No anotem aquí, perquè seria inacabable, el gran nombre de reparacions,
recanvis de mobiliari urbà, instal·lació de nou mobiliari urbà, neteja de
pintades i altres actuacions diàries a la via pública, per part de la brigada
municipal i d’actuacions externes, per tal d’evitar nous deterioraments. Cal
dir només que s’hi ha invertit molts esforços econòmics i humans, i ens consta
que no han passat desapercebuts per a la ciutadania.

Hem intentat fer desaparèixer aquella sensació de desídia que hi havia,
motivada pel fet que, per exemple, quan unes peces de sòl es trencaven o
s’arrencaven no s’arreglaven, o quan un arbre es moria no es reposava o quan
una cosa es feia malbé es deixava allà on era.

Per acabar, volem recordar també l’esforç que s’ha fet i s’està fent per
millorar el servei de neteja i, sobretot volem agrair els ciutadans que ens han
anat notificant puntualment quan tenien algun problema o quan hi havia alguna
avaria prop de casa seva.

## Obres i actuacions fetes i previstes de fer a Olesa de Montserrat des de l’any 2011 al 2015

### Any 2011

Actuació | Cost
---------|-----
Pas – vorera des del Parc Sta. Oliva fins a la Llar d’Infants La Baldufa |  14.788,92€
Arranjament de voreres i passos de vianants entre C/Colon – La Rambla | 58.951,66€
Arranjament de voreres i passos de vianants entre C/Mallorca – Colon  | 37.912,70€
Arranjament de voreres i passos de vianants entre C/ Argelines – Vall d’Aran | 44.737€
Construcció de passos de vianants al  C/Francesc Macià – Pl. Catalunya, Vorera amb passos de vianants cruïlla Passeig Progrés – Francesc Macià i Arranjament de la Pl. de l’Oli a causa d’un esfondrament | 54.079,48€
Adequació de voreres als C/ Pep Ventura; S. Josep Obrer; Clota i Lluís Puigjaner (davant CAP), vorera a la Pl. del final  carrer Passeig del Progrés i Millora  de la vorera i els passos de vianants al C/Colon, entre Pau Casals i FFCC. | 147.189,72€
Voreres i passos de vianants al C/Barcelona; Pl. Catalunya (al costat del Mercat Municipal); Jaume Baró de Viver (entre Calvari – Francesc Macià) i asfaltat C/Jacint Verdaguer (entre C/Barcelona – Francesc Macià) amb pas elevat. | 121.405,34€
Voreres a Francesc Macià (entre Rbla. Catalunya i Ps. Progrés); Accés a la Pl. Catalunya i Mercat Municipal ( pel costat del  C/Alfons Sala) i vorera de Salvador Casas (L’ERA); vorera i pas de vianants al C/Mallorca (entre Lluís Puigjaner – Francesc Macià) | 128.960,77€

### Any 2012

Actuació | Cost
---------|-----
Modificació de la xarxa d’enllumenat i soterrament a les cruïlles dels C/Forn; Sant Joan; Baix; Dalt i Església |  47.725€
Unidireccionalitat de circulació de vehicles al Collet de Sant Joan. | 19.853,83€
Arranjament i Condicionament del Carreró de Ca l’Escoci | 37.682€
Arranjament i condicionament del Carreró Rampeu – Pl. Nova | 47.267,52€
Arranjament del C/ Montserrat i Tetuan | 39.704,75€
Segona fase d’urbanització de la Rambla de Catalunya. | 390.200€
Desenvolupament de la Unitat d’Actuació del C/ Calvari – Benet Margarit, amb protecció de la Xemeneia Industrial catalogada com a patrimoni Olesa (situada al costat de la nova escola Povill) | 600.000€
Asfaltat del C/Urgell davant del Pavelló , C/ Colon i la cruïlla C/Garraf amb la Riera de Can Llimona | 9.700€
Asfaltat a  Avda. Francesc Macià  (entre La Rambla – Ps. Progrés); C/ Priorat (entre Empordà – Argelines) C/ Rossinyol; C/ Urgell; Accés sud  a la Vila (venint de la C–55 des d’Abrera) i quatre passos elevats al C/ Montseny (Daina Isard) Anselm Clavé (Alçada C/ Sant Pere) ; Francesc Macià (a l’alçada de la Llar d’infants Taitom) i C/Vall d’Aran (a l’alçada de l’IES Daniel Blanxart) | 171.121,64€

### Any 2013

Actuació | Cost
---------|-----
Execució de la sortida d’emergències de l’Escola d’Arts i Oficis | 10.418€
Arranjament d’un esfondrament al C/ Clota – S. Josep Oriol | 8.525,65€
Aïllament per evitar sorolls i molèsties als usuaris de l’Equip de Climatització de la Biblioteca | 2.722,50€
Reubicació del Servei de Benvinguda als baixos de l’Escola d’Arts i Oficis | 3.120€
Arranjament i eliminació de les humitats  a l’Escola d’Arts i Oficis | 1.476,20€
Arranjament  de la Pl. Rv. Massana | 4.000€
Enllumenat de l’accés des de la Ctra. a Martorell fins a l’entrada del Cementiri de Can Singla | 4.791,60€
Arranjament de la vorera del C/Anselm Clavé – Pl. De l’Oli | 35.817,42€
Condicionament  - reforma dels serveis  de l’ AAVV de La Rambla  (a l’antic Escorxador) | 3.565,18€
Arranjament del clavegueram a la cruïlla del C/Francesc Macià – FFCC | 3.992,46€
Arranjament del clavegueram al C/ Escorxador | 1.948,83€
Arranjament del clavegueram al C/ Metge Carreras–FFCC | 15.484,57€
Arranjament de la façana i dels canalons pluvials i de part del teulat per evitar la degradació de l’edifici del Cal Puigjaner (situat al C/ Sta. Oliva i Creu Reial) | 48.261,96€
Asfaltat del camí a La Baldufa i condicionament de l’aparcament (per a uns 80 vehicles) | 53.403,14€

## Any 2014

Actuació | Cost
---------|-----
Arranjament i pintura de les façanes de l’Escola d’Arts i Oficis | 17.143,58€
Adequació del local dels Voluntaris, ubicat als baixos de l’Escola d’Arts i Oficis, incloent la millora dels canalons pluvials i de part del teulat | 8.919,93€
Connexió entre els pous de clavegueram per evitar inundacions als baixos del C/ Margarida Biosca/Pintor Fortuny i Sant Pere | 6.252,38€
Millora del parc infantil del Parc de l’Estatut, amb la col·locació d’un terra de cautxú | 14.793,46€
Adequació de la secretaria de l’Escola Arts i Oficis | 3.803,57€
Subministrament de jocs Infantils a l’AAVV de Ribes Blaves | 3.127,18€
Arranjament d’un habitatge municipal al Grup Sant Bernat  5 | 29.506,55€
Adequació dels passos de vianants al C/ Mallorca, a les cruïlles del C/ Lluís Puigjaner i Colon | 22.385€
Creació de 25 places d’aparcament al C/Hospital (en un solar cedit per uns particulars a través d’un conveni | 28.091,34€
Arranjament del clavegueram del C/ Barcelona, entre La Rambla  i l’Escorxador),  a causa d’un esfondrament | 65.957,71€
Reurbanització del C/ Coscoll | 211.262,91€
Unidireccionalitat de la circulació de vehicles a Les Planes | 36.794€
Adequació parcial del terra en el Parc del Poble Sec | 11.634,15€
Creació d’un nou Parc infantil a la part superior de la Plaça de l’Oli i condicionament/millora de la part inferior | 60.480,93€
Condicionament de la Font de Can Carreras | 2.400€ (a càrrec de la CMO)
Reurbanització de la Pl. Nova i de La Cendra | 325.678€
Recollida d’aigües pluvials al C/ Vall d’Aran | 16.231€
Arranjament i asfaltat del C/ Vall d’Aran, entre la Riera de Can Llimona i l’IES Daniel Blanxart. Construcció d’un pas elevat i de 30 places d’aparcament | 92.277,75€
Creació d’un nou aparcament al C/Priorat – Empordà amb capacitat per a 80 cotxes i 2 autocars | 72.438€
Remodelació del parc Infantil del Parc Municipal, tot creant una zona per a infants de fins a 5 anys i una altra per a nens de fins a 12 anys | 53.337,29€
Arranjament i condicionament del pis superior de la seu de la Policia Local | 20.000€
Refer clavegueram entre C/ Coscoll – Riera Can Llimona | 11.634,15€
Construcció del Pavelló Sant Bernat – 1ª Fase | 1.035.000€

### Properes actuacions:

* Reurbanització del c/ Calvari, de la Travessia Calvari i del Duc de La
  Victòria.  670.000€ (Obra adjudicada que començarà aquest any)
* Pavelló Sant Bernat 2ª fase i darrera  773,418,90€  (en fase d’adjudicació.
  Molt probablement començarà al gener del 2015)
* Remodelació i condicionament de l’antic Escorxador  1a fase. 180.000€  (en
  fase d’aprovació de plecs. Està previst que comenci al gener del 2015)
* Remodelació de les Rambletes Lluís Puigjaner – Els Closos. 198.317€  (?)
* Condicionament del camí principal des de la Ctra. a Terrassa (Ribes Blaves)
  fins a la Finca Oasis  162.730,27€ 
* Nou enllumenat del  C/ Anselm Clavé entre el C/Jacint Verdaguer i Alfons Sala
  201.602,07 (s’estan elaborant els plecs)
* Escorxador 2ª fase 105.000€ (subvenció de la Diputació de 45.797,50€ que
  l’ajuntament complementarà amb 60.000)
* Arranjament dels passos elevats del C/ Alfons Sala i creació de 4 de nous
  19.500€ (està previst que es facin abans d’acabar l’any)


### Convocatòries d’ajuts al comerç olesà

Any | Ajuts concedits | Import
----|-----------------|-------
2010 | 5 comerços (anterior govern) | 9.566,70€
2011 | 7 comerços | 12.228,23€
2012 | 7 comerços | 12.641,24€
2013 | 5 comerços | 8.498,94€
Total | | 42.935,11€


### Convocatòries d’ajuts a la rehabilitació d’habitatges al nucli antic

Any | Ajuts concedits | Import
----|-----------------|-------
2011| 17 habitatges   | 64.529,67€
2012|  5 habitatges   | 16.070,87€
Total|| 80.600,54€
