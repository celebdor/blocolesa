+++
title = "Per què ens amoïna el creixement urbanístic d'Olesa? "
tags = ["urbanisme"]
type = "blog"
date = "2014-04-09"
author =  "celebdor"
menu = "urbanisme"
+++

![Foto Nico Segado](/images/nico.jpeg)
Perquè fins ara s'ha seguit el model de **"creixement urbanístic a qualsevol
preu"**. Entenem que és hora que els ciutadans d'Olesa decidim sobre el futur
urbanístic del nostre poble i proposem que el futur creixement aposti per la
**qualitat de vida, la sostenibilitat i l'equilibri**.

Els grups PSC, CiU i ICV ja van donar la seva opinió quan van aprovar el **Pla
General d'Ordenació Urbana (PGOU)**, tot assumint un model de creixement
definit per l'explotació vertical del territori (amb pisos fins i tot en zones
consolidades com l'Eixample o al costat del Parc Municipal) i per l'ocupació de
noves zones no urbanitzades (Cal Candi, Els Closos...). Però, a més, alguns
grups polítics locals s'han dedicat aquests darrers anys a modificar el Pla o a
passar-hi per sobre, en el sentit d'afavorir els interessos d'alguns promotors
incrementant volumetries, edificabilitats...

També aquests darrers anys hem estat testimonis de com el nostre consistori es
convertia en un **agent especulador** més amb la creació de la societat
municipal **Olesa Urbana**, que avui està liquidada com a resultat d'una gestió
nefasta que ha generat unes pèrdues multimilionàries, la dilapidació d'una part
dels nostres diners i del nostre patrimoni comú. L'exemple més conegut és el de
**Can Carreras** on avui s'estan construint pisos de renda lliure en un solar
que els antics propietaris havien cedit pel seu ús públic, per a nosaltres.

### ENTRE TOTS HEM D’ACABAR AMB L'ESPECULACIÓ I REPENSAR QUIN MODEL DE POBLE ENS CONVÉ.

Ha arribat el moment que entre tots i d'una manera **oberta i transparent**
definim quin és el model de poble que volem. Per això cal evitar el caos
immobiliari que regna a Olesa, mitjançant una **moratòria urbanística** que
aturi els moviments especulatius.

El Bloc Olesà proposa corregir els errors del PGOU per tal de:

* reordenar l'espai urbà sense incrementar les zones urbanitzables,
* utilitzar racionalment els solars i les cases de titularitat municipal,
* habilitar zones d'aparcament, d'esbarjo i de serveis,
* protegir els espais naturals i agrícoles.

També cal contribuir al benestar dels habitants d'Olesa **promocionant la
construcció d'habitatges per a les unitats familiars amb rendes baixes
(sobretot per als joves de la Vila)**. Cal millorar l'eficiència de les
polítiques públiques en matèria de **rehabilitació**, estimulant les actuacions
dels ciutadans pel manteniment del seu patrimoni.

Cal fomentar el **respecte a l'entorn** i fer una **aplicació real de la
legislació mediambiental**, així com **crear un consell local del medi
ambient**, del qual formaran part les entitats locals i els ciutadans que hi
treballen, per potenciar les actuacions en aquest àmbit.
