+++
title = "Programa del Bloc Olesà 2011"
tags = ["Campanya2011"]
date = "2011-03-04"
author =  "celebdor"
+++

# PROMOCIÓ ECONÒMICA
1. Convertir l’ajuntament en el principal agent dinamitzador de l’economia
   local i, alhora, procurar protegir els ciutadans i les famílies que es
   trobin en risc d’exclusió social.
2. Posar en marxa polítiques efectives que afavoreixin el creixement econòmic.
   En aquest sentit, **centrarem els nostres objectius en la recuperació de
   l’activitat industrial**, marginada tots aquests anys en què s’ha primat
   exclusivament l’especulació i el totxo. Les mesures concretes a prendre són:
       * Polítiques actives d’ocupació reforçant l’administració local com a
         agent d’ocupació directe. El Bloc proposa convertir el Departament de
         Promoció Econòmica en un mediador entre ciutadans, empreses, comerços…
       * Potenciació de la formació dels ciutadans aturats per tal de
         facilitar-los la reinserció laboral. Per aconseguir-ho potenciarem la
         tasca de l’Escola Municipal d’Arts i Oficis
       * Agilitació del servei de promoció econòmica. Mantindrem activa la base
         de dades i hi anirem incorporant ciutadans i ofertes de treball en
         temps reals.
       * Volem que una part important de la tasca de l’Oficina de Promoció
         econòmica consisteixi a convertir-se en una agència de col·locació.
         Enllaçarem la pàgina web de l’ajuntament i el Servei d’Ocupació de
         Catalunya
       * Habilitarem un espai d’accés públic, on els ciutadans trobaran
         assessorament per trobar una cerca eficient de feina: són les
         anomenades Aules Actives del SOC
       * Seran objectiu prioritari d’ocupació els joves i els majors de 50 anys
       * Promocionarem la contractació d’olesans en les licitacions públiques
       * Promocionarem activament i decidida els productes agrícoles locals
       * Donarem suport a l’associacionisme dels treballadors autònoms, dels
         microempresaris i d’aquelles empreses que creïn ocupació
       * Promocionarem el sòl industrial
       * Impulsarem el servei d’informació i assistència tècnica a aquelles
         empreses que es vulguin instal·lar a la nostra vila. En aquest sentit,
         serà objectiu prioritari l’assessorament a les PIMEs i els autònoms

3. Pel que fa al comerç, aquest sector pot contribuir poderosament a l’objectiu
bàsic de generar ocupació i activitat. En aquest sentit, **el Bloc Olesà
apostarà decididament pel comerç de proximitat**
       * Farem del Mercat Municipal el veritable eix vertebrador comercial del
         centre urbà
       * Traslladarem la Fira ambulant dels divendres al centre de la vila
       * Estimularem una major presència dels productes de proximitat
       * Vetllarem pels drets dels consumidors
       * Assegurarem el compliment de les normatives existents, entre d’altres,
         en matèries d’horaris comercials
       * Potenciarem l’associacionisme entre els comerciants

4. Per promocionar l’economia, **potenciarem la marca Olesa**, que consistirà
no solament en elements tan reconeguts i prestigiats com la Passió, la
proximitat a Montserrat, l’oli, el paisatge, l’entorn, la natura… sinó també a
través de la incorporació de nous elements. Per fer-ho possible, **el Bloc
Olesà crearà des del Govern un Consell Econòmic**, amb competències ben
definides en aquesta matèria, que s’ocuparà d’establir noves estratègies i
d’analitzar el nostre creixement.

# HISENDA

En aquesta situació de crisi, i a través d’un augment dels seus impostos, els
ciutadans han hagut de compensar la disminució d’ingressos des del sector de la
construcció. Per la seva banda, el sector industrial i el comercial han hagut
de seguir fent el mateix esforç tributari. Per tal de millorar la situació
d’uns i altres, sense perjudicar les arques municipals, la nostra proposta per
als propers anys és:

1. Com a mesura prioritària, per aclarir l’estat real de les finances públiques,
  per comptar amb tots els elements que ajudin a prendre decisions i per evitar
  errades que poden condicionar el futur de la nostra vila, **realitzarem
  immediatament una auditoria de comptes**.
2. Els impostos no s’incrementaran en cap cas per sobre de l’IPC.
3. Revisarem la taxa d’escombraries (per exemple: no estem d’acord que es faci
   pagar més a les urbanitzacions pel fet d’estar fora del nucli central
   d’Olesa).
4. Eliminarem la taxa de clavegueram.
5. Racionalitzarem el sistema de subvencions per tal millorar-ne la gestió.
6. Establirem una línia de bonificacions tributàries a totes les activitats
   productives que contribueixin a la creació de llocs de treball o al
   desenvolupament econòmic d’Olesa.
7. Pel que fa a les despeses públiques, no continuarem amb la línia que ha
   seguit l’Equip de Govern tripartit, en el sentit que, malgrat estar immersos
   en un situació econòmica delicadíssima, ha incrementat la factura de personal
   en més d’un milió dos cents mil euros. Bona part d’aquesta despesa ha anat a
   parar a les butxaques de polítics i dels seus càrrecs de confiança.
8. Rebaixarem les despeses en conceptes com:
    * El nostre alcalde cobrarà un sou raonable i treballarà a jornada completa
      a l’ajuntament. No compaginarà la seva activitat amb altres càrrecs de
      representació política que el distreguin del seu objectiu primordial:
      Olesa.
    * Eliminarem les dedicacions parcials de la majoria dels regidors de
      l’Equip de Govern; per tant ens estalviarem la seva quota a la seguretat
      social i passaran a cobrar per assistència efectiva als òrgans
      col·legiats de què formin part.
    * Reduirem significativament els càrrecs de confiança, i els que mantinguem
      tindran un contracte laboral, amb mecanismes clars d’avaluació de la seva
      gestió.
    * Respectarem els drets consolidats de tots el treballadors municipals,
      però modificarem l’estructura organitzativa interna i dotarem els
      departaments dels mitjans necessaris per tal d’aconseguir millorar-ne
      l’eficiència.
9. Posarem en marxa mecanismes de control del compliment del contracte dels
   serveis de recollida d’escombraries, neteja, zones blaves, transport, serveis
   funeraris… No pot ser que l’ajuntament no controli ni faci complir els
   contractes que signa i paga a empreses privades.
10. Posarem les condicions, fins allà on la llei ho permeti, perquè siguin
    empreses olesanes les encarregades de prestar els serveis a Olesa.  En
    matèria de contractació, el Bloc Olesà haurà de complir les prescripcions
    legals que afecten a aquesta matèria. Però, a més, posarà el màxim èmfasi
    en la difusió activa de totes les convocatòries per tal que tots els
    olesans tinguin la possibilitat d’aspirar, en condicions d’igualtat, a
    treballar per a l’administració local.

# PLANEJAMENT I MANTENIMENT

1. Creiem en un creixement racional i per això ens oposem a les conclusions
   dels treballs previs del nou Pla d’urbanisme (POUM) que parla de la
   necessitat de la creació de 4000 habitatges més per a Olesa, a:
       * Zona de Les Planes cap a la KAO corporation.
       * Zona del Cementiri de Can Singla fins a les Torres del Vicentó.
       * Zona de Can Llimona anomenada Parc lineal.
       * Zona al costat del Pla Parcial SUPr.3 de Can Carreras (del Collet de
         Sant Joan fins el Porxo Santa Oliva).

       **Nosaltres estem a favor de la reducció de la densitat d’habitatges a
       l’Eixample, de l’augment de zones verdes, de la catalogació minuciosa del
       nostre patrimoni arquitectònic i de la defensa dels edificis actualment
       catalogats.**
2. No estem d’acord amb l’actual ARE, que proposa la construcció de **848**
   nous habitatges.
3. No estem d’acord amb la modificació de l’article 53 del Casc Antic,
   **aprovada per l’actual Equip de Govern**, que permet fer pisos en tot el
   barri, anul·lant les condicions restrictives.
4. Proposem deixar el mateix sòl urbà programat en el Pla General de l’any 1993
   i estudiar la seva reducció.
5. Potenciarem les zones industrials, i convertirem els costats de la carretera
   C-55 en espais aptes a tot tipus de negocis.
6. Ens oposem a una nova carretera des de la Balconada fins a Can Singla,
   quedant sota una sèrie de terrenys que molt probablement es convertirien en
   edificables.
7. Ens oposem a la construcció d’una nova autovia d’Abrera a Vacarisses, ja que
   NO té raó de ser perquè va paral·lela al Quart Cinturó (B-40).
8. **Volem una estació intermodal a Olesa** de la nova línia de ferrocarril
   Orbital que passa per Terrassa, Olesa, Esparreguera, Abrera i Martorell.
   Està planejat de construir estacions a tots els pobles veïns, menys a Olesa
   de Montserrat.
9. Proposem la recuperació de tot l’espai fluvial des de la Roca de la Mona
   fins a la Puda.
10. Proposem la interconnexió del casc urbà amb el riu Llobregat mitjançant
   corredors per on els olesans puguin gaudir dels arenys com a zones d’oci i
   de contacte amb la natura. **Augmentarem les zones de protecció necessària
   per a la flora i la fauna**.
11. Defensem l’ampliació del Parc Natural de la Muntanya de Montserrat i el
   **compliment d’una Moció del Bloc Olesa sobre l’adhesió a la Carta de
   Preservació, desenvolupament i gestió Parc Rural Montserrat.**
12. Defensem la preservació dels espais agroforestals dels municipis a l’entorn
   de Montserrat; la preservació de la biodiversitat; la protecció de la
   riquesa natural, paisatgística, arquitectònica i el manteniment del
   patrimoni agrícola.
13. Farem un Pla de senyalització d’itineraris, en col·laboració amb entitats i
   persones sensibilitzades amb el medi ambient.
14. Promocionarem l’habitatge protegit municipal en règim de lloguer,
   preferentment per a joves i gent gran.
15. Negociarem per tal que l’edifici de la caserna de la guàrdia civil sigui
   cedit a l’Ajuntament d’Olesa, i el rehabilitarem per fer-hi habitatges per a
   jovent.
16. Col·laborarem amb les comunitats de propietaris per instal·lar ascensors als
   edificis que no en tinguin, siguin protegits o lliures (Assessorament,
   tràmits, subvencions, bonificacions fiscals, etc..
17. Farem el cobriment de la Riera de Can Carreres, millorant el projecte per
   garantir-ne la qualitat i la seguretat-
18. Reactivarem els projectes per ajudar als veïns del Casc Antic a la
   rehabilitació dels seus habitatges, incloses teulades i façanes.
19. La Via Pública serà una prioritat per al BLOC OLESA, en lloc de les grans
   obres:
       * Farem un Pla Director de manteniment de voreres, carrers, paviments i
         clavegueram

       * Rehabilitarem els espais municipals
       * Actuarem per eliminar progressivament les barreres arquitectòniques
         existents.
       * Controlarem que les obres noves i les llicències d’activitat siguin
         accessibles als discapacitats.
       * Vetllarem perquè es compleixi l’ordenança d’antenes i aires
         condicionats.
       * Farem un efectiu i responsable control de les obres públiques i
         privades.
       * Farem una tasca de manteniment de les zones verdes

20. Davant dels problemes d’insuficiència de potència d’energia elèctrica,
    treballarem per aconseguir una nova central subtransformadora.
21. Asfaltarem el camí que va des del Porxo de Sta. Oliva fins a la Llar
    d’Infants La Baldufa.
22. Acabarem el Pla de Mobilitat i n’assegurarem l’aplicació per fases.
23. Treballarem per augmentar les zones d’aparcament, per la instauració de
    trens semidirectes i per la millora del servei d’autocars a BCN.
24. Desenvoluparem els plans especials per a la protecció de les zones
    Agrícola, Forestal, Riberes, Arenys, Masies i Espais d’interès natural.
25. Farem campanyes periòdiques de conscienciació ciutadana del reciclatge i
    actituds cíviques.
26. Negociarem amb la propietat del Molí per incorporar l’Areny del Molí al
    Patrimoni municipal.

# SERVEIS FORMATIUS A LES PERSONES

1. Consolidació i coordinació de l’oferta cultural existent amb ajuts materials
   i humans a les entitats culturals de la Vila. Volem compartir la tasca
   organitzativa d’activitats amb les entitats, per dinamitzar i estendre la
   cultura i el lleure.
1. Rehabilitarem i optimitzarem els espais culturals existents. En aquest
   mandat cal mirar d’aconseguir fons par completar el Casal, la UEC i la Sala
   Petita de la Passió.
1. Olesa, ciutat teatral: analitzarem la viabilitat, en col·laboració amb el
   Departament d’Ensenyament, d’oferir un mòdul de formació professional
   dedicat a activitats teatrals. Buscarem col·laboracions amb entitat per tal
   de crear un espai de formació.
1. Millorarem les condicions d’ús de la biblioteca municipal: l’accessibilitat
   i l’ampliació de l’horari (sobretot en èpoques d’exàmens).
1. Estudiarem la possible creació de biblioteques i zones wifi als barris
1. Organitzarem una Festa Major en col·laboració amb les entitats que hi
   vulguin participar, tot proposant una oferta participativa, variada i
   engrescadora.
1. Analitzarem i, si convé, redefinirem els criteris de les subvencions i ajuts
   a les entitats culturals de la vila -criteris objectius-. Tu què ofereixes
   al poble?
1. Repensarem els usos de Cal Puigjaner
1. Publicitarem, per tots els mitjans possibles, les ofertes o propostes
   d’activitats que es fan a Olesa: internet, panells electrònics, pirulís,
   mitjans de comunicació….
1. Fomentarem l’ampliació de l’oferta de mòduls i cicles formatius, sobretot
   d’aquells relacionats amb la tradició o amb el teixit productiu d’Olesa.
1. Implantarem el sistema d’intercanvi de llibres de text.
1. Potenciarem el Consell Escolar. Intensificarem les col·laboracions amb la
   comunitat educativa i amb les AMPES.
1. Incentivarem l’accés a la formació digital (TIC) per evitar que el seu
   desconeixement esdevingui un element d’exclusió social.
1. Promourem la formació a totes les edats.
1. Revisarem el mapa escolar tenint en compte els criteris de proximitat als
   domicilis (sempre d’acord amb AMPES i consell escolar municipal)
1. Obrirem les infraestructures escolars a l’ús d’activitats comunitàries en
   horaris extraescolars.
1. Fomentarem el civisme a les escoles a través de l’organització de tallers i
   la difusió de materials.
1. Planificarem, a partir dels estudis de creixement i dels permisos d’obres,
   les necessitats de nous centres d’ensenyament per eradicar, definitivament,
   la massificació i les aules prefabricades a Olesa. És prioritari, primer,
   fer el pavelló esportiu per a l’escola Sant Bernat i, segon, el cinquè CEIP
   per a Olesa.
1. Augmentarem la col·laboració entre els centres d’ensenyament, els centres
   d’activitats extraescolars, les institucions esportives i l’entitat
   municipal, per tal coordinar, millorar i facilitar les activitats
   extraescolars.
1. Revisarem els criteris per atorgar ajuts a les entitats esportives: valor
   formatiu i educatiu, priorització de l’esport base. Criteris objectius de
   subvencions.
1. Prioritzarem la inversió en la millora i el manteniment de les
   instal·lacions esportives existents i, en la mesura del possible, mirarem
   d’incrementar-ne l’oferta.
1. Potenciarem el Consell Sectorial d’Esports i la participació ciutadana, fent
   vinculants les decisions pel que fa a política esportiva.
1. Fomentarem l’esport escolar, amateur i aquell adreçat a persones amb
   necessitats especials i amb minusvalideses físiques i psíquiques.
1. Aprofitarem i rendibilitzarem les infraestructures de les escoles en horari
   no escolar.
1. Mantindrem i millorarem l’actual instal·lació del camp de futbol Olesa.

# SUPORT A LA CIUTADANIA

1. Volem detectar i abordar els conflictes abans que aquests es produeixin
   (actuacions preventives).
1. Farem que s’actuï quan es produeixi el conflicte, per evitar la sensació
   d’impunitat.
1. Construirem interlocució i promourem la participació ciutadana en el
   diagnòstic, la prevenció i el control de la seguretat.
1. Planificarem i programarem actuacions, en funció dels esdeveniments públic,
   encaminades a garantir la seguretat ciutadana en tot moment.
1. Propiciarem que s’intervingui amb celeritat, per tal de pacificar i evitar
   problemes majors, en els conflictes comunitaris i de convivència.
1. Garantirem els usos i la seguretat als espais públics,
1. Avaluarem periòdicament la seguretat ciutadana a Olesa per tal de fer una
   tasca valorativa i preventiva de la seguretat i la convivència.
1. Farem una tasca assistencial d’atenció als menors, gent gran i situacions de
   desemparament i treballarem en la mediació en els conflictes i assistència a
   víctimes de delictes i maltractaments.
1. Vetllarem pel compliment de les ordenances municipals.
1. Donarem contingut i protagonisme als Consells sectorials de participació.
1. Reformarem i racionalitzar el consell de participació ciutadana i revisarem
   el Reglament General de Participació Ciutadana.
1. Publicarem i divulgarem els acords adoptats per l’Ajuntament a la web i als
   mitjans de comunicació -total transparència-.
1. Habilitarem locals comuns i polivalents per fer possible el desenvolupament
   del moviment associatiu.
1. Crearem de la figura del defensor del Ciutadà.
1. Treballarem per posar en marxa el Banc del Temps (jo dono hores de treball
   de la meva especialitat i en rebo d’una altra quan la necessito).
1. Garantirem que els mitjans de comunicació estiguin al servei dels interessos
   dels ciutadans, que en són els legítims propietaris. Per tant, cal que els
   mitjans garanteixin la participació i la informació des de l’absoluta
   transparència.
1. Organitzarem les festes locals de manera coordinada i participativa entre
   l’ajuntament i les entitats olesanes, per elaborar una proposta
   engrescadora, participativa i variada

# SERVEIS SOCIALS

1. Encetarem negociacions amb les administracions competents per a l’ampliació
   de serveis i de personal del CAP i per la millora de l’atenció a l’usuari.
1. Treballarem perquè els olesans puguem gaudir d’un segon CAP de titularitat
   pública.
1. Ens basarem, en matèria de Salut, en la **prevenció i el foment d’una
   cultura saludable i el control del serveis sanitaris**.
1. Impulsarem el consum crític i responsable.
1. Crearem el Consell Local de la Dona.
1. Realitzarem campanyes i programes municipals que promoguin l’educació en la
   igualtat i contra la violència de gènere, en coordinació amb altres
   administracions.
1. Treballarem per garantir el respecte a la llibertat sexual, per tal d’evitar
   el rebuig a l’homosexualitat i la transsexualitat.
1. Endegarem polítiques actives que afavoreixin el coneixement de les cultures
   dels nouvinguts.
1. Apostem per la integració, respectant la identitat de les minories i dels
   immigrants.
1. Treballarem perquè qualsevol persona que visqui o treballi a Olesa conegui
   tant els seus drets com els seus deures envers la comunitat.
1. Organitzarem jornades, debats, seminaris i activitats culturals relacionades
   amb cooperació, solidaritat i pau.
1. Recolzarem preferentment els Projectes de solidaritat i cooperació on
   participin Olesans.
1. Fomentarem la participació en les associacions locals.
1. Recolzarem projectes que tinguin com a objectiu fomentar la solidaritat
   dintre d’Olesa ( transversalment amb S Socials).
1. Promourem les mesures que donin respostes integradores a les necessitats que
   planteja el col·lectiu de persones amb discapacitats físiques i/o
   psíquiques, per a la seva incorporació plena a la vida social.
1. Recolzarem totes les iniciatives privades o públiques destinades a pal·liar
   la marginació social i la pobresa en totes les franges d’edat.
1. Farem el control dels serveis i de l’atorgament d’ajuts a la població.
1. Treballarem per millorar els serveis d’atenció a domicili.
1. Recolzarem un servei de suport a les famílies amb gent gran al seu càrrec,
   per facilitar-ne l’atenció.
1. Recolzarem i fomentarem la construcció d’habitatges tutelats.
1. Potenciarem l’oci adaptat a la tercera edat.
