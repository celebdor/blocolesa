+++
title = "Programa electoral per les eleccions municipals 2019"
description = "Document amb el programa electoral per les eleccions Municipals 2019"
date = "2019-05-15"
tags = ["campanya19"]
menu = "programes"
+++

{{< pdf-embed url="/programa/programa_bloc_2019.pdf" >}}
