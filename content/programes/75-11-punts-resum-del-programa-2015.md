+++
title = "11 PUNTS RESUM DEL PROGRAMA 2015"
description = "Resum en onze punts del programa del Bloc Olesà per a les eleccions municipals del 2015"
tags = ["campanya2015"]
date = "2015-05-09"
author =  "celebdor"
menu = "programes"
+++
{{< load-photoswipe >}}
{{< gallery >}}
    {{< figure src="/images/programes/programa_resumit_candidatura_2015_p1.jpeg" alt="Primera pàgina del programa resumit candidatura 2015">}}
    {{< figure src="/images/programes/programa_resumit_candidatura_2015_p2.jpeg" alt="Segona pàgina del programa resumit candidatura 2015">}}
{{< /gallery >}}
