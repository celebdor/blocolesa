+++
title = "Programa 2015"
description = "Programa del Bloc Olesà a les Eleccions municipals 2015"
tags = ["campanya2015"]
date = "2015-05-07"
author =  "celebdor"
menu = "programa"
+++

{{< pdf-embed url="/docs/programa_bloc_2015.pdf" height="720" >}}
