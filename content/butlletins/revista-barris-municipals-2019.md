+++
title = "Revistes de Barris Municipals 2019"
type = "blog"
date = 2019-05-14T23:33:08+02:00
author =  "celebdor"
tags = ["campanya19"]
menu = "butlletins"
+++

**A continuació podeu trobar totes les revistes de barris.**

{{< pdf-embed url="/revistes/revista_casc_antic_2019.pdf" >}}
{{< pdf-embed url="/revistes/revista_collet_2019.pdf" >}}
{{< pdf-embed url="/revistes/revista_els_closos_2019.pdf" >}}
{{< pdf-embed url="/revistes/revista_la_central_2019.pdf" >}}
{{< pdf-embed url="/revistes/revista_les_planes_2019.pdf" >}}
{{< pdf-embed url="/revistes/revista_oasi_2019.pdf" >}}
{{< pdf-embed url="/revistes/revista_poble_sec_2019.pdf" >}}
{{< pdf-embed url="/revistes/revista_rambla_2019.pdf" >}}
{{< pdf-embed url="/revistes/revista_sant_bernat_2019.pdf" >}}
