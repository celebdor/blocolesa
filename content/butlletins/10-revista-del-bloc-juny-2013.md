+++
title = "Revista del bloc Juny 2013"
description = ""
tags = [  ]
type = "blog"
date = "2013-06-02"
author =  "celebdor"
banner = "/images/img_butlle/revista_juny_2013.jpg"
menu = "butlletins"
+++

{{< pdf-embed url="/revistes/revista_juny_2013.pdf" height="720" >}}
