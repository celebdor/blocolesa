+++
title = "Les humitats a un habitatge del carrer Alfons Sala"
description = "<h3>Primers acords per trobar solucions al problema de les humitats</h3>"
tags = [  ]
type = "blog"
date = "2012-03-06"
author =  "celebdor"
menu = "noticies"
+++
<h3>Primers acords per trobar solucions al problema de les humitats</h3>


<p>Al darrer Ple del mes de febrer, la família Mañà, en el torn de precs i preguntes, va fer arribar a l’Equip de Govern el seu malestar per la manca de resposta als requeriments que en el mes d'octubre havia fet a l’ajuntament, per un problema d’humitats que afecta el seu domicili des de l'any 2005, és a dir, des que es van fer les controvertides i problemàtiques obres del carrer Alfons Sala. Cal recordar&nbsp; que l'aleshores responsable de la regidoria d'urbanisme era un membre d'ERC, i que, posteriorment, durant la darrera legislatura, se'n feu càrrec el PSC, dins el govern tripartit amb CiU i IV.</p>
<p>La voluntat d’aquest escrit no es, però, fer un històric de tants anys, que la majoria dels ciutadans coneixen, sinó mirar de trobar respostes i solucions al problema. En aquest sentit, volem fer saber que:</p>
<ol>
<li>En acabar el Ple del dia 23 de febrer, es va parlar amb la família per buscar una data de visita a la vivenda afectada.</li>
<li>El dissabte 25, dos dies després del Ple, l’alcalde, el regidor de via pública i el regidor de comunicació van realitzar la visita i es concretà una reunió a l’ajuntament.</li>
<li>El dijous 1 de març, una setmana justa després del Ple, l’alcalde, al principi de la reunió, els regidors de via pública i comunicació i el gerent de l’ajuntament, es van trobar amb la família Mañà i s'arribà als següents acords:</li>
<li>Que l’ajuntament finalitzarà l’anàlisi de tota la documentació de què disposa.</li>
<li>Que es farà una relació hipotètica de les possibles causes de les humitats, i una relació de les proves necessàries per descartar aquestes hipòtesis.</li>
<li>Que es posarà en comú amb la família Mañà les conclusions d’aquestes anàlisis, per tal de conèixer les causes de les humitats i definir les actuacions a emprendre.</li>
<li>Que es va proposar als veïns, per garantir l'objectivitat de les accions i si així ho decidien, d'aportar un tècnic de la seva confiança per assessorar-los en totes les deliberacions, observacions i peritatges oportuns.</li>
<li>Que aquestes actuacions es realitzaran al llarg del mes de març.</li>
</ol>
<p>Per acabar, només volem manifestar que anirem informant de l'evolució dels informes, feines i acords així que es produeixin.</p>