+++
title = "El que el Bloc sempre t'ha explicat (2)"
description = ""
date = ""
categories = ["blog"]
tags = ["resposta"]
thumbnail = ""
+++

Fa uns dies,  Movem Olesa va repartir un pamflet on s’afirmen algunes coses
que, genèricament, no són de cap manera veritat.

L’escrit que ara podeu llegir té la voluntat d’aclarir i de seguir explicant
totes i cada una de les asseveracions que, sense fonament i dades, es fan.

## Policia

De la Policia se n’ha parlat freqüentment, tant als Plens municipals com als
mitjans de comunicació.

Afirma Movem que no s’ha fet res fins que la Policia ha fet pública la seva
queixa. Rebatre aquesta afirmació, amb dades fefaents, ens obliga a repetir
informació que ja s’ha donat. És per això que us tornem a explicar algunes de
les actuacions més importants que hem dut a terme durant els mandats del Bloc,
totes anteriors a la queixa:

* S’han incrementat dos llocs de treball i s’han cobert les places de caporal i
  sergent.
* S’han adquirit tres vehicles, dos cotxes i una moto (substitució dels antics
  que estaven ja molt deteriorats).
* S’han comprat armilles antibala per a tota la plantilla.
* S’ha instal·lat un sistema de gravació de les trucades dels ciutadans per
  millorar la qualitat del servei
* S’han instal·lat càmeres de seguretat a l’edifici de la policia
* Es van fer obres de millora en l’espai actual de la policia (asfaltat del
  pàrquing i arranjament dels espais interiors que no podien esperar a la
  construcció del nou edifici).

Pel que fa al nou edifici, conscients de la problemàtica de l’actual, les
primeres gestions per trobar-ne una solució i construir una nova caserna són
força anteriors a cap queixa policial.

A les previsions d’inversió per al 2015 ja hi figurava una partida. A més, des
del 2015 s’han realitzat visites a d’altres poblacions (Cardedeu i Granollers,
entre d’altres) per estudiar projectes de noves casernes.

## Mercat municipal

No només no hem solucionat els problemes de gestió del mercat, sinó que l’hem
agreujat. Això és el que escriu al seu pamflet Movem.

En què és basen per fer aquesta afirmació?  Valoreu vosaltres amb les dades que
us donem tot seguit:

Just abans de recuperar la concessió, i és aquest fet el que ens empeny a
prendre la decisió, les instal·lacions s’anaven degradant per la impossibilitat
dels concessionaris de complir amb el manteniment i algunes reparacions
necessàries. Hi havia deutes i l’amenaça de tancament d’alguns serveis. Això
generava situacions de tensió entre els propis paradistes.

Des de la recuperació de la concessió, en data 1 de novembre de 2015, s’ha
arreglat l’aire condicionat, els lavabos, l’ascensor, els compressors i el
paviment de les càmeres, les portes automàtiques, s’han repintat les façanes,
s’han endegat noves campanyes publicitàries i s’està treballant al costat dels
paradistes per a continuar amb les millores (un enllumenat millor i més
eficient, per exemple) i remuntar un espai tan nostre com és el Mercat d’Olesa.

## POUM

Diu Movem de que no hem mogut el pla estrella del Bloc, el POUM, el Pla d’Ordenació Urbana Municipal.

El Bloc deia al seu programa electoral del 2011:

**Creiem en un creixement racional i per això ens oposem a les conclusions dels
treballs previs del nou Pla d'urbanisme (POUM) que parla de la necessitat de la
creació de 4000 habitatges més per a Olesa, a**

* **Zona  de Les Planes** cap a la KAO corporation.

* **Zona del Cementiri de Can Singla** fins a les Torres del Vicentó.

* **Zona de Can Llimona** anomenada Parc lineal.

* **Zona al costat del  Pla Parcial SUPr.3 de Can Carreras (del  Collet de Sant
  Joan fins el Porxo Santa Oliva)**.

* **No estàvem d'acord amb la zona ARE**.

El Bloc estava i està a favor de: la reducció de la densitat d’habitatges a

l’Eixample, l’augment de zones verdes, la catalogació minuciosa del nostre
patrimoni arquitectònic i de la  seva preservació.

Aquestes propostes que dúiem al programa, volíem que fossin avalades mitjançant
un procés de participació ciutadana abans de modificar el POUM.

La fonamentació de la consulta urbanística la vam raonar al Ple de gener de
2014 amb la següent explicació:

### Fonamentació al Ple

“Des de l’any 1993 Olesa de Montserrat disposa d’un instrument de planificació
urbanística general, anomenat Pla General d’Ordenació Urbana, que va ser
aprovat seguint totes i cadascuna de les prescripcions legals, en ús de les
atribucions que la normativa reserva a les administracions locals i del
principi constitucional d’autonomia dels ens locals en l’exercici de les seves
competències.

Anys després, mitjançant el Decret Llei 1/2007, de 16 d’octubre, de mesures
urgents en matèria urbanística, es va crear la figura de les àrees residencials
estratègiques. Aquestes s’han d’entendre com aquelles actuacions
supramunicipals, promogudes per la Generalitat de Catalunya, que es
desenvolupen amb la finalitat de subvenir els dèficits d’habitatge requerit
d’ajut públic, per fer efectiu el dret de la ciutadania a un habitatge digne i
adequat.

El 15 de febrer de 2008, la Comissió d’Urbanisme de Catalunya va aprovar el
document d’objectius i propòsits generals pel desenvolupament dels Plans
Director Urbanístics de les Àrees Residencials Estratègiques de Catalunya, pel
quadrienni 2008-2011. En aquest document es proposaven 101 àmbits susceptibles
de ser àrees residencials estratègiques, entre ells el d’ Olesa de Montserrat.
En el nostre municipi, les zones proposades com a AREs són els àmbits SUPr1
“Eixample” i SUPr8 “Ca l’Isard”, inclosos com a sòl urbanitzable programat i
residencial en el Pla General d’Ordenació Urbana vigent.

Aquest Pla Director Urbanístic, pel que fa a Olesa de Montserrat, altera
substancialment les previsions de nombre d’habitatges del Pla General
d’Ordenació Urbana: es passa d’un màxim de 597 habitatges, a permetre la
construcció de fins a 899. També aquest Pla Director Urbanístic incorpora un
nou àmbit qualificat pel PGOU com un sector urbà programat de serveis.

**En segon lloc, el planejament vigent, així com l’avanç de POUM, proposa la
substitució i trasllat de l’actual camp de futbol , qualificant els terrenys
que ocupa com a sòl residencial ( clau 3b) a desenvolupar mitjançant un estudi
de detall (ED-5) per a la promoció d’habitatge de protecció pública.**

I en tercera instància, l’avanç de POUM proposa estudiar la possibilitat de
creixement urbà al sòl ara no urbanitzable de Can Llimona, al nord del nucli
antic. Per precisar, entre la riera de Can Carreras i Les Planes.

Com saben, mantenir aquest tipus de propostes comportaria un endeutament públic
i un augment important  del nombre d’habitatges de manera innecessària.

Innecessària perquè, a part de ser ara mateix una obvietat el fet de que no són
necessaris més habitatges, sinó que el que es necessari es que els pisos buits
deixin de estar buits, a banda, dic, d’aquesta consideració, els estudis previs
territorials redactats l’abril de 2013 per la Generalitat de Catalunya , per
tal de proposar una modificació del pla director urbanístic del baix Llobregat
nord, fixa una demanda d’habitatge al 2021 per al nostre municipi (2663 hab.)
que ja està coberta a dia d’avui amb els habitatges buits i vacants.

El desenvolupament de la zona ARE i el creixement urbanístic comportarien  un
canvi en la densitat de població i un canvi en l’entorn paisatgístic del
municipi. Un canvi que tindria conseqüències sobre la qualitat de vida dels
ciutadans.

És per tots aquests motius que entenem que està més que justificada la
convocatòria d’una consulta popular per la via del referèndum, al tractar-se
d’assumptes propis de la competència municipal, de caràcter local, i d’especial
rellevància per al interessos dels veïns.

Per acabar, afegir tan sols que tot i que els creixements urbanístics queden
regulats pels POUMS, i que els equips de govern poden promoure’ls i
modificar-los, entenem que qualsevol modificació dels plans de creixement futur
que vinguí emparada per la opinió directa dels veïns, te una força democràtica
de valor absolut.

Una modificació del POUM avalada per un referèndum, encara que els referèndums
no siguin vinculants, repeteixo, ELS REFERENDUMS NO SON VINCULANTS, doncs tot i
no ser-ne, tenen un valor i una força democràtica de valor absolut, i de
compliment, entenem, obligatori.

Aquesta es la força de la democràcia directa”.

Per aquestes raons vam convocar, amb les totes les dificultats i vicissituds
possibles, el primer referèndum que organitza un Ajuntament amb el permís del
Consell de Ministres del Govern d’Espanya.

Us recordem les tres preguntes que vam voler formular:

1. En el mandat consistorial de 2007-2011, la Generalitat de Catalunya va
   aprovar la creació d'una zona ARE ( Àrea Residencial Estratègica) a Olesa de
   Montserrat.
   **Vol que l'ajuntament d' Olesa de Montserrat demani a la Generalitat de
   Catalunya la supressió d'aquesta zona   ARE?**

2. Els terrenys del camp de futbol d' Olesa de Montserrat tenen la qualificació
   urbanística que permet construir-hi habitatge.
   **Vol que es preservin aquests terrenys per a ús d'equipaments?**

3. L'actual Pla General d'Ordenació Urbana (PGOU) preserva el pla de Can
   Llimona, zona delimitada per la riera de Can Carreras a la dreta, i per les
   Planes a l'esquerra, com a zona agrícola.

Atès que la Generalitat de Catalunya encara no ha aprovat definitivament
l'ampliació del Parc Natural de Montserrat, que preservaria definitivament el
pla de Can Llimona per aquest ús agrícola. 

Vol que en el futur es mantingui la qualificació agrícola d'aquesta zona nord
(pla de Can Llimona)?

Com sabeu, el Consell de Ministres finalment només ens va autoritzar dues de
les tres preguntes. La de la zona ARE va quedar fora amb l’argumentació que la
competència no era municipal. Sense entrar ara en aquella consideració, vam
poder celebrar el referèndum urbanístic el 22 de febrer de 2015, més d’un any
després de la seva aprovació pel Ple, i més de 18 mesos després de l’ inici de
l’expedient, al setembre del 2013.

Direu que això no es fer res? Direu que el govern del Bloc Olesà s’ha desentès
del POUM i del creixement urbanístic d’Olesa?

De què treuen pit la gent de Movem? Treuen pit d’haver mantingut com a
tripartit la sol·licitud de la zona ARE fins al 2011, quan la crisis de la
bombolla immobiliària ja feia tres anys que havia esclatat i a Olesa hi havia
promocions inacabades com qui diu a cada barri? Treuen pit d’haver volgut
augmentar la densitat de població a Olesa sense, com qui diu, res a canvi? O
potser treuen pit d’haver volgut construir on hi ha el camp de futbol? O potser
d’haver votar favorablement un avanç de POUM que deixava la porta oberta a
construir habitatges al pla de Can Llimona?

Recordeu que el regidor d’habitatge del tripartit era d’Iniciativa, actualment
regidor de Movem. El mateix regidor que no va fer cap consulta, però que va
votar a favor del creixement urbanístic, de la zona ARE, de fer pisos al camp
de futbol i de la possibilitat de la construcció d’habitatges a Can Llimona. Es
de tot això que se’n vanten?

Per acabar aquesta informació sobre el POUM, dir-vos que ja s’ha licitat i que
l’empresa que ha guanyat el concurs ja hi està treballant. Un POUM que, a més,
comptarà amb un pla molt acurat pel que fa a la participació ciutadana.
