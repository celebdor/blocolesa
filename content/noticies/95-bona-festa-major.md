+++
title = "Bona Festa Major"
description = "Bona festa major"
tags = ["FestaMajor2015"]
type = "blog"
date = "2015-06-18"
author =  "celebdor"
menu = "noticies"
+++
![bona festa major 2015](/images/noticies/bona_festa_major_2015.jpeg)

### Estimades olesanes i olesans,

Ens adrecem a tots vosaltres, ara que arriba la Festa Major, per desitjar-vos
que passeu uns dies farcits d&rsquo;il&middot;lusi&oacute; i diversi&oacute;.

Les festes estan fetes per celebrar-les, per a sortir al carrer, per participar
de tots els actes i per retrobar-se amb els amics i la fam&iacute;lia. La Festa
Major &eacute;s, per tant, un temps de fer poble amb alegria.

De tota manera, no tothom t&eacute; motius per celebrar coses. Olesa de
Montserrat t&eacute; encara massa ve&iuml;ns a l&rsquo;atur i hi ha encara
massa fam&iacute;lies per a les quals no &eacute;s f&agrave;cil arribar a final
de mes. A totes aquestes persones, els volem dir que el Bloc Oles&agrave;
seguir&agrave; treballant per aconseguir millorar la seva situaci&oacute; i per
dignificar el seu dia a dia. Ning&uacute; no n&rsquo;ha de quedar
excl&ograve;s.

Tamb&eacute; us volem agrair, ara que no fa ni un mes que es van celebrar les
eleccions municipals, la confian&ccedil;a que ens vau fer i la proximitat que
vau demostrar. Us hem sentit molt a prop. Amb aquests renovada for&ccedil;a,
els del nou Equip de Govern volem assegurar-vos que seguirem treballant com
fins ara: amb il&middot;lusi&oacute;, seny, austeritat, responsabilitat,
responsabilitat i efici&egrave;ncia. Estem conven&ccedil;uts que amb el vostre
suport i la vostra participaci&oacute; podrem seguir construint un poble
millor, just, solidari i pr&ograve;sper.

Us donem les gr&agrave;cies pel vostre majoritari suport que, no ho dubteu,
sabrem administrar amb prud&egrave;ncia, humilitat i mesura.

Desitgem que passeu una immillorable Festa Major. Celebreu-la, gaudiu-la,
compartiu-la, animeu-la, viviu-la, balleu-la, canteu-la... &Eacute;s la nostra
Festa Major!!
