+++
title = "Commemoració del Dia de l\"orgull Gai"
description = "Divendres 1 de juliol celebrarem el dia de l\"orgull LGTB 2011"
tags = ["OrgullLGBT"]
type = "blog"
date = "2011-06-27"
author =  "celebdor"
menu = "noticies"
+++
**Divendres 1 de juliol celebrarem el dia de l'orgull LGTB 2011**

Us convidem a veure la magnífica pel·licula Transamérica, guanyadora de múltiples i prestigiosos premis.


![Cartell 2011 orgull LGBT](/images/noticies/cartell_2011_orgull_gai.jpeg)
