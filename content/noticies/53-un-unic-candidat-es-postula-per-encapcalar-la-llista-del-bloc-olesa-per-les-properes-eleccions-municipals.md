+++
title = "UN ÚNIC CANDIDAT ES POSTULA PER ENCAPÇALAR LA LLISTA DEL BLOC OLESÀ PER LES PROPERES ELECCIONS MUNICIPALS "
description = "<p>El proc&eacute;s participatiu per escollir qui ser&agrave; el candidat del Bloc Oles&agrave; a l&rsquo;Alcaldia d&rsquo;Olesa de Montserrat acaba amb la presentaci&oacute; d&rsquo;un sol aspirant. Aix&ograve; fa que no hagi estat necessari cridar els simpatitzants de la formaci&oacute; a emetre el seu vot en una jornada electoral que estava previst que es desenvolup&eacute;s dem&agrave; en jornades de mat&iacute; i tarda.</p><p>&nbsp;</p><p>La presentaci&oacute; del nou l&iacute;der del Bloc Oles&agrave; tindr&agrave; lloc, tal com ha s&rsquo;havia anunciat, dem&agrave; dissabte, a les 19 hores, en l&rsquo;Auditori de la Casa de Cultura.</p>"
tags = [  ]
type = "blog"
date = "2015-02-06"
author =  "ppuimedon"
menu = "noticies"
+++
<p>El proc&eacute;s participatiu per escollir qui ser&agrave; el candidat del Bloc Oles&agrave; a l&rsquo;Alcaldia d&rsquo;Olesa de Montserrat acaba amb la presentaci&oacute; d&rsquo;un sol aspirant. Aix&ograve; fa que no hagi estat necessari cridar els simpatitzants de la formaci&oacute; a emetre el seu vot en una jornada electoral que estava previst que es desenvolup&eacute;s dem&agrave; en jornades de mat&iacute; i tarda.</p>
<p>&nbsp;</p>
<p>La presentaci&oacute; del nou l&iacute;der del Bloc Oles&agrave; tindr&agrave; lloc, tal com ha s&rsquo;havia anunciat, dem&agrave; dissabte, a les 19 hores, en l&rsquo;Auditori de la Casa de Cultura.</p>
