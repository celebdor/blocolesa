+++
title = "El Programa i els candidats del Bloc Olesà 2011"
description = "Programa i candidats del Bloc Olesa a les eleccions municipals 2011"
tags = ["Campanya2011"]
type = "blog"
date = "2011-05-19"
author =  "celebdor"
menu = "noticies"
+++
<h3>Aquí teniu el programa i els candidats del Bloc Olesa 2011-2015</h3>
<p>A la revista final de campanya vam incloure, a les pàgines centrals, un resum del nostre programa. Faltava el programa complet, amb totes les propostes i aportacions. Ara us l'oferim, per tal que el pugueu consultar i valorar.</p>
<iframe src="//www.slideshare.net/slideshow/embed_code/8018370" width="590" height="510" frameborder="0" marginwidth="0" marginheight="0" scrolling="no" style="border:1px solid #CCC; border-width:1px; margin-bottom:5px; max-width: 100%;" allowfullscreen> </iframe> <div style="margin-bottom:5px"> <strong> <a href="//www.slideshare.net/celebdor/programa-bloc-oles-2011" title="Programa Bloc Olesà 2011" target="_blank">Programa Bloc Olesà 2011</a> </strong> from <strong><a href="//www.slideshare.net/celebdor" target="_blank">Antoni Puimedon</a></strong> </div>

<h3>Et presentem la llista de candidats del Bloc Olesà a les eleccions del 22 de maig de 2011.</h3>
<iframe src="//www.slideshare.net/slideshow/embed_code/7788530" width="590" height="355" frameborder="0" marginwidth="0" marginheight="0" scrolling="no" style="border:1px solid #CCC; border-width:1px; margin-bottom:5px; max-width: 100%;" allowfullscreen> </iframe> <div style="margin-bottom:5px"> <strong> <a href="//www.slideshare.net/celebdor/bloc-oles-olesa-t-soluci" title="Bloc Olesà - Olesa té Solució" target="_blank">Bloc Olesà - Olesa té Solució</a> </strong> from <strong><a href="//www.slideshare.net/celebdor" target="_blank">Antoni Puimedon</a></strong> </div>
