+++
title = "Les humitats del C/ Alfons Sala núm.72"
description = "<h3>El problema que hi va haver al carrer Alfons Sala mostra, una vegada més, les diferents maneres de treballar dels partits polítics. Mentre&nbsp; CiU no va saber resoldre el problema durant els anys que va governar, El Bloc Olesà va trobar la solució en dos mesos.</h3>"
tags = [  ]
type = "blog"
date = "2013-05-30"
author =  "celebdor"
menu = "noticies"
+++
<h3>El problema que hi va haver al carrer Alfons Sala mostra, una vegada més, les diferents maneres de treballar dels partits polítics. Mentre&nbsp; CiU no va saber resoldre el problema durant els anys que va governar, El Bloc Olesà va trobar la solució en dos mesos.</h3>


<p><span>Fa poc més d’un any, la propietat de l’immoble situat al C/Alfons Sala, 72 va instar l’Equip de Govern a trobar una solució al greu problema d’humitats que afectava la seva vivenda des de l’any 2005, causat, al seu parer, per unes filtracions de la via pública.</span></p>
<p><span>En poc més de 30 dies els serveis tècnics municipals van concloure que les humitats estaven causades per un embassament d’aigua al subsòl de l’edifici, originat per una obstrucció de la canonada, en el punt de sortida general de la casa, dins l’espai que correspon a la propietat.</span></p>
<p><span>En cap cas, doncs, les humitats eres degudes a una deficient obra executada per l’Ajuntament amb anterioritat.</span></p>
<p><span>Un cop elaborat l’informe, la propietat va sol·licitar el permís d’obres i va reparar el problema.</span></p>
<p><span>Ha passat més d’un any i l’Ajuntament no té constància de noves humitats, el que permet de pensar que tot plegat va estar ben solucionat. Un problema que s’allargava durant 7 anys va ser respost i solucionat en dos mesos.</span></p>
<p><span>Si avui fem memòria del cas és per recordar que el tècnic que assessorava la propietat era un regidor de CiU. També cal recordar que CiU va formar part de l’Equip de Govern entre el 2007 i el 2011 i no va fer res per solucionar el problema de les humitats. El que sí que va fer, quan va ser a l’oposició, va ser instar la reprovació del regidor Pere Planes, del Bloc Olesà, mentre aquest treballava per resoldre el tema i el resolia en poc temps.</span></p>
<p><span>Tot plegat ensenya diferents maneres de treballar des de l’ajuntament: l’actual Equip de Govern, que va solucionar un conflicte que s’arrossegava des de feia 7 anys en 60 dies, i el grup de&nbsp; CiU, que no va fer res sobre l’assumpte durant els anys que era al govern i, en ser a l'oposició, es va dedicar a assessorar la propietat afectada i a reprovar qui ho resolia.</span></p>
<p><span>Curiosa manera de fer, aquesta de CiU, no trobeu?&nbsp;</span></p>