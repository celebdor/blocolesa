+++
title = "El PSC diu que no pactarà amb el Bloc"
description = "<h3>El número 14 pel PSC, davant l\"alcaldable Monné i altres persones de la llista, afirmava rotundament que no pactaran amb el Bloc Olesà</h3>"
tags = [  ]
type = "blog"
date = "2011-05-19"
author =  "celebdor"
menu = "noticies"
+++
<h3>El número 14 pel PSC, davant l'alcaldable Monné i altres persones de la llista, afirmava rotundament que no pactaran amb el Bloc Olesà</h3>


<p>El futur d'Olesa es va aclarint, malgrat que tant PSC com CiU vulguin tenir amagats els seus pactes, perquè de moment no els interessa que se sàpiguen. Això, d'altra banda, no és cap sorpresa perquè ja ho van fer fa quatre anys. El que ha passat és que alguns estan parlant massa aviat i van aclarint les coses als votants olesans. Ens expliquem: en un acte electoral del PSC celebrat a Sant Bernat aquest cap de setmana, el catorzè de la llista, Pere Guillamon, va afirmar, parlant del Bloc Olesà "con esos nunca vamos a pactar". Per la seva banda, Jaume Monné assenyalava al programa "Amb Solera" d'Olesa ràdio que era difícil que hi haguessin majories i que ells haurien de pactar per governar perquè volien governar.</p>
<p>Per tant, això demostra el que el Bloc Olesà fa dies que pressent i fa públic, si el Bloc no treu un nombre suficient de vots que ho impedeixin, tornarà a governar la parella de fet formada per CiU + PSC, que ja porta 6 anys cohabitant. I és que, ja se sap, hi ha amors que van més enllà de la política.</p>