+++
title = "La regidora de Mercè Jou felicita el Govern del Bloc Olesà"
description = "<h3>L’excel·lent resultat pressupostari aconseguit per l’Ajuntament d’Olesa l’any 2012 va ser motiu del reconeixement per part de la cap de llista de CiU.</h3>"
tags = [  ]
type = "blog"
date = "2013-03-27"
author =  "celebdor"
menu = "noticies"
+++
<h3>L’excel·lent resultat pressupostari aconseguit per l’Ajuntament d’Olesa l’any 2012 va ser motiu del reconeixement per part de la cap de llista de CiU.</h3>


<p>Tanmateix, la senyora Jou també va fer una relació d’aquells ingressos de caràcter extraordinari que, segons la seva opinió, expliquen el canvi de signe de les finances municipals. Lamentablement, ella va oblidar que al costat d’aquests ingressos també hi va haver unes despeses extraordinàries que van gravar les finances: l’increment de l’IVA, el pagament de factures endarrerides, etc.</p>
<p>L’any 2012 ha estat molt difícil per a les finances locals, com també per a la majoria de les economies domèstiques dels olesans. L’aprofundiment de la crisi ha afectat negativament els ciutadans i la seva administració. Per això, el Bloc Olesà també vol posar en valor l’èxit aconseguit. D’ara endavant caldrà seguir actuant amb el mateix rigor i seriositat, tot canalitzant els saldos positius, si es produeixen, per pal·liar els efectes de la crisi sobre les persones.</p>