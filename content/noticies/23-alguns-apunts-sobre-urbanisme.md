+++
title = "Alguns apunts sobre urbanisme"
description = "<h3>Després d\"alguns mesos d\"anàlisi i valoracions de l\"estat de la via pública, volem exposar la peridificació d\"alguns projectes de millora, si l\"economia ho permet.</h3>"
tags = [  ]
type = "blog"
date = "2011-11-17"
author =  "celebdor"
menu = "noticies"
+++
<h3>Després d'alguns mesos d'anàlisi i valoracions de l'estat de la via pública, volem exposar la peridificació d'alguns projectes de millora, si l'economia ho permet.</h3>


<p>L’estat de deixadesa en què hem trobat algunes parts de la via pública farà que en molts indrets d'Olesa de Montserrat no n’hi hagi prou a fer tasques de reparació i manteniment. Caldrà emprendre accions d’envergadura que obligaran a fer costoses inversions.</p>
<p>Per això l’Equip de Govern ha dissenyat un pla d’actuacions que prioritzarà aquells indrets on hi ha perill per a la seguretat dels ciutadans. Les actuacions han començat al carrer Colon, la Plaça de l’Oli i seguiran al carrer Mallorca, la Vall d’Aran, etc. (vegeu també, en aquest sentit, l'article anterior penjat a aquesta mateixa secció). Més endavant, a principis del 2012, s’han previst actuacions, per exemple, al carrer Francesc Macià (la perillosa vorera del “tot a cent”) i el paviment del carrer Jacint Verdaguer.</p>
<p>A finals del 2012, per no perjudicar els establiments ubicats en aquell indret, es compta de començar les actuacions de finalització de la remodelació de La Rambla. Abans, però, caldrà revisar les obres de la primera fase, ja que, malgrat que encara no han passat dos anys des de la seva inauguració, moltes rajoles ballen o s’han trencat i presenten un aspecte força deficient.</p>
<p>Per afrontar aquestes necessàries obres caldran diners. Uns diners que&nbsp; poden veure’s compromesos pels deutes que tenen altres administracions amb l’Ajuntament d’Olesa i per l’herència rebuda de l’antic Equip de Govern. En aquest sentit és probable que s'hagin de dedicar 700.000€ (una bona part del crèdit anual) a finançar un pla urbanístic que la llei contemplava que havien de pagar els propietaris-beneficiaris del Pla. Què ha passat, doncs, perquè ara potser s'hagin de fer càrrec d'aquesta despesa els pressupostos municipals? Doncs que l’anterior Equip de Govern va permetre que caduqués el dret preferent que l'ajuntament tenia per cobrar aquest deute, si els propietaris no pagaven. Ara potser ens tocarà pagar a tots els olesans. Si això passés, malauradament alguns dels projectes urbanístics de millora es podrien veure afectats.</p>