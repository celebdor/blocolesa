+++
title = "Creació de la Comissió Municipal de Protecció Civil"
description = "<h3>En el passat Ple celebrat el dia 25 de mar&ccedil; de 2013 es va aprovar la creaci&oacute; de la Comissi&oacute; Municipal de Protecci&oacute; Civil. Aquesta Comissi&oacute; s&rsquo;hagu&eacute;s hagut de crear l&rsquo;any 2004, per&ograve; inexplicablement aquesta feina no es va fer. Han hagut de passar 9 anys i dues legislatures per arribar a tenir-la.</h3>"
tags = [  ]
type = "blog"
date = "2013-04-13"
author =  "celebdor"
menu = "noticies"
+++
<h3>En el passat Ple celebrat el dia 25 de mar&ccedil; de 2013 es va aprovar la creaci&oacute; de la Comissi&oacute; Municipal de Protecci&oacute; Civil. Aquesta Comissi&oacute; s&rsquo;hagu&eacute;s hagut de crear l&rsquo;any 2004, per&ograve; inexplicablement aquesta feina no es va fer. Han hagut de passar 9 anys i dues legislatures per arribar a tenir-la.</h3>


<p>El BLOC OLESA&nbsp; ha tirat endavant aquesta comissi&oacute; perqu&egrave; ent&eacute;n que &eacute;s feina de l&rsquo;Equip de Govern vetllar per tenir actives totes les normatives que han de protegir la nostre Vila, i en especial els ciutadans.</p>
<p>Aquesta Comissi&oacute; ser&agrave; la responsable d&rsquo;encarregar que es redactin tots els Plans d&rsquo;emerg&egrave;ncia que per Llei ha de tenir Olesa de Montserrat, i tamb&eacute; d&rsquo;anar-los modificant a partir de les noves normatives que s&rsquo;aprovin i de fer-ne el seguiment.</p>
<p>Avui dia solament tenim vigent el Pla per riscos Qu&iacute;mics, i aix&ograve; gracies a qu&egrave; hi ha una empresa que se n&rsquo;ha preocupat. Hem de lamentar, per&ograve;, que no tenim Pla per inundacions d&rsquo;aigua en cas de pluges, de nevades, d&rsquo;incendis, etc.</p>
<p>Esperem que a partir de la creaci&oacute; d&rsquo;aquesta Comissi&oacute;, i del treball conjunt de tot els Grups Municipals, puguem avan&ccedil;ar en l&rsquo;elaboraci&oacute; d&rsquo;aquests Plans.</p>