+++
title = "Actes de campanaya 13 i 14 de Maig"
description = "La festa jove va ser un èxit"
tags = ["campanya2011"]
type = "blog"
date = "2011-05-14"
author =  "celebdor"
menu = "noticies"
+++
### La festa jove va ser un èxit

Els actes programats per al divendres 13 i dissabte 14 van tenir una sort
ben diversa:

Divendres, tant l'acte a la seu de l'Associació de veïns de La Central com la
festa jove van anar molt bé, com podeu veure a les imatges. Des d'aquí volem
agrair-vos el suport i la participació.

Dissabte, la pluja ens va tornar a obligar a cancel.lar l'acte que teníem
preparat al Poble Sec.

![Acte de la Central](/images/noticies/acte_de_la_central.jpeg)
![Acte festa jove](/images/noticies/acte_festa_jove.jpeg)
