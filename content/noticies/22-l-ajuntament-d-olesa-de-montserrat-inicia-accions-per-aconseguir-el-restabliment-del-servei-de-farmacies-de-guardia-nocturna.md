+++
title = "L’Ajuntament d’Olesa de Montserrat inicia accions per aconseguir el restabliment del servei de farmàcies de guàrdia nocturna"
description = "<h3>&nbsp;Des del passat divendres 18 de novembre Olesa ja no té farmàcies de guàrdia entre les 12 de la nit i les vuit del matí .</h3>"
tags = [  ]
type = "blog"
date = "2011-11-23"
author =  "celebdor"
menu = "noticies"
+++
<h3>&nbsp;Des del passat divendres 18 de novembre Olesa ja no té farmàcies de guàrdia entre les 12 de la nit i les vuit del matí .</h3>


<p>Les retallades impulsades pel Govern català continuen passant factura a Olesa i als pobles de la comarca. Arran de l'acord entre en govern de la Generalitat i el Col·legi de Farmacèutics de Barcelona, els municipis que no disposen d'hospital o Centres d'Assistència Primària (CAP) amb urgències nocturnes, que són la majoria al Baix Llobregat Nord, no tindran servei de farmàcia de guàrdia nocturn. Entre les 00 hores i les 8, els veïns d’Olesa s'hauran de desplaçar a les farmàcies que prestin els serveis d'urgències a Martorell.</p>
<p>Per mostrar la disconformitat amb aquestes noves retallades impulsades pel Govern&nbsp; de la Generalitat, l’Ajuntament d’Olesa&nbsp; ha enviat una reclamació/queixa escrita a la Conselleria de Salut, mostrant-los la "més contundent repulsa" per posar "encara més en precari" l'assistència sanitària obligatòria al nostre municipi.</p>
<p>També s’ha enviat una carta al President del Col·legi de Farmacèutics de Barcelona&nbsp; exposant-li la necessitat de continuïtat del servei, "instant a les farmàcies olesanes vers la seva corresponsabilitat ciutadana amb els més de 24.000 habitants afectats del&nbsp; nostre municipi". Per altra banda,&nbsp; s’han iniciat actuacions per portar el tema al Parlament de Catalunya.&nbsp; Finalment, demà es debatrà a Ple una moció sobre el tema i s'estan estudiant altres mesures, en consens amb els altres municipis afectats per aquesta nova retallada.</p>
<div id="NewsPostDetailSummary"><strong> Des del passat divendres 18 de novembre Olesa ja no té farmàcies de guàrdia entre les 12 de la nit i les vuit del matí . </strong></div>