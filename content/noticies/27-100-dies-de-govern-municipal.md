+++
title = "100 DIES DE GOVERN MUNICIPAL"
description = "<h3>100 dies de govern municipal: Balanç, anàlisi i actuacions</h3>"
tags = [  ]
type = "blog"
date = "2011-10-30"
author =  "celebdor"
menu = "noticies"
+++
<h3>100 dies de govern municipal: Balanç, anàlisi i actuacions</h3>


<h2>DIVENDRES 30 DE SETEMBRE A LES 8 DEL VESPRE,&nbsp; A L’AUDITORI DE LA CASA DE CULTURA:</h2>
<h2>&nbsp;ACTE PÚBLIC:&nbsp; 100 DIES DE GOVERN MUNICIPAL</h2>
<p>En el decurs de l’acte el Govern Municipal farà balanç dels primers 100 dies de Govern del Bloc Olesà i donarà a conèixer com ha trobat l’ajuntament, què s’ha fet des del canvi de Govern, quins han estat els problemes detectats i&nbsp; quines prioritats&nbsp; s’han marcat.</p>