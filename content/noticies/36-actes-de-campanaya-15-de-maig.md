+++
title = "Actes de campanaya 15 de Maig"
description = "L\"acte de la Plaça de l\"Oli va ser un èxit!"
tags = ["Campanya2011"]
type = "blog"
date = "2011-05-16"
author =  "celebdor"
menu = "noticies"
+++
### L'acte de la Plaça de l'Oli va ser un èxit!<

Els actes programats per al diumenge 15 van ser, al matí, un míting al barri de
les Planes, que va comptar amb un reduït, però calorós públic.

A la tarda, la plaça de l'Oli es va omplir de gom a gom per presenciar un acte
variat i amè, que va comptar amb una magnífica audició d'havaneres.

Us donem les gràcies a tots pel vostre entusiasme i suport!

![Acte plaça de l'Oli del 15 de maig](/images/noticies/acte_pl_oli_15_maig.jpeg)
![Públic a l'acte plaça de l'Oli del 15 de maig](/images/noticies/acte_pl_oli_15_maig_1.jpeg)
![Acte a les Planes del 15 de maig](/images/noticies/acte_les_planes_15_maig.jpeg)
