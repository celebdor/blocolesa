+++
title = "11 de juny. Salvador Prat alcalde d\"Olesa"
description = "Ple de nomenament del nou alcalde d\"Olesa de Montserrat"
tags = ["eleccions2011"]
type = "blog"
date = "2011-06-14"
author =  "celebdor"
menu = "noticies"
+++
### Ple de nomenament del nou alcalde d'Olesa de Montserrat

L'11 de juny Salvador Prat, candidat del Bloc Olesà, fou elegit, amb els nou
vots dels regidors d'aquest partit, nou alcalde d'Olesa. A l'acte hi assistí un
nombrós públic que ovacionà l'elecció. Des d'aquí, una altra vegada, gràcies.

Així doncs, de moment, el Bloc Olesà governara en minoria a Olesa.

![Regidors electes 2011](/images/noticies/regidors_electes_2011.jpeg)
