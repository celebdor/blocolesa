+++
title = "Jordi Cerdeira publica al Blog del Centre Muntanyenc les raons per votar SI a la preservació de Can Llimona com a sòl agrícola "
description = "<p>L\"entitat c&iacute;vica olesana Centre Muntanyenc i de Recerques ha publicat en el seu blog una magn&iacute;fica aportaci&oacute; de Jordi Cerdeira sobre algunes de les raons per votar SISI en la consulta del proper diumenge.</p><p>Trobareu aquiest article en el seg&uuml;ent enlla&ccedil;:</p><p>http://cmrolesa.blogspot.com.es/2015/02/la-biodiversitat-de-la-plana-de-can.html</p>"
tags = [  ]
type = "blog"
date = "2015-02-17"
author =  "ppuimedon"
menu = "noticies"
+++
<p>L'entitat c&iacute;vica olesana Centre Muntanyenc i de Recerques ha publicat en el seu blog una magn&iacute;fica aportaci&oacute; de Jordi Cerdeira sobre algunes de les raons per votar SISI en la consulta del proper diumenge.</p>
<p>Trobareu aquiest article en el seg&uuml;ent enlla&ccedil;:</p>
<p>http://cmrolesa.blogspot.com.es/2015/02/la-biodiversitat-de-la-plana-de-can.html</p>
