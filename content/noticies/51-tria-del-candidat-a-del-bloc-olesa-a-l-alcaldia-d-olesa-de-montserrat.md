+++
title = "Tria del candidat/a del bloc olesà a l’alcaldia d’Olesa de Montserrat"
description = "Procés de tria de la persona candidata a l'alcaldia d'Olesa de Montserrat a les eleccions municipals 2015"
tags = ["Campanya2015"]
type = "blog"
date = "2015-02-04"
author =  "ppuimedon"
menu = "noticies"
+++
<p style="line-height: normal;"><span style="text-decoration: underline;">Del 24 de gener fins al 5 de febrer (a les 24 hores): Presentaci&oacute; de candidatures</span>. Aquestes candidatures es poden presentar per correu electr&ograve;nic a la seg&uuml;ent adre&ccedil;a electr&ograve;nica: <a href="mailto:blocolesa2015@gmail.com">blocolesa2015@gmail.com</a></p>
<p style="line-height: normal;">&nbsp;</p>
<p style="line-height: normal;"><span style="text-decoration: underline;">6 de febrer: Presentaci&oacute; del programa i els objectius dels candidats</span>, en cas que hi hagi m&eacute;s d&rsquo;un candidat.</p>
<p style="line-height: normal;">&nbsp;</p>
<p style="line-height: normal;"><span style="text-decoration: underline;">7 de febrer: Presentaci&oacute; del candidat</span>. La presentaci&oacute; p&uacute;blica del candidat/a del Bloc Oles&agrave; a l&rsquo;alcaldia d&rsquo;Olesa de Montserrat es far&agrave; a les 19 hores a l&rsquo;auditori de la Casa de Cultura. Aquest acte ser&agrave; obert a tots el ve&iuml;ns d&rsquo;Olesa de Montserrat.</p>
