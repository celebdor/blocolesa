+++
title = "Olesa guanya"
description = "<p><span style=\"font-size: 12pt;\">Resultats de la consulta: </span></p><p><span style=\"font-size: 12pt;\">Participaci&oacute; 2.954 votants.</span></p><p><span style=\"font-size: 12pt;\">Pregunta 1: Protegir el camp de futbol: Si, 2841 vots; No, 82 vots. Per tant el nou Poum haur&agrave; de canviar la qualificaci&oacute; urban&iacute;stica perqu&egrave; no s\"hi facin pisos.</span><br /><span style=\"font-size: 12pt;\"> Pregunta 2: protegir la plana de Can Llimona: Si, 2838; No, 83 vots. Per tant el nou Poum no estudiar&agrave; el possible creixement urb&agrave; per aquell lloc.</span></p>"
tags = [  ]
type = "blog"
date = "2015-02-24"
author =  "ppuimedon"
menu = "noticies"
+++
<p><span style="font-size: 12pt;">Resultats de la consulta: </span></p>
<p><span style="font-size: 12pt;">Participaci&oacute; 2.954 votants.</span></p>
<p><span style="font-size: 12pt;">Pregunta 1: Protegir el camp de futbol: Si, 2841 vots; No, 82 vots. Per tant el nou Poum haur&agrave; de canviar la qualificaci&oacute; urban&iacute;stica perqu&egrave; no s'hi facin pisos.</span><br /><span style="font-size: 12pt;"> Pregunta 2: protegir la plana de Can Llimona: Si, 2838; No, 83 vots. Per tant el nou Poum no estudiar&agrave; el possible creixement urb&agrave; per aquell lloc.</span></p>
