+++
title = "El que el Bloc sempre t'ha explicat"
description = ""
date = ""
categories = ["blog"]
tags = ["resposta"]
thumbnail = ""
+++
Fa uns dies,  Movem Olesa va repartir un pamflet on s’afirmen algunes coses
que, genèricament, no són de cap manera veritat.

L’escrit que ara podeu llegir té la voluntat d’aclarir i d’explicar totes i
cada una de les asseveracions que, sense fonament i dades, es fan.

### Ordenança de civisme

Ens diuen que vam votar en contra d’una  moció que va presentar CiU que
demanava, entre d’altres coses, augmentar les sancions pels casos d’incivisme i
que l’ajuntament es fes càrrec de la neteja dels grafits als espais
particulars.

Doncs si, hi vam votar en contra perquè feia pocs mesos s’havia aprovat
l’ordenança de civisme, presentada pel Bloc Olesà, on les sancions es
rebaixaven perquè el ciutadà les pogués pagar (les de l’anterior ordenança
difícilment es cobraven, per cares) i es van incorporar treballs socials, per
tal que els infractors prenguessin consciència de les males actuacions. Ens
semblava absurd tenir un quadre sancionador inassolible i desproporcionat.

El que no sabeu, perquè Movem no ho diu al seu escrit, és que un dels dos
regidors de Movem tampoc va votar a favor d’aquesta moció.

Ens acusen de no votar favorablement una moció que un dels seus regidors tampoc
va votar. Això és el que Movem no us explica.

### Dedicacions i assistències dels regidors

S’acusa al Bloc d’haver augmentat les retribucions, ja sigui en forma de sou
(dedicacions totals o parcials) o en forma d’assistència. Segons Movem, les
retribucions per assistència equival a un sou, diuen, “en diferido”. En aquest
punt val la pena aturar-se un estona llarga.

El Bloc Olesà va reduir de maner dràstica, en arribar al govern el 2011, els
sous i les remuneracions polítiques i els càrrecs de confiança.

Si comparem l’any 2009, segon any sencer del tripartit PSC, CiU i Iniciativa
(Movem), amb el 2013, segon any de govern del Bloc, la despesa total dels
càrrecs electes, alcalde, regidors amb dedicació i regidors sense dedicació va
ser:

Any  | Quantitat
-----|------------
2009 | 264.796,80€
2013 | 133.456,65€

Vol dir que el govern del Bloc Olesà va reduir a la meitat les retribucions
polítiques per dedicacions i assistència.

**Com és possible?**

**Primera qüestió:** El sou de l’alcalde del tripartit era de 75 mil€ bruts
anuals. El del Bloc era de 45 mil.

La retribució mitjana dels alcaldes de Catalunya de poblacions entre 20 mil i
50 mil habitants és de 55.829€.

Per tant, la retribució de l’alcalde del Bloc Olesà és situa un 20% per sota de
la mitja,  mentre que l’alcalde del tripartit estava gairebé un 25% per sobre.

**Segona qüestió:** Pel que fa a les dedicacions, el tripartit tenia tres
dedicacions totals (alcalde i dos regidors) i 8 regidors amb un 25% de
dedicació (equival a 2 dedicacions totals més). En conjunt, la suma era de 5
dedicacions completes per any.

El Bloc Olesà va tenir durant el període 2011-2015 només una mitjana de 1,65
dedicacions totals, i durant el període 2015-2019 aquestes dedicacions han
pujat fins a les 3,5 (alcalde i regidor dedicació completa, dos regidors al
50%, i dos al 25%. Encara molt per sota de les 5 que tenia el tripartit.

**Tercera qüestió:** Dels 12 regidor del Bloc Olesà, sis reben retribucions per
dedicacions i la resta, els altres sis, reben remuneracions per assistència a
les Juntes de Govern i als Plens municipals.

Penseu que les remuneracions per dedicació son incompatibles amb les
remuneracions per assistència. O una cosa o l’altra. La diferència recau en la
contribució a la Seguretat Social. La dedicació cotitza, l’assistència no.

Si fem la suma de les retribucions per dedicació més les retribucions per
assistència, la despesa política del 12 regidors del Bloc Olesà l’any 2016 serà
d’uns 170 mil€ anuals; la despesa del tripartit rondava els 264 mil€ any.

Per tant, la despesa del Bloc és de 94mil€ anuals per sota de la despesa del
tripartit.

En global, l’estalvi del govern del Bloc Olesà en comparació al mandat del
tripartit va ser, durant el període 2011-20015, de gairebé 500 mil€, i en el
període 2015-2019 serà de 376mil€.

La suma de l’estalvi dels dos períodes de mandats del Bloc Olesà respecte als
anys del tripartit arribarà gairebé als 900 mil€.

Sorprèn, per tant, que Movem Olesa critiqui les remuneracions del Bloc Olesà,
quan l’estalvi és de les magnituds explicades.

Sorprèn encara més quan un dels regidors actuals de Movem Olesa formava part
del govern del tripartit que va votar favorablement el sou de l’alcalde de
75mil€, i va votar a favor de les 5 dedicacions totals.

Sorprèn també veure aquesta crítica quan revisen que fa Iniciativa (Movem) als
pobles del voltant on governa. A Sant Feliu de Llobregat, per exemple, quines
retribucions es donen?

L’alcalde, d’Iniciativa, cobra el màxim que li permet la llei, 55 mil€ anuals.
També hi ha sis regidors amb dedicació exclusiva que perceben retribucions
entorn el 49 mil€ anuals (més que l’alcaldessa d’Olesa). La resta de regidors
de govern tenen retribucions per dedicacions parcials, que gairebé doblen les
retribucions a Olesa. I nosaltres només exposem fets, no tenim cap ànim de
criticar-ho.

De què pot presumir Movem Olesa? D’haver gastat molt més que el Bloc en despesa
política, de què els seus alcaldes i regidors tenen retribucions més elevades
que no pas a Olesa, de que van votar a favor dels 75mil€ de l’alcalde
socialista del tripartit, de que tenien més dedicacions que no pas el Bloc. Per
què, a banda de criticar, no ens diuen quines són les seves propostes
retributives i comencen aplicant-les allà on governen?

### Les mocions de la PAH

També ens acusen de votar en contra d’una moció de la Plataforma d’Afectats per
l’Hipoteca (PAH).

D’aquest tema trobareu molta informació en aquesta web en els següents
articles: Estudi sobre l’habitatge buit a Olesa de Montserrat (20.11.2015);
Crònica d’una moció (encara) no presentada (27.11.2015); i a l’escrit Moció de
suport a la demanda per a exigir una nova llei reguladora del dret a
l’habitatge que cobreixi les mesures de mínims per a fer front a la emergència
habitacional (11.12.2015).

El que no diuen és que al desembre de 2015 es va aprovar pel Ple la moció del
Bloc Olesà de suport a la nova llei reguladora de l’habitatge, i es va aprovar
amb el suport de tots els grups (Bloc,ERC, PSC i CiU) i amb l’abstenció només
de Movem Olesa i del Partit Popular.

I el que tampoc diuen és que quan el Bloc Olesa va arribar al govern municipal
al juny del 2011, es va trobar damunt la taula 9 processos per iniciar el
desnonament de 9 pisos municipals, aprovat pel govern del tripartit. I el que
tampoc diuen és que el regidor d’habitatge que signa la proposta en nom del
govern del tripartit, és el regidor d’Iniciativa, que actualment és regidor de
Movem.

Per tant, de què treu pit Movem Olesa, d’haver iniciat processos de desnonament
quan estaven al front de la regidoria d’habitatge, d’haver-se abstingut en la
votació d’una moció a favor d’una nova llei reguladora del dret a l’habitatge,
aprovada per tots el grups tret d’ells i del PP?

## Caserna de la Guardia Civil

Al seu pamflet,  Movem Olesa parla de l’antiga caserna de la Guardia Civil i
retreuen que tot segueix igual.

A la darrera revista que va publicar el Bloc, ara fa uns mesos, explicàvem

que la regidoria d’Habitatge d’Olesa, l’Agència de l’Habitatge de Catalunya i
l’Incasol, les tres institucions de la mà,  estaven treballant intensament per
poder recuperar la titularitat jurídica de l’antiga caserna de la Guardia
Civil, amb l’objectiu d’arreglar els pisos i destinar-los a lloguer social.

Doncs bé, la titularitat ja és un fet, i s’està treballant actualment amb
l’Agència de l’Habitatge de la Generalitat en el procés d’adjudicació dels 16
habitatges en règim de lloguer social.

### Tot segueix igual?

Valoreu-ho vosaltres mateixos amb les dades que us aportem.

Entre el 2007 i el 2011, amb el govern tripartit, no consta als arxius cap
reunió amb l’Incasol o amb Adigsa per reclamar l’antiga caserna per a pisos en
règim de lloguer social. Només hi ha alguna carta adreçada des d’alcaldia a la
Gerencia de Infraestructuras y equipamientos de la Seguridad del Estado, on es
demana pel destí de la caserna.

A partir del 2015, amb el Govern del Bloc Olesà, consten moltes reunions amb
l’Agència de l’Habitatge per soluciona el destí de la  caserna.

El **8.05.2015**, l’alcalde del Bloc i el regidor d’habitatge es reuneixen amb
el Secretari d’Habitatge i Millora Urbana de la Generalitat, per a reclamar,
entre d’altres coses, la rehabilitació de l’antiga caserna per destinar-la a
pisos en règim de lloguer social.

El **30.06.2015** l’Ajuntament rep un escrit del Ministeri de l’Interior
oferint la venta del immoble per un valor de 1.700.000.-€.

El **14.01.2016** reunió amb l’Agència de l’Habitatge. L’ajuntament els recorda
que al cadastre figura la propietat de l’immoble a nom de l’Incasol i que, per
tant, se’ls demana que actuïn davant el ministeri reclamant-ne la propietat.

El **20.03.2016**, es torna a reclamar a l’Incasol la recuperació de l’immoble.

A l’**abril del 2016** l’Incasol ens comunica que ha fet efectiva la possessió
de la Caserna.

El **4.05.2016** el Director de Qualitat de l’Edificació i Rehabilitació de
l’Habitatge i el regidor d’Habitatge de l’Ajuntament, acompanyats dels tècnics,
visiten l’immoble.

El **22.07.2016**, nova reunió amb l’Incasol per reclamar celeritat amb les
obres d’adequació.

El **19.09.2016** se’ns comunica que les obres estaran enllestides a finals del
2016, principis del 2017.

El **22.09.2016** es comença a treballar la proposta d’adjudicació dels
habitatges.

Un tema, encallat des de feia 15 anys, que se solucionarà aviat, i Olesa tindrà
16 habitatges de titularitat pública en règim de lloguer social.

Tot segueix igual?

Encara una dada més. Sabeu qui era el regidor d’habitatge del tripartit, de qui
no consta cap reunió amb l’Incasol ni amb l’Agència de l’Habitatge? Doncs era
el regidor d’Inciativa, actualment regidor de Movem. 

## ADF

Mai t’explicaran, diuen els de Movem, que el govern del Bloc ha reduït la
partida pressupostària de l’Agrupació de Defensa Forestal  en quasi 7 mil€.

Ara també us quedareu parats. Llegiu.

Des de l’any 1999, l’Ajuntament d’Olesa, governat per tothom menys pel Bloc, no
havia destinat ni un sol euro a la partida dedicada a l’ADF.

Zero absolut. I tenim documents que ho demostren. Això va fer que el deute amb
l’ADF Olesa-Esparreguera que es va trobar el Bloc Olesa el 2011 fos de més de
20mil€.

Aquest deute el va abonar el govern del Bloc el 2013. Des de llavors, les
partides destinades a l’ADF han oscil·lat entre el 7 mil i els 12 mil€ en
funció del mínim pactat i les demandes.

En aquest sentit, i en conèixer l’escrit de Movem, l’ADF ens ha fet arribar una
carta on diu:

“Com ADF d’Olesa de Montserrat, volem que sàpiga que no tenim res a veure amb
la publicació que volta pel poble. Full informatiu “Movem Olesa” i de la que
adjuntem còpia.

Desconeixem d’ on ha sortit o d’ on han tret aquesta informació, ja que mai hem
tingut cap entrevista ni contacte amb aquest grup, **i el que publiquen es
totalment fals**”.

Sembla que queda clar qui diu la veritat i qui no.

Per arrodonir-ho.

Sabeu qui era el regidor de Medi Ambient en l’etapa del tripartit, qui era el
regidor que no va pagar ni un euro a l’ADF entre el 2007 i el 2011? Doncs era
el regidor d’Iniciativa, actualment regidor de Movem Olesa.

## Transparència

Aquesta informació també és pe sucar-hi pa.

L’estiu de 2015 l’empresa Xut Consulting, amb una bona estratègia comercial per
vendre els seus serveis, donat que al gener de 2016 entrava en vigor al Llei
19/114 de 29 de setembre de Transparència i Accés a la Informació pública i bon
govern, va publicar que el nostre Ajuntament tenia una puntuació de 1,5 sobre
10.

L’estudi i els criteris de puntuació els va crear la pròpia empresa, que el que
volia era dir-li a l’ajuntament  que tenia una puntuació baixa, i que els
contractessis per millorar la baixa puntuació que ells, l’empresa, havia
atorgat.

D’aquest estudi Movem Olesa en fa bandera, però s’oblida d’algunes coses.

Per exemple, que el web muncipal ha aconseguit en les dues darreres edicions la
màxima puntuació del Segell Infoparticipa a la Qualitat i la Transparència que
atorga el Laboratori de Periodisme i Comunicació de la Universitat Autònoma de
Barcelona. Es més, en la darrera edició aquest segell ha afegit 10 nous
indicadors (en total en són 52) per adaptar el Segell a la Llei de
Transparència.

Doncs bé, la nostra transparència valorada per un organisme objectiu i que no
vol fer negoci del seu treball, ens ha donat el 100% de puntuació. La màxima.
Només hi ha 20 ajuntaments del 947 que obtenen aquesta màxima valoració.

Cal recordar aquí que quan el Segell Infoparticipa fa la primera valoració, al
2012, la puntuació del web que ens va deixar el tripartit era del 39%.

Amb el nou web creat per l’equip de govern del Bloc es va arribar l’abril del
2013 al 59%, al febrer del 2014 al 93% i des de llavors ja no hem baixat del
100% de transparència.

Com a referència dels pobles veïns, dir que Martorell obté en aquesta darrera
valoració una puntuació del 34,62%, amb 18 indicadors sobre 52, i Esparreguera
el 67,31%, amb 35 de 52.

Pel que fa al compliment de la Llei de Transparència, l’Ajuntament està
treballant de manera molt activa per complir totes les seves exigències. La
puntuació la donarà a conèixer mes endavant el Síndic de Greuges.

## ADHESIÓ A L’OBSERVATORI DONES EN MITJANS DE COMUNICACIÓ

Diuen des de Movem que el Bloc Olesà no t’explicarà que ha cancel·lat la quota
d’adhesió a l’Observatori de les Dones en Mitjans de Comunicació, en contra de
tots el grups de l’oposició, però no expliquen el perquè d’aquesta adhesió.

La proposta neix d’un petició que fa l’ Institut Creu de Saba, que volia
presentar-se al concurs (Re) Imagina’t, per fer un guió audiovisual no sexista,
adreçat a joves de 14 a 18 anys. L’escola volia fer-ho perquè estava treballant
la igualtat a les aules.

La condició per poder inscriure’s al concurs era que l’Ajuntament fos membre de
dret de l’Observatori. Per aquest motiu l’Equip de govern del Bloc va valorar
positivament la proposta d’adhesió i la va portar al Ple d’abril de 2015.

La valoració posterior d’aquesta activitat, tant per part de l’ Institut  com
per part de l’ajuntament va ser negativa per, entre d’altres motius, l’
incompliment d’alguns aspectes dels acords signats. Per això es va dur al Ple
de novembre de 2015 donar-nos de baixa del Obervatori. No cal pagar mil euros,
ni cinc, per una adhesió que no aporta retorn efectiu.

Menteix descaradament Movem Olesa quan diu que tots el grups hi estaven en
contra. La votació al Ple de novembre de 2015 va ser: 12 vots a favor de la
proposta dels regidors del Bloc Olesa, sis abstencions dels regidors d’ERC,
PSC, CiU i PP; i dos vots en conta dels regidors de Movem Olesa.

Estar en contra, es votar en contra i l’únic que ho va fer va ser qui menteix
amb aquesta afirmació: Movem Olesa

## LES OBRES DE MILLORA DEL PAVELLÓ SALVADOR BOADA I DEL TIR AMB ARC

Ens diu de manera sorprenent que les obres de millora de la zona esportiva del
Pavelló Salvador Boada s’han endarrerit. Doncs bé, ni aquesta l’endevinen.

Una de les millores principals era el tancament parcial de la pista exterior
del pavelló, la destinada al patinatge i a l’hoquei, ja que quan plovia fort la
pista no es podia utilitzar a causa de l’aigua.

Un cop es va aprovar el projecte, es va pactar amb el clubs que en fan us, el
Club Olesa Patí i el Club Patinatge Artístic, de fer les obres a l’estiu per
mirar de no impedir els entrenaments i els partits durant el curs. Amb el
compromís, tal com s’està fent, que si les obres no estaven acabades a l’agost,
durant el setembre l’empresa treballaria fins a les 16h, per no impedir els
entrenaments dels clubs.

Pel que fa a la construcció d’una nau que permeti els entrenaments i els
campionats del Tir amb arc, evitant el fred i la pluja, aquest projecte sí que
s’ha endarrerit nou mesos sobre el temps previst d’execució. En part, unes
verificacions de la consistència del sòl n’han estat la causa. Ens hauria
agradat que fos una realitat al gener del 2016, i és una realitat a dia d’avui.

El fet és que les dues millores llargament reivindicades pels clubs, estan a
punt de ser una realitat.

Potser treu pit Movem Olesa d’haver estat incapaços de fer-ho ells quan
manaven, malgrat l’enorme quantitat de milions que els van arribar amb els
plans Zapatero? O potser treuen pit de no haver fet inversions a la zona del
Pavelló Salvador Boada, tot esperant la meravellosa anella olímpica del 13
milions d’euros que asseguraven fer? Treuen pit de tot això que no van fer?
