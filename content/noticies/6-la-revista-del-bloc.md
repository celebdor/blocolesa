+++
title = "La revista del Bloc"
description = "<p><strong>Ja podeu llegir des d\"aquí la revista del Bloc de Juny </strong></p><p>Si voleu conèixer de primera mà les nostres notícies, les podeu consultar al menú superior de l\"esquerra, on diu butlletins informatius. Com que hem canviat una mica la presentació, us volem indicar que per veure la revista a tamany normal heu de clicar, dins la pestanya inferior, el quadrat que conté quatre fletxes.</p>"
tags = [  ]
type = "blog"
date = "2014-04-09"
author =  "celebdor"
menu = "noticies"
+++
<p><strong>Ja podeu llegir des d'aquí la revista del Bloc de Juny </strong></p>
<p>Si voleu conèixer de primera mà les nostres notícies, les podeu consultar al menú superior de l'esquerra, on diu butlletins informatius. Com que hem canviat una mica la presentació, us volem indicar que per veure la revista a tamany normal heu de clicar, dins la pestanya inferior, el quadrat que conté quatre fletxes.</p>
