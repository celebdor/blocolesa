+++
title = "CiU d’Olesa de Montserrat en contra de la llei del Mínim Vital Garantit"
description = "<h3>Una vegada més, CiU d\"Olesa dóna l\"esquena als més desafavorits i se situa més a la dreta que PP i PxC</h3>"
tags = [  ]
type = "blog"
date = "2013-06-15"
author =  "celebdor"
menu = "noticies"
+++
<h3>Una vegada més, CiU d'Olesa dóna l'esquena als més desafavorits i se situa més a la dreta que PP i PxC</h3>


<p><span>Al Ple del passat 23 de maig el Bloc Olesà, conjuntament amb el PSC, l’Entesa del Olesans i ERC, vam presentar una moció per sol·licitar l’aprovació d'una llei que pretén assegurar un mínim vital de les persones. L'objectiu és, doncs, aconseguir un nou dret social: el dret a una existència digna a través d’una prestació econòmica per a les persones que acreditin ingressos inferiors a l’IRSC (Indicador de Renda de Suficiència de Catalunya), que actualment és de 569€ mensuals.</span></p>
<p><span>Aquesta llei, a més de la renda garantida, també contempla l’accés als aliments saludables necessaris per a la subsistència, l’aprofitament de productes consumibles però no comercialitzables, la disponibilitat del subministrament d’uns mínims bàsics d’aigua i energia per cuinar i escalfar-se i reclama el dret a un allotjament digne, en cas de pèrdua de l’habitatge habitual per manca de recursos o a causa de desnonaments.</span></p>
<p><span>Doncs bé, <strong>aquesta moció que reclama una llei tan humana com aquesta va ser rebutjada pel grup municipal de CiU. S'hi van abstenir el PP i PxC. </strong>Convergència i Unió, doncs, mostrant una actitud més dretana que aquests partits, hi va votar en contra. Algú podria excusar-los pensant que ho van fer per disciplina de partit, però no seria veritat, ja que a la mateixa moció presentada a l’ajuntament de Rubí CiU hi ha votar a favor, i a Gavà, es van abstenir.</span></p>
<p><span>Tot plegat ens demostra quina es la Catalunya que vol el grup de CiU d’Olesa de Montserrat, i quins són els catalans als qui volen protegir: aquells que més tenen, aquells que més guanyen, aquells que no necessiten de lleis tan dignes com la del Mínim Vital Garantit. CiU d’Olesa evidencia, amb votacions com aquesta, que el mínim vital per dignificar la vida de les persones que més ho necessiten no forma part ni del seu programa ni de les seves prioritats.</span></p>