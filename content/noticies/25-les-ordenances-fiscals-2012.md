+++
title = "Les ordenances fiscals 2012"
description = "<h3>Crònica sobre la proposta d\"ordenances fiscals 2012 a Olesa de Montserrat</h3>"
tags = [  ]
type = "blog"
date = "2011-11-04"
author =  "celebdor"
menu = "noticies"
+++
<h3>Crònica sobre la proposta d'ordenances fiscals 2012 a Olesa de Montserrat</h3>


<p>Les ordenances municipals agrupen els impostos, les taxes i els preus dels serveis municipals, i forma part de les atribucions del Govern municipal presentar als grups polítics, regidors de l’oposició i veïns, una proposta anyal d’ordenances, per tal que, durant els dies d’exposició pública, puguin, si així ho creuen convenient, presentar les al·legacions que considerin oportunes.</p>
<p>El dilluns 17 d’octubre l’Equip de Govern va presentar la seva proposta d’ordenances a la Comissió d’Hisenda municipal i, un cop explicada i debatuda, aquesta Comissió d’Hisenda, amb els vots contraris de CiU, PSC, PP, Plataforma i Esquerra, l’abstenció d’Entesa del Olesans, i el vot favorable del Bloc Olesà, va rebutjar la proposta.</p>
<p>La majoria de l'oposició va votar en contra, al·legant, essencialment, dos motius:</p>
<ol>
<li>&nbsp;A nivell formal, que el procés seguit no era el correcte, ja que haurien volgut debatre les ordenances previ a la presentació a la Comissió d’Hisenda.</li>
<li>Pel que fa al contingut, tret d’una esmena oral, que no escrita, que va presentar el grup socialista, pel que fa l'ICIO, que és el impost sobre construccions, instal·lacions i obres, i una esmena general, també oral, a l’augment de l’IBI per sobre de l’IPC, no hi va haver gaire més treball. Amb aquest vot desfavorable, l'oposició volia fer palesa la seva disconformitat amb el procés i la nostra minoria davant el ple. Com si això no ho sabéssim!!!</li>
</ol>
<p>Ja amb anterioritat, però amb més constància a partir d’aquest moment, l’Equip de Govern del Bloc ha buscat el diàleg amb tots els grups de l’oposició i, sobretot, ha buscat el consens amb aquells grups ideològicament més propers.</p>
<p>&nbsp;Tot ha estat endebades.</p>
<p>A la primera Junta de Portaveus posterior a la Comissió, els grups de l'oposició van tornar a deixar clara la seva postura contrària a les ordenances, tot i que insistien que calia presentar-les al Ple, ja que la Comissió d’Hisenda no te capacitat d’impedir-ho, al no ser vinculants les seves decisions.</p>
<p>Vull deixar clara que la voluntat de l’Equip de Govern era, com s'ha demostrat, portar les ordenances al Ple per a la seva aprovació preliminar, la qual n'hagués possibilitat el debat posterior, per arribar a desembre amb una proposta ja definitiva. Aquesta era, doncs, la nostra voluntat, que les ordenances passessin el primer tràmit, per poder-les consensuar en la segona fase, la més important.</p>
<p>En la segona Junta de Portaveus, realitzada el dimecres 26, i convocada d’urgència 24 hores abans del Ple, vam buscar altre cop el consens amb els grups, tot explicant-los que havíem incorporat la seva proposta de modificació de l'ICIO i la proposta d’alguns veïns que l'IBI no s’augmentaria per sobre de l'IPC. Creiem que aquestes dues mesures visualitzaven de manera ferma el nostre compromís per arribar al consens i al pacte. Afegíem, a més, que seguia intacta la nostra invitació i voluntat, manifestada una i mil vegades, de seguir treballant totes i cada una de les ordenances un cop s’hagués aprovat la fase preliminar.</p>
<p>Tot va tornar a ser endebades.</p>
<p>L'oposició va repetir el seu mantra: aquesta no és la manera i votarem en contra per evidenciar la vostra minoria, però volem que les ordenances vagin al Ple. <strong>Es va arribar a dir, i cal parar-hi atenció, que el menys important eren les ordenances, que l'única cosa que els interessava era mostrar la nostra soledat</strong>. Com si no la coneguéssim!!</p>
<p>Un cop arribats a aquest punt, és facultat de l’alcalde decidir portar al Ple les ordenances amb caràcter d’urgència, decidir no dur-les, o convocar un Ple Extraordinari per tractar únicament aquest punt. I aquesta darrera va ser la decisió que es va prendre. I es va prendre perquè estàvem convençuts, i es demostrable amb dades objectives, que presentaven la millor proposta d'ordenances dels darrers 20 anys.</p>
<p>Encara, però, una penúltima cosa. Les ordenances que hem presentat busquen, per primer cop al municipi, un tractament impositiu progressiu. Vol dir que aquells que menys capacitat econòmica tenen, haguessin pagat menys impostos, i aquells econòmicament més afavorits, haguessin pagat una mica més. En tots els casos, però, <strong>sumats tots els impostos, la càrrega impositiva per al 2012 resultava inferior a l'IPC. També s'eliminava la taxa de clavegueram</strong>. El Bloc Olesà doncs, complia els seus compromisos electorals. A diferència de la nostra proposta, l'anterior Equip de Govern va augmentar les taxes un 20% en els 4 anys anteriors que va governar, però&nbsp; l'IPC només va augmentar un 10%.</p>
<p>I ara l’última. <strong>Varem fer-vos tres promeses electorals: reduir el sou de l’alcalde, eliminar la taxa de clavegueram i confeccionar un sistema impositiu progressiu. Han passat només 4 mesos des que vam arribar a l’Ajuntament i n'hen complert tres de tres.</strong></p>
<p>Us seguirem informant....</p>
<p><em>Jordi Martínez Vallmitjana</em><br /><em>Regidor de Comunicació</em></p>