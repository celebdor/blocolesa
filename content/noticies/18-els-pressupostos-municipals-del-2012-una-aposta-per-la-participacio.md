+++
title = "Els pressupostos municipals del 2012: una aposta per la participació"
description = "Als pressupostos 2012 hi ha unes inversions \"obertes\", perquè els ciutadans diguin i decideixin en què volen que es gasti gairebé un milió d\"euros"
tags = ["Pressupostos2012"]
type = "blog"
date = "2012-07-27"
author =  "celebdor"
menu = "noticies"
+++
<h3>Als pressupostos 2012 hi ha unes inversions "obertes", és a dir, sense un destí concret,&nbsp; perquè els ciutadans diguin i decideixin en què volen que es gasti gairebé un milió d'euros</h3>


<p>En la situació actual de crisi, elaborar uns pressupostos és una tasca més que difícil, és una aventura, i no només pels problemes econòmics que ens estan tocant de viure, sinó també pel fet que ens superiors, com ara la Generalitat de Catalunya, no paguen als municipis quan caldria aquelles competències delegades (llars d'infants,&nbsp; escola de musica alguns assumptes socials...),&nbsp; o perquè els canvis legislatius que els governs van fent un dia sí i l'altre també capgiren les nostres previsions i ens fan refer cada dos per tres tot allò fet. L'exemple més recent, que no el més important, és l'augment de l'IVA, que ens ha obligat a fer modificacions pressupostàries per enèsima vegada.</p>
<p>Un dels aspectes més novedosos d'aquests pressupostos és la seva voluntat participativa, és a dir, el desig de l'Equip de govern que els ciutadans hi puguin dir la seva. En aquest sentit, quan se'n va fer la presentació publica, de la mà del regidor d'hisenda, Joan Segado, ja es van recollir algunes propostes d'entitats i ciutadans, que s'estudiaran amb molta atenció, per tal de mirar d'incorporar-les.</p>
<p>Però la voluntat de fer uns pressupostos participatius va més enllà, ja que s'obre a tots els ciutadans. Com es pot veure al quadre, hi ha unes inversions “obertes”, és a dir, estan previstos els imports, però no s'especifica on es gastaran els diners,&nbsp; perquè el que es vol és que aquests diners es gastin allà on els ciutadans considerin prioritari.</p>
<table border="1">
<tbody>
<tr style="border-color: #000000; border-width: 2px;">
<td>Millora de vials de l'eixample</td>
<td>504.000€</td>
</tr>
<tr style="border-color: #000000; border-width: 2px;">
<td>Millora de vials de l'eixample</td>
<td>&nbsp;264000</td>
</tr>
<tr style="border-color: #000000; border-width: 2px;">
<td>Millora instal·lacions esportives</td>
<td>&nbsp;160000</td>
</tr>
<tr style="border-color: #000000; border-width: 2px;">
<td>Intervencions en parcs infantils</td>
<td>58000</td>
</tr>
<tr style="border-color: #000000; border-width: 2px;">
<td>TOTAL</td>
<td>986.000</td>
</tr>
</tbody>
</table>
<p>Així doncs, seran els ciutadans, a través de les seves propostes, qui hauran de decidir on s'inverteixen els diners d'aquestes partides.</p>
<p>Els ciutadans i entitats interessats a participar-hi poden trobar una fitxa a omplir a la pàgina web municipal (www.olesam.cat) dins l'apartat de “Notícies d'interès” o personant-se a l'OAC i demanant aquesta fitxa en paper.&nbsp; La fitxa es pot entregar a l'OAC, o bé es pot enviar a l'Ajuntament per correu electrònic a l'adreça <a href="mailto:inversionsolesa2012.2015@gmail.com">inversionsolesa2012.2015@gmail.com</a>. El termini de presentació és fins al 22 de setembre. Passat el 22 de setembre, totes les&nbsp; propostes seran estudiades i valorades pels tècnics municipals per a la seva incorporació. Els criteris seran exposats i justificats públicament.</p>
<p>Esperem la vostra aportació, participeu-hi!</p>
