+++
title = "PROBLEMES A LA LLAR D\"INFANTS LA BALDUFA"
description = "<h3>Una altra vegada, una obra feta en l\"anterior mandat pel tripartit olesà mostra deficiències preocupants i problemes.</h3>"
tags = [  ]
type = "blog"
date = "2012-03-05"
author =  "celebdor"
menu = "noticies"
+++
<h3>Una altra vegada, una obra feta en l'anterior mandat pel tripartit olesà mostra deficiències preocupants i problemes.</h3>


<p>Com la majoria d'olesans i olesanes saben, ara fa tot just dos anys que es va inaugurar la llar d'infants “La baldufa”. L'obra va ser una carrera feta de desencerts, des de la concessió a l'empresa que havia de fer l'obra fins la manera d'acabar-la, tirant pel dret i sense garanties constructives. També va ser penosa la data d'inauguració o obertura, que es va anar ajornant més d'un any. Això sí, l'Equip de govern tripartit sempre jurava i perjurava que aquella vegada sí, que aquella data era la bona i que s'estava fent una bona feina.</p>
<p>Nosaltres, els del Bloc Olesà, com es pot veure encara a aquesta pàgina web, anàvem expressant els nostres dubtes i el nostre desconcert per com anaven les coses i ells responien que no teníem raó i que les nostres crítiques eren falses. Doncs bé, poc després d'arribar al Govern municipal, la directora de la&nbsp; Llar d'infants ens feia arribar un llistat de deficiències constructives a l'edifici preocupants. La nostra resposta va ser demanar un informe als serveis tècnics de l'ajuntament, per tal que, objectivament, valoressin la seva importància. La inspecció es va fer el 9 de novembre de 2011. Reproduïm textualment tot seguit alguns fragments de l'informe:</p>
<ol>
<li>Es comprova que hi ha goteres al sostre de l'aula 5</li>
<li>Al despatx annex al de direcció, hi ha dins un armari un aparell de megafonia que no es troba connectat a la xarxa d'altaveus instal·lats per a l'edifici</li>
<li>S'aprecia que el terra de la part sud de l'edifici s'ha assentat, concretament a la zona de l'aula 8, provocant un desnivell entre dues zones del paviment. Analitzat el projecte executiu que consta a l'expedient, es comprova que es tracta d'un paviment de PVC col·locat sobre una solera de formigó assentada sobre un terraplè de terres compactades. Pel que es dedueix, la compactació de terres no deu ser suficient, fet que ha provocat moviments al paviment. Aquests moviments han provocat a la vegada que els envans també s'hagin mogut i esquerdat i que les portes no tanquin de forma correcta.</li>
<li>Es comenta que a la vegada hi ha males olors a les aules.</li>
<li>Les esmentades deficiències constructives són defectes de construcció i per tant són imputables al contractista.</li>
</ol>
<p><br />Després de llegir tot això, hom podria pensar que la solució estaria, per una banda, a utilitzar els diners que les empreses constructores dipositen a l'ajuntament com a garantia per subsanar part del problema. Doncs bé, això no es pot fer perquè l'anterior Equip de Govern tripartit es va gastar els diners de la garantia per acabar l'obra.</p>
<p>Per altra banda, les empreses han de donar 10 anys de garantia de construcció i per tant se'ls hauria de reclamar que arreglessin tots aquests problemes, i altres de menors, que ni esmentem.<br />Tampoc no es pot reclamar a l'empresa constructora perquè es tracta d'una empresa “desapareguda”. De fet, quan van licitar l'obra ja se sabia que era una empresa amb problemes, i malgrat tot van tirar pel dret. Quin interès hi havia a contractar aquesta empresa?</p>
<p>Per acabar, tres coses: 1. S'està mirat de contactar amb les empreses que van acabar les obres per solucionar alguns problemes, tot i que amb algunes està resultant molt difícil d'entrar-hi en contacte. 2. Una mostra de la desídia i el desinterès per l'obra es veu en el punt 2 de l'informe, ja que, després de gastar milers d'euros en una instal·lació de megafonia a tota la llar d'infants, ni s'havien molestat a fer-ne la connexió (els cables es van trobar enrotllats dins d'una armari. Hi ha fotografies que ho demostren). 3. Hores d'ara el problema del desnivell a la part sud de l'edifici s'ha ageujat i afecta ja a 3 aules.</p>
<p>&nbsp;Qui és el responsable que un centre amb només dos anys d'ús presenti aquests problemes que poden anar a més? Qui pagarà el que val solucinar, sobretot, el problema estructural?</p>