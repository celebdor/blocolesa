+++
title = "Les eleccions 2011"
description = "Ens heu donat donat nou regidors. Gràcies"
tags = ["Eleccions2011"]
type = "blog"
date = "2011-05-23"
author =  "celebdor"
menu = "noticies"
+++
### Ens heu donat donat nou regidors. Gràcies

Fa quatre anys ens van definir com els guanyadors morals de les eleccions. Ara
ens heu fet els guanyadors absoluts. El triomf del Bloc Olesà només ha estat
possible amb el vostre suport i confiança. A l'oposició vam voler ser la vostra
veu. Ara més que mai estem obligats a treballar i actuar amb la vostra
aprovació i tenint-vos al costat per millorar Olesa. Només així, Olesa té
solució!

Gràcies per la vostra confiança>

![Regidors electes](/images/noticies/triomf_2011_1.jpeg)
![Candidats del Bloc](/images/noticies/triomf_2011_2.jpeg)
