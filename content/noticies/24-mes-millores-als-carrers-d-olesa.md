+++
title = "Més millores als carrers d\"Olesa"
description = "<h3>El projecte d\"inversió, que puja gairebé 400.000€, representarà una important millora per a molts carrers d\"Olesa</h3>"
tags = [  ]
type = "blog"
date = "2011-11-07"
author =  "celebdor"
menu = "noticies"
+++
<h3>El projecte d'inversió, que puja gairebé 400.000€, representarà una important millora per a molts carrers d'Olesa</h3>


<p>La Junta de Govern local de 23 d'octubre va aprovar una despesa de 397.555,83€ per al projecte d'urbanització d'alguns vials de l'Eixample. El 50% d'aquest import prové d'una subvenció i l'altre 50% serà a càrrec del municipi. Això representarà una altra millora important de zones de la via pública d'Olesa que estan en mal estat.</p>
<p>L'Equip de Govern va haver de fer una aprovació d'urgència, ja que el 50% d'aquest import depenia&nbsp; d'una subvenció que està dins el Pla Únic d'Obres i Serveis de l'anualitat&nbsp; 2009, i la seva adjudicació s'havia de realitzar al més aviat possible a fi de no perdre-la.</p>
<p>Els carrers on es projecta actuar, per tal de millorar-los són:</p>
<p>-&nbsp;&nbsp;&nbsp; Passeig del Progrés davant del Mercar Municipal<br />-&nbsp;&nbsp;&nbsp; Baró Jaume de Viver, entre Calvari i Av. Francesc Macià<br />-&nbsp;&nbsp;&nbsp; Carrers Escorxador i Barcelona<br />-&nbsp;&nbsp;&nbsp; Jacint Verdaguer entre Barcelona i Av. Francesc Macià<br />-&nbsp;&nbsp;&nbsp; Av Francesc Macià entre P. Progrés i Pl. Catalunya<br />-&nbsp;&nbsp;&nbsp; Pep Ventura<br />-&nbsp;&nbsp;&nbsp;&nbsp; Sant Josep obrer<br />-&nbsp;&nbsp;&nbsp; Clota<br />-&nbsp;&nbsp;&nbsp; Lluís Puigjaner davant del CAP<br />-&nbsp;&nbsp;&nbsp; Colom entre Pau Casals i Ferrocarrils Catalans<br />-&nbsp;&nbsp;&nbsp; Rbla Catalunya entre Rep. Argentina i Mallorca<br />-&nbsp;&nbsp;&nbsp; Mallorca entre Ll. Puigjaner i av Francesc Macià<br />-&nbsp;&nbsp;&nbsp; Alfons Sala davant del Mercat<br />-&nbsp;&nbsp;&nbsp; Salvador Casas</p>
<p>Si tot va com està projectat, les obres començaran a inicis del 2012</p>