+++
title = "24 de gener, Jornada de preparació Municipals 2015"
description = "Jornada de preparació municipals 2015"
tags = ["campanya2015"]
type = "blog"
date = "2015-01-21"
author =  "celebdor"
menu = "noticies"
+++
![24 de gener jornada Bloc i Blanxart](/images/noticies/24_de_gener_jornada_bloc_i_blanxart.jpeg)

Podeu descarregar el Programa Electoral al següent [enllaç](/docs/programa_revisat_maig_2014.pdf)
