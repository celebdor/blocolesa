+++
title = "Carta al PSC"
description = ""
date = "2016-09-"
categories = ["blog"]
tags = ["resposta"]
thumbnail = ""
+++
Al Ple passat (febrer), el regidor Fernàndez, del PSC, em va acusar de mentir
perquè a una tertúlia de ràdio, vaig dir que la llei de racionalització i
sostenibilitat recomanava als ajuntament un romanent del 15%. Em va acusar de
mentir i em va demanar que penses si havia de dimitir.

En aquest escrit hi trobareu la resposta que vaig llegir al Ple del mes de març.

Al preàmbul de la Llei,  la LRSAL,  llegim que el que es pretén es reforçar la
idea d’**estabilitat**.

El principi d’estabilitat es defineix, també al preàmbul, com la situació
**d’equilibri o superàvit**. S’entén que s’assoleix aquesta situació quan les
administracions públiques no incorren en dèficit.

Al Capítol III diu que totes les administracions públiques han de presentar
**equilibri o superàvit**, sense que puguin incórrer en dèficit estructural.

A l’article 3, punt 2, llegim que “s’entén per estabilitat pressupostària la
situació d’equilibri o superàvit estructural.

A l’article 11, hi trobem que **“cap administració pública pot incórrer en
dèficit estructural. Les corporacions locals han de mantenir una posició
d’equilibri o superàvit pressupostari”**.

Com veuen, la llei parla de manera constant d’equilibri i superàvit.

A propòsit d’aquesta llei, el Departament d’economia i hisenda de la
Generalitat, va treure una nota informativa sobre tutela financera on diu que
**“el romanent de tresoreria ha de tenir signe positiu. La millora de la
planificació pressupostària anual ha de permetre el manteniment del signe
positiu del romanent”**. Primera recomanació de romanent positiu.

En aquest sentit, Ramón Auset, Responsable de l’Àrea de Corporacions Locals de
la Direcció de Política financera de la Generalitat, a l’escola d’estiu de
governs locals, que organitza la ACM i l’escola d’electes, en el seu curs sobre
finançament i endeutament de les entitats locals que va tenir lloc al setembre
del 2014, i al qual vaig assistir, diu que el romanent ha de tenir signe
positiu i recomana un romanent sobre el 10 o el 15% del pressupost, ja que
constitueix un recurs per a finançar despesa. Segona recomanació de romanent
positiu.

Diu exactament el mateix Josep Casas, cap del servei d’assistència a la gestió
econòmica local de la Diputació de Barcelona.  Tercera recomanació de romanent
positiu.

I a casa nostra, la intervenció municipal sempre ens demana, als seus informes,
un romanent positiu per atendre adequadament tant el fons de solvència, com el
pagament a tercers. Quarta recomanació de romanent positiu.

La llei parla de superàvit, i els he citat fins a 4 recomanacions de romanents
positius.

Per tant, quan vaig dir que la llei recomanava el 15% de romanent, hauria
d’haver dit que la llei demana superàvit, i que qui  recomana el 15% són
aquells que ens assessoren.

Podríem dir que no vaig ser rigorosament precís, però no vaig enganyar ningú.
D’això no se’n pot dir mentir. Ho saben perfectament. Mentir, els llegeixo la
definició, es “dir el contrari d’allò que se sap, es creu o es pensa que és
veritat”.  O bé, “conduir a un error o raonament fals”. Ja veuen, doncs,  que
no vaig dir el contrari del que sabia, ni vaig conduir ningú a cap raonament
fals. El que vaig dir s’ajusta força a la veritat, i els encoratjo a trobar una
sola recomanació o interpretació de a llei en sentit contrari al que els acabo
de dir

De tota manera, si algú que aquell dia escoltava la radio es va sentir confós,
em disculpo per l’error de precisió.

Però vostès, regidors del PSC, no parlen de falta de precisió, sinó que
m’acusen  d’haver faltat de manera premeditada a la veritat. M’acusen de mentir
de forma deliberada i de tenir una cara granítica, que no es poca cosa.

I van encara molt més lluny i agreugen les acusacions,  perquè en un exercici
de difamació pública, insinuen que, a més, amago informació als ciutadans i
ciutadanes. Sense cap proba de res, amb la única voluntat d’intoxicar, escriuen
i publiquen: “què deu fer el Sr. Martinez amb els ciutadans i ciutadanes que
tenen un accés restringit a la documentació, si a nosaltres ens menteix?”.

Què insinuen amb aquestes paraules, quins falsos rumors volen estendre? Què és
el que volen dir....? Entenen quina es la responsabilitat d’estendre
insinuacions i falsos rumors?  Han pensat que seria de la política municipal si
ens dediquéssim a acusar sense cap proba de res? Hi han destinat un minut a
fer-ne cap reflexió?

Quan la política utilitza el recurs dels atacs personals i dels insults, quan
busca debilitar les opcions contraries des de la demagògia, quan necessita del
rumor i de la insinuació per menystenir, es llavors quan la política entra de
ple en el terreny de l’abjecció i de la vilesa.

I els confesso que ha estat una autèntica decepció veure on arriba el seu
nivell en aquest sentit.

Em va demanar al Ple que dimitís, i també m’ho demanen per escrit. M’ho hauria
de pensar, diuen.

Crec que qui hauria de pensar en com es fan les coses son vostès, que haurien
d’avergonyir-se de situar el discurs polític al nivell de la desqualificació
personal. I haurien de disculpar-se davant els ciutadans i ciutadanes per haver
insinuat que se’ls amaga informació sense aportar la més mínima proba, i per
llançar insinuacions i falsos rumors només per desacreditar.

Son vostès, regidors del PSC, els qui s’han  de disculpar perquè no tot si val
ni tot està permès. I ho hauríem de fer als mateixos llocs on ho han dit i
publicat.

Amb tot, no els demano que pleguin. Son vostès els legítims representants dels
seus votants, i als seus votants els deuen fidelitat.

I als seus votants, però també a la resta de veïns i veïnes, els deuen, lluny
dels insults i de les falses insinuacions, un comportament exemplar.

Els deuen el que tots i totes ens mereixem, que és una manera honorable
d’exercir la política. Espero que així sigui.
