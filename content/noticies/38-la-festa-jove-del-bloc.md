+++
title = "LA FESTA JOVE DEL BLOC"
description = "Aquest divendres, festa jove al Parc, si el temps ho permet, a càrrec del Bloc Olesà"
tags = ["campanya2011"]
type = "blog"
date = "2011-05-10"
author =  "celebdor"
menu = "noticies"
+++
### Aquest divendres, festa jove al Parc, si el temps ho permet, a càrrec del Bloc Olesà

Aquest divendres 13 de maig a les 11 de la nit tenim festa grossa al PARC
MUNICIPAL amb el grup musical URBAND. Esperem que el temps ens permeti de
fer-la al parc, si no, aquesta vegada la fem on sigui!

Si voleu més informació sobre el grup:

{{< youtube daHEVg4SfJQ >}}

{{< youtube vd4qgnyoX20 >}}

{{< youtube 4YNsq2cWlUQ >}}

### Cartell de la Festa jove:

![Cartell de la festa jove 2011](/images/noticies/cartell_festa_jove.jpeg)
