+++
title = "Ribes Blaves"
description = "<h3 id=\"NewsPostDetailContent\"><strong> La modificació del Pla General sobre la Urbanització de Ribes Blaves: un important pas endavant per regularitzar-ne la situació</strong></h3>"
tags = [  ]
type = "blog"
date = "2013-06-03"
author =  "celebdor"
menu = "noticies"
+++
<h3 id="NewsPostDetailContent"><strong> La modificació del Pla General sobre la Urbanització de Ribes Blaves: un important pas endavant per regularitzar-ne la situació</strong></h3>


<p>En el Ple celebrat el passat dia 25/04/2013 el Consistori va aprovar la modificació del Pla General pel que fa a la Urbanització de RIBES BLAVES. Aquesta modificació permetrà donar un pas crucial en la legalització de la Urbanització</p>
<p>Els olesans residents en aquesta urbanització porten més de 20 anys intentant poder-la legalitzar. Les gestions van començar els anys 1991-1993, però quan l’any 1995 ho tenien tot a punt els varen dir que s’havia de canviar tot.</p>
<p>L any 2000 la Generalitat de Catalunya&nbsp; va aprovar els 5 Plans de Millora Urbana (PEMUs) de la urbanització Ribes Blaves. A principis de l’any 2011 La Junta de Compensació de Ribes Blaves va presentar a l’anterior Equip de Govern municipal una modificació dels 5 PEMUs aprovats. La modificació substancial era la de modificar la mobilitat dels vehicles fent els carrers més segurs.</p>
<p>Quan el BLOC OLESA va entrar a gestionar l’Ajuntamenta a meitats de l’any 2011, l’assumpte estava encallat, no s'havia completat la feina ni s'havia informat els veïns de la gran dificultat que representava negociar 5 PEMUs diferents, cosa que volia dir tenir 5 juntes compensació, resoldre les obligacions de reserves d’espai per a equipaments i zones verdes de cadascuna de les 5 zones separadament, fer 5 projectes d’urbanització i 5 reparcel·lacions ...</p>
<p>El Bloc Olesà va veure que per desencallar la qüestió el primer que calia fer era modificar el Pla General perquè només contemplés un PEMU en lloc de 5, i a partir d’aquí, el treball a fer és:</p>
<ul>
<li>Modificar totes les amplades dels vials, excepte la del principal, fent-los més estrets, per adequar-se a la realitat actual (es compta amb l’informe favorable de Bombers, que és obligatori).</li>
<li>Consolidar les aportacions obligatòries d’equipaments i zones verdes, ja que dividides en 5 PEMUs n’hi havia que complien la normativa i altres que no. Però unificades en un sol PEMU sí que es compleix.</li>
<li>Modificar zones de serveis, sobretot pel que fa a les depuradores.</li>
</ul>
<p>Amb aquestes modificacions els olesans residents a Ribes Blaves es beneficien perquè:</p>
<ol>
<li>No hauran de cedir més terrenys per a zones verdes ni equipaments.</li>
<li>Aconsegueixen una millor ubicació dels serveis “depuradores” i, per tant, un estalvi en el manteniment.</li>
<li>Obtenen un abaratiment “considerable” en el cost de la urbanització del sector, ja que degut a la reducció d’amplada dels carrers no s'hauran d'enderrocar alguns habitatges i una gran quantitat de murs.</li>
<li>Guanyen en seguretat viària, ja que desapareixen gairebé tots els “culs de sac” per fer els girs dels vehicles, que seran substituïts per uns bucles de connexió que permeten retorns coherents. Es minimitza l’impacte ambiental d’aquests sobre la urbanització.</li>
<li>Estalvien i faciliten la gestió, perquè redueixen de cinc juntes de compensació a una.</li>
</ol>
<p>Esperem i desitgem que la unificació dels PEMUs sigui l’última modificació que hagin de patir els olesans de Ribes Blaves. Són molts anys (20), massa, que els olesans de Ribes Blaves estan lluitant per regularitzar la seva situació i mereixen un final feliç. Des d’aquí els donem les gracies per la paciència que han tingut i per la seva constant col·laboració.</p>