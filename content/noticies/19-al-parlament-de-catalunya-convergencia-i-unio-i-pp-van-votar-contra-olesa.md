+++
title = "Al parlament de Catalunya, Convergència i Unió i PP van votar contra Olesa"
description = "<p>El projecte per salvar el Molí de l’Oli i fer possible la rehabilitació de la Nau municipal de Vilapou no troba el suport parlamentari de Convergència i Unió i PP. Hi van votar a favor ICV-EUiA, PSC, ERC, SI i C’s.</p><p>A Olesa de Montserrat, i en concret a la zona de Vilapou, hi ha una nau industrial modernista, catalogada i propietat patrimonial dels olesans i olesanes (nivell de protecció II. Estructura tipològica). Malgrat aquest reconeixement, la nau es troba en un estat de degradació preocupant, ja que durant molts anys no s\"hi ha fet manteniment, fins al punt que algunes finestres es comencen a ensorrar.</p><h3>Per altra banda, el Molí de l\"Oli d\"Olesa, un altre edifici emblemàtic, està afectat pel projecte de soterrament de la Riera de can Carreras. Es tracta d\"una actuació prevista a la Llei de barris, que fa inviable continuar amb l’activitat actual.</h3>"
tags = [  ]
type = "blog"
date = "2012-03-09"
author =  "celebdor"
menu = "noticies"
+++
<p>El projecte per salvar el Molí de l’Oli i fer possible la rehabilitació de la Nau municipal de Vilapou no troba el suport parlamentari de Convergència i Unió i PP. Hi van votar a favor ICV-EUiA, PSC, ERC, SI i C’s.</p>
<p>A Olesa de Montserrat, i en concret a la zona de Vilapou, hi ha una nau industrial modernista, catalogada i propietat patrimonial dels olesans i olesanes (nivell de protecció II. Estructura tipològica). Malgrat aquest reconeixement, la nau es troba en un estat de degradació preocupant, ja que durant molts anys no s'hi ha fet manteniment, fins al punt que algunes finestres es comencen a ensorrar.</p>
<h3>Per altra banda, el Molí de l'Oli d'Olesa, un altre edifici emblemàtic, està afectat pel projecte de soterrament de la Riera de can Carreras. Es tracta d'una actuació prevista a la Llei de barris, que fa inviable continuar amb l’activitat actual.</h3>


<p>Tot relacionant aquests dos problemes, l'Equip de govern actual va pensar que es podia rehabilitar la nau modernista de Vilapou i convertir-la en la seu del Molí de l'Oli. Es tractaria de fer una permuta d'usos, sense canvi de propietat. Això també podria contribuir a aconseguir la denominació d'origen per a la nostra varietat autòctona d'oli. Alhora, l’actual molí de l'oli, després de traslladar tota la maquinària, es podria convertir en un espai museístic al centre del poble, compatible amb altres usos públics.</p>
<p>Això permetria aconseguir uns objectius de gran importància per a la nostra vila, com:</p>
<ol>
<li>&nbsp;&nbsp;&nbsp; Es rehabilitaria una propietat municipal molt degradada, situada en una zona industrial.</li>
<li>&nbsp;&nbsp;&nbsp; Es garantiria la continuïtat d’una activitat econòmica i tradicional (el molí de l’oli), tan emblemàtica d'Olesa i necessària en temps de crisi, posada en perill per una actuació urbanística &nbsp;</li>
<li>&nbsp;&nbsp;&nbsp; Es dotaria Olesa d’un nou espai públic polivalent al centre del poble. &nbsp;</li>
<li>&nbsp;&nbsp;&nbsp; Es desencallaria el projecte de soterrament de la Riera de Can Carreras que, recordem-ho, va ser multirecorregut en seu judicial.</li>
</ol>
<p>És per aconseguir aquests objectius que es va presentar una proposta al Parlament de Catalunya: la de donar suport al&nbsp; projecte per salvar el Molí de l’Oli i fer possible la rehabilitació de la Nau municipal de Vilapou.&nbsp; Malauradament, però, no va ser aprovada, ja que, malgrat el gran benefici que això representava per a Olesa de Montserrat, Convergència i Unió i PP hi van votar en contra i en van impedir l'aprovació. Aquesta proposta sí que trobà el suport parlamentari d’ICV-EUiA, PSC, ERC, SI i C’s.</p>
<p>Cal recordar, ara i aquí, que tenim una olesana al Parlament de Catalunya, que és de CiU i que a les darreres eleccions va ser candiadata a alcaldessa per aquest partit.</p>