+++
title = "{{ replace .Name "-" " " | title }}"
type = "blog"
date = {{ .Date }}
author =  "celebdor"
menu = "butlletins"
+++

{{< pdf-embed url="/revistes/revista_mes_any.pdf" >}}
